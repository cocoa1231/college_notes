from Model import SIRModel
from matplotlib.animation import FuncAnimation
from matplotlib.patches import Circle
import pandas as pd
import matplotlib.pyplot as plt

susceptible, infected, recovered = 99, 1, 0
width = height = 20
infection_radius = 0.30
infection_probability = 0.20
recovery_time = 5

M = SIRModel(susceptible, infected, recovered, width, height, infection_radius,
             infection_probability, recovery_time)

while M.distribution['I'] > 0:
    M.step()

sim_dataframe = M.datacollector.get_model_vars_dataframe()
particle_dataframe = M.datacollector.get_agent_vars_dataframe()

print(particle_dataframe.keys())
print(particle_dataframe.loc[particle_dataframe['Step'] == 0])
