from mesa import Model, Agent
from mesa.datacollection import DataCollector
from mesa.space import ContinuousSpace
from mesa.time import RandomActivation
import numpy as np

# Model Data Collection
get_susceptible = lambda model: model.distribution['S']
get_infected = lambda model: model.distribution['I']
get_recovered = lambda model: model.distribution['R']
#get_sir_distribution = lambda model : tuple(model.distribution.values())

# Agent Data datacollection
get_agent_pos = lambda agent_id : agent_id.pos
get_agent_state = lambda agent_id : agent_id.state
get_agent_infect_count = lambda agent_id : agent_id.count_spread_to

class SIRAgent(Agent):
    """
    SIR Agent Class
    %param unique_id int : Unique Agent ID
    %param model mesa.Model : Model obejct that this Agent is attached to
    %param state char : State of the Agent, 'S', 'I' or 'R'
    """

    def __init__(self, unique_id, model, state):
        # Call base class constructor
        super().__init__(unique_id, model)

        # Count the number of people this agent spreads the disease to
        # and save the amount of time the agent has been infected for.
        self.count_spread_to = 0
        self.infected_duration = 0
        self.state = state

    def move(self):
        """
        Agent moves randomly within the space, with each step having a max size
        of 10 (chosen arbitrarily. Can change around)
        """
        random_sample_x = np.random.random_sample(10) * 10
        random_sample_y = np.random.random_sample(10) * 10

        dx = np.random.choice(random_sample_x)
        dy = np.random.choice(random_sample_y)

        new_pos = (self.pos[0]+dx, self.pos[1]+dy)
        self.model.space.move_agent(self, new_pos)

    def infect(self):
        """
        Get a list of neighbors and infect them
        """
        # Get neighbors in radius
        neighbors = self.model.space.get_neighbors(
            self.pos, self.model.inf_radius)

        # Infect probabilistically
        for neighbor in neighbors:
            if neighbor.state == 'S':
                r = np.abs(np.random.normal(0,1))
                if r < self.model.inf_prob:
                    neighbor.state = 'I'
                    self.model.distribution['I'] += 1
                    self.model.distribution['S'] -= 1
                    self.count_spread_to += 1

    def step(self):
        # Check if agent is infected and infect others if so
        if self.state == 'I':
            self.infect()
            self.infected_duration += 1
            if self.infected_duration > self.model.recovery_time:
                self.state = 'R'
        self.move()



class SIRModel(Model):
    """
    SIR Model Class
    %param S int : Number of susceptible people initially
    %param I int : Number of infected people initially
    %param R int : Number of recovered people initially
    %param width float : Width of the ContinuousSpace
    %param height float : Height of the ContinuousSpace
    %param infectious_radius float : Radius within which an agent can infect
    %param infectious_prob float : Probability of spreading the infecting within
                             infectious_radius
    %param recovery_time int : Number of time steps an agent takes to recover

    Randomly activates all agents
    """

    def __init__(self, S, I, R, width, height,
                 infectious_radius, infectious_prob, recovery_time):

        self.width = width
        self.height = height
        self.inf_radius = infectious_radius
        self.inf_prob = infectious_prob
        self.recovery_time = recovery_time
        self.distribution = {'S': S, 'I': I, 'R': R}
        self.schedule = RandomActivation(self)
        self.space = ContinuousSpace(width, height, True)
        self.random_sample_x = np.random.sample(self.width)
        self.random_sample_y = np.random.sample(self.height)
        self.running = True

        # Define data collector to collect SIR distribution data and agent
        # position, state and spread count.
        self.datacollector = DataCollector(
            model_reporters={'Susceptible': get_susceptible,
                             'Infected': get_infected,
                             'Recovered': get_recovered},
            agent_reporters={'Agent Position': get_agent_pos,
                             'Agent State': get_agent_state,
                             'Agent Spread Value': get_agent_infect_count}
        )

        # Not the best way, but add agents with a unique incremental integer ID
        for i in range(S):
            self._add_agent(i, 'S')

        for i in range(S, S+I):
            self._add_agent(i, 'I')

        for i in range(S+I, S+I+R):
            self._add_agent(i, 'R')

    def step(self):
        """
        Each step collect the data and update self.distribution
        Check if self.distribution['I'] == 0, and if nobody is infected, stop
        running
        """
        self.datacollector.collect(self)
        sir_data = self.datacollector.get_model_vars_dataframe().loc[['S', 'I', 'R']]
        self.distribution['S'] = sir_data[0][0]
        self.distribution['I'] = sir_data[0][1]
        self.distribution['R'] = sir_data[0][2]

        if self.distribution['I'] == 0:
            self.running == False

        self.schedule.step()



    def _add_agent(self, i, state):
        """
        Randomly places an agent in our ContinuousSpace
        """
        a = SIRAgent(i, self, state)

        x = (np.random.choice(self.random_sample_x) * self.width) % self.width
        y = (np.random.choice(self.random_sample_y) * self.height) % self.height

        self.schedule.add(a)
        self.space.place_agent(a, (x, y))
