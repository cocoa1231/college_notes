from Model import SIRModel, get_sir_distribution
from mesa.batchrunner import BatchRunner
import pandas as pd
import matplotlib.pyplot as plt

susceptible, infected, recovered = 99, 1, 0
width = height = 20
infection_radius = 0.30
infection_probability = 0.20
recovery_time = 5

M = SIRModel(S, I, R, width, height, infection_radius, infection_probability, recovery_time)

while M.distribution['I'] > 0:
    M.run()
    
run_data = M.datacollector.get_model_vars_dataframe()

fixed_params = {
    "S": susceptible,
    "I": infected,
    "R": recovered,
    "width": width,
    "height": height,
    "infectious_radius": infection_radius,
    "infectious_prob": infection_probability,
    "recovery_time": recovery_time
}

variable_params = {}

batch_run = BatchRunner(
    SIRModel,
    variable_params,
    fixed_params,
    iterations=50,
    model_reporters={"SIR_Data": get_sir_distribution}
)

batch_run.run_all()
