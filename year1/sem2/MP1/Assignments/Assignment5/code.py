#!/usr/bin/env python

import numpy as np

def nilpotent_degree(P):
    """
    Return the nilpotent degree of a matrix P
    Uses numpy.matrix and the correct * operator defined
    for matrix-matrix multiplication
    """
    P = np.matrix(P)
    degree = 1

    while(P.all() != 0):
        degree += 1
        P = P * P

    return degree

if __name__ == "__main__":
    P = np.array([ [2,2,2,-3],
                   [6,1,1,-4],
                   [1,6,1,-4],
                   [1,1,6,-4] ])

    print("The matrix in question is \n",P)
    print("It's nilpotent degree is", nilpotent_degree(P))
