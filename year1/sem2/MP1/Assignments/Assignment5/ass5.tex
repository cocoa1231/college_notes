\documentclass[12pt]{article}


\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{physics}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{fancyhdr}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\usepackage{fontspec}
\usepackage{enumitem}


\graphicspath{ {./figures/} }

\pagestyle{fancy}
\fancyhf{}
\rhead{Spring 2020}
\lhead{PHY1110 - Mathematical and Computational Toolkit}

\newcommand{\inv}[1]{\ensuremath{#1^{-1}}}
\newcommand{\Mat}[2]{\ensuremath{\mbox{Mat}_{#1}(#2)}}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\ima}{Im}

\begin{document}

	\begin{center}
		{\LARGE Assignment 5: \\ Singular Matrices and Change of Basis}

		\vspace{1em}
				
		{\large Cocoa}
		
		\vspace {1em}
		
		{\large March 06, 2020}
		
		\vspace{-1em}
		
	\end{center}
	
	\noindent \textbf{\hrulefill}


	\section{The Inverse}
	
	\begin{enumerate}[label=(\alph*)]
	
		\item We can multiply $A^2 + B^2$ by $A-B$ to get $A^3 - A^2B + B^2A - B^3 = 0$. This means that $(A^2 + B^2)(A-B) = 0$. Since $A \neq B$, that means $A^2 + B^2 = 0$, and thus has a zero determinant. Therefor, it is not invertable.
		
		\item Since we know that $A = A^{-1}$, we know $\det(A) = \det(A^{-1}) \implies \det(A) = \frac{1}{\det(A)}$. This leaves $\pm 1$ as the only possible value for $\det(A)$
		
		\item If we multiply $F$ by $\inv F$, we should get the identity matrix. Performing the calculation
		
		\begin{align*}
			\mqty[ a & b \\ c & d] \mqty [d & -c \\ -b & a] 
			= \mqty[ad - bc & -ac + ac \\ bd - bd & ad - bc]
			= \mqty[ad - bc & 0 \\ 0 & ad - bc]
		\end{align*}
		
		Multiplying this by $1/\det(F) = \frac{1}{ad - bc}$,
		
		\begin{align*}
			\frac{1}{ad - bc} \mqty[ad - bc & 0 \\ 0 & ad - bc] = \mqty[1 & 0 \\ 0 & 1]
		\end{align*}
		
		Therefor, $\inv F = \frac{1}{\det(F)} \mqty[d & -c \\ -b & a]$
		
	\end{enumerate}
	
	\section{Matrix Representations of Linear Maps}
	
	\begin{enumerate}[label=(\alph*)]
	
		\item The space of all polynomials with degree 3 has one basis $\beta = \{x^0,x^1,x^2,x^3\}$. To show this, consider the following
		
		\begin{align*}
			a_i \vb {x^i} = 0
		\end{align*}
		
		summed over the index $i$, where $\vb{x^i} \in \beta$ and $a_i \in \mathbb R$. The only time this equation holds true is when all $a_i = 0$, and thus $\beta$ is linearly independent. Since a general element of $P_3(x)$ has the form $c_1 + c_2x + c_3x^2 + c_4x^3$, we can represent this using our basis vectors as $c_1 \cdot b_0(x) + c_2 \cdot b_1(x) + c_3 \cdot b_2(x) + c_4 \cdot b_3(x)$ and thus any element of the space $P_3(x)$ admits a unique representation in our basis $\beta$. Thus the system $\beta$ is linearly independent and spanning, therefor it's a basis. Since the basis has 4 elements, the dimension of the space is 4. The transformation $L(p(x)) : P_3(x) \mapsto \mathbb R^4 \ \forall p(x) \in P_3(x)$ acts on the basis vectors as follows.
		
		\begin{align*}
			L(b_0(x)) = \mqty[ 1 \\0\\0\\0] \ 
			L(b_1(x)) = \mqty[0\\ 1 \\0\\0] \ 
			L(b_3(x)) = \mqty[0\\0\\ 1 \\0] \
			L(b_4(x)) = \mqty[0\\0\\0\\ 1 ]
		\end{align*}
		
		\item We are looking at the transformation $D : p(x) \mapsto p^\prime (x)$
		
		\begin{enumerate}[label=(\roman*)]
		
			
			
			\item To check for linearity, since we already know that the derivative is linear, we know that $D$ must be linear. However, explicitly checking for that,
			
			\begin{itemize}
				\item \emph{Additivity} -- $f(\vb a + \vb b) = f(\vb a ) + f(\vb b)$. Checking this for $D$, considering some $p, q \in P_3(x)$ and some $\lambda \in \mathbb R$
				
				\begin{align*}
					D(p+q) = \dv{x} (p + q) = \dv{p}{x} + \dv{q}{x} = D(p) + D(q)
				\end{align*}
				
				\item \emph{Scaling} -- $f(\lambda x) = \lambda f(x)$
				
				\begin{align*}
					D(\lambda p) = \dv{x} (\lambda p) = \lambda \dv{p}{x} = \lambda D(p)
				\end{align*}
			\end{itemize}
			
			Thus $D$ is linear
			
			\item Seeing what $D$ does to our basis vectors
			
			\begin{align*}
				D(b_0(x)) = \dv{x} (1) = 0 = \mqty[ 0\\0\\0\\0 ] \
				&D(b_1(x)) = \dv{x} (x) = 1 = \mqty[ 1 \\0\\0\\0] \\
				D(b_2(x)) = \dv{x} (x^2) = 2x = \mqty[0\\ 2 \\0\\0] \
				&D(b_3(x)) = \dv{x} (x^3) = 3x^2 = \mqty[0\\0\\ 3 \\0]
			\end{align*}
			
			Thus the matrix is simply a matrix counting up the natural numbers on the superdiagonal
			
			\begin{align*}
				\Mat{L}{D} = \mqty[  0 & 1 & 0 & 0 \\ 0 & 0 & 2 & 0 \\ 0 & 0 & 0 & 3 \\ 0 & 0 & 0 & 0 ]
			\end{align*}
			
			\item To find $\ker$ of $D$, we need to write it in reduced echelon form $D_{re}$. That allows us to find the free variables which will span the null space. Essentially, this allows us to solve $D\vb p = \vb 0$, and since during row operations, $\vb 0$ does not change, we simply need to find $D_{re}$.
			
			\begin{align*}
				\mqty[  0 & 1 & 0 & 0 \\ 0 & 0 & 2 & 0 \\ 0 & 0 & 0 & 3 \\ 0 & 0 & 0 & 0 ] \mqty[ c_1 \\ c_2 \\ c_3 \\ c_4 ] = \mqty[ 0 \\ 0 \\ 0 \\ 0 ]
			\end{align*}
			
			Thus, $c_1$ is free, $c_2 = c_3 = c_4 = 0$
			
			\begin{align*}
				\vb p = c_1 \mqty[ 1 \\ 0 \\ 0 \\ 0 ]
			\end{align*}
			
			And the $\vspan(\vb p)$ is $\vspan(\vb{b_0})$. 
			
			\item The dimension of $\ker(D)$, as we've seen is 1, since $b_0$ forms a basis in it. The image of $D$ is the set of all vectors $\vb q$ such that $D\vb q = \vb b$ and $\vb b \neq \vb 0$. In the echelon form of a matrix, the columns forming the pivots form a basis in the column space of $D$ (or simply $\ima(D)$). Since $D$ is already in echelon form, we can see the pivots here
			
			\begin{align*}
				\mqty[  0 & \boxed{1} & 0 & 0 \\ 0 & 0 & \boxed{2} & 0 \\ 0 & 0 & 0 & \boxed{3} \\ 0 & 0 & 0 & 0 ]
			\end{align*}
			
			And thus these vectors form a basis in $\ima(D)$, giving us a 3 dimensional space. Therefor, $\dim(\ker(D)) + \dim(\ima(D)) = 4$ which is our row space, $n$.
			
		\end{enumerate}
		
		\item Let our new basis be $\gamma = \{1,x,x+x^2,x^3\}$, and the new map $M:P_3(x) \mapsto \mathbb R^4$ acts on these basis vectors as follows
		
		\begin{align*}
			M(1) = \mqty[ 1 \\0\\0\\0] \ 
			M(x) = \mqty[0\\ 1 \\0\\0] \ 
			M(x+x^2) = \mqty[0\\0\\ 1 \\0] \
			M(x^3) = \mqty[0\\0\\0\\ 1 ]
		\end{align*}
		
		\begin{enumerate}[label=(\roman*)]
			\item Applying $D$ to $\gamma$, $D(\gamma) = \{0, 1, 1+2x, 3x^2\}$, we get
			
			\begin{align*}
				D(1) = \smqty[0\\0\\0\\0] \ D(x) = \smqty[1\\0\\0\\0] \
				D(x+x^2) = \smqty[1\\2\\0\\0] \ D(x^3) = \smqty[0\\-3\\3\\0] \\
			\end{align*}
			
			And then finding $\Mat{M}{D}$
			
			\begin{align*}
				\Mat{M}{D} = \mqty[ 0 &1&1& 0 \\ 0 & 0 & 2 & -3 \\ 0 & 0 & 0 & 3 \\ 0 & 0 & 0 & 0 ]
			\end{align*}
			
			\item The change of basis matrix, which transforms D written using $\beta$ to D written using $\gamma$ is
			
			\begin{align*}
				B = \mqty[ 1 & 0 & 0 & 0 \\ 0 & 1 & 1 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 1 ]
			\end{align*}
			
			Since $B$ is the transformation of our basis vectors, the basis vectors are covariant with $B$ and any arbitrary vector in $P_3(x)$ is contra-variant with $B$, i.e, it's coordinates transform as $\inv B$. Thus,
			
			\begin{align*}
				M = BL
			\end{align*}
			
			\item We can use $B$ to transform between $\Mat L D$ and $\Mat M D$. The matrix transforms in the following way
			
			\begin{align*}
				\Mat M D  = \inv B \Mat L D B
			\end{align*}
			
			This is because if we consider $\Mat M D$, it is a transformation in $P_3(x)$ written in $\mathbb R^4$ using $\gamma$, thus it takes in vectors written in terms of $\gamma$. On the right side, $B$ transforms vectors written in $\gamma$ to vectors written in $\beta$, then applies the transformation $\Mat L D$, and then converts it back into a vector written in terms of $\gamma$ using $\inv B$. Carrying out this multiplication, we get the following
			
			\begin{align*}
				\inv B \Mat L D B &= \mqty[ 1 & 0 & 0 & 0 \\ 0 & 1 & -1 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 1 ] \mqty[  0 & 1 & 0 & 0 \\ 0 & 0 & 2 & 0 \\ 0 & 0 & 0 & 3 \\ 0 & 0 & 0 & 0 ] \mqty[ 1 & 0 & 0 & 0 \\ 0 & 1 & 1 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 1 ] \\
				&= \mqty[ 1 & 0 & 0 & 0 \\ 0 & 1 & -1 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 1 ] \mqty[ 0 & 1 & 1 & 0 \\ 0 & 0 & 2 & 0 \\ 0 & 0 & 0 & 3 \\ 0 & 0 & 0 & 0 ] \\
				&= \mqty[ 0 &1&1& 0 \\ 0 & 0 & 2 & -3 \\ 0 & 0 & 0 & 3 \\ 0 & 0 & 0 & 0 ] = \Mat M D
			\end{align*}
			
		\end{enumerate}
	
	\end{enumerate}
	
	\section{Computing}
	
	The code for this section is attached below
	
	\lstinputlisting[language=Python]{code.py}
	
	The function uses Numpy's matrix objects to perform matrix-matrix multiplication using the star (*) operator. The function simply keeps multiplying the matrix $P$ by itself until it finds that all the elements of the matrix are 0, which is when it returns the degree.

\end{document}