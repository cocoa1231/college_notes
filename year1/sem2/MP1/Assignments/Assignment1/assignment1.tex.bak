\documentclass[12pt]{article}

\usepackage{enumitem}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[export]{adjustbox}
\usepackage[left=1in,right=1in=top=1in,bottom=1in]{geometry}

\graphicspath{ {./images/} }

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\title{MCT: Assignment 1}
\author{Cocoa}

\begin{document}

	\maketitle

	\section{Dimensional Analysis}
	
	\textbf{Question}: In the Discussion Session, we spoke in detail of Dimensional Analysis and how it can answer some apparently intractable problems quite simply. Explain your working out \textbf{very clearly.}
	
	\begin{enumerate}[label=(\alph*)]
		\item Imagine you spill a glass of some liquid on the ground. The height $h$ of the puddle of liquid depends on the density of the liquid $\rho$, the surface tension $s$ of the liquid, and the acceleration due to gravity $g$ . Find a dimensionally correct formula for h using dimensional analysis. Taking the dimensionless constant to be of order 1, find h for water. \hfill \textbf{[2]}
		
		\textbf{Answer}: The height of the puddle depends on the density of the liquid, the surface tension and the acceleration due to gravity. The dimensions for each are as follows
		
		\begin{align}
			[h] &= [L] \\
			[\rho] &= \left[\frac{\mbox{Mass}}{\mbox{Volume}}\right] = \left[\frac{M}{L^3}\right] \\
			[s] &= \left[\frac{\mbox{Energy}}{\mbox{Area}}\right] = \left[\frac{Fd}{A}\right] \left[ \frac{M \ T^{-2} L^2}{L^2} \right] = \left[\frac{M}{T^2}\right] \\
			[g] &= \left[ \frac{L}{T^2} \right]
		\end{align}	
		
		If we let the proportionality constant be $\alpha$, then we can say
		
		\begin{gather*}
			h = \alpha \rho^i s^j g^k
		\end{gather*}	
		
		\pagebreak
		
		Comparing the dimensions on both sides
		
		\begin{align*}
			[L] &= \left[\frac{M}{L^3}\right]^i \cdot \left[\frac{M}{T^2}\right]^j \cdot \left[ \frac{L}{T^2} \right]^k \\
			[L] &= \left[M^{(i+j)} \ L^{(-3i+k)} \ T^{(-2j-2k)}\right]
		\end{align*}
		
		And comparing the powers we can see that $i = -j$ and $j = -k$ immediately. Thus, in $-3i + k = 1$, we can let $k = -i$, and thus $i = - \frac{1}2$, $j = \frac{1}2$, $k = -\frac{1}2$, giving us the final equation
		
		\begin{gather}
			\boxed{ h = \alpha \sqrt{ \frac{s}{\rho g} } }
		\end{gather}
		
		In order to cross check, we can compare the dimensions on both sides
		
		\begin{align*}
			\left[L\right] &= \sqrt{\frac{ \left[\frac{M}{T^2}\right] }{ \left[\frac{M}{L^3}\right] \cdot \left[\frac{L}{T^2}\right] }} \\
			\left[L\right] &= \sqrt{ \frac{1}{ \left[\frac{1}{L^2}\right] } } \\
			\left[L\right] &= \left[L\right]
		\end{align*}
		
		\item Consider a drop of liquid in free space (i.e., neglecting gravity). The drop takes on a spherical shape because for a given volume the sphere has the minimum surface area, and the spherical configuration minimises the energy associated with creating the surface. If this drop is disturbed, it starts oscillating. Find a dimensionally correct formula for the time period of oscillation in terms of the quantities that matter. \hfill \textbf{[3]}
		
		\textbf{Answer}: The dimensions of the spring constant $k$ are the same as the dimensions of surface tension $s$. Since we know the time period of a simple harmonic oscillator, we can say that a dimensionally correct formula for the time period of the oscillation can be
		
		\begin{gather}
			T = \alpha \sqrt{\frac{s}{m}}
		\end{gather}
		
		Where $\alpha$ is some proportionality constant. However it is ambiguous as to what one means by the time period of the oscillation of a drop of liquid, because once the surface is disturbed at a point by a force, the effect of the force travels across the surface of the bubble as a wave, thus each point oscillates at a different amplitude and it is ambiguous to talk about the time period of the whole drop. For the sake of simplicity, we will refer to the time period of some tiny area $dA$ on the surface of the drop, upon which the force acts, and we watch that $dA$ oscillate. We can think of the density of water $\rho$ and the width of the surface $dx$, allowing us to define a mass $dm = \rho \ dA \ dx = \rho \ dV$. We can then write $(6)$ as
		
		\begin{gather}
			\boxed{T = \alpha \sqrt{\frac{s}{\rho dV}}}
		\end{gather}
		
	\end{enumerate}
	
	\section{Programming}

	\textbf{Question}: Start off with the simple harmonic oscillator that we spoke about in the Discussion Session. Now introduce a damping force that is of the form $F_d = -m \gamma \ddot{x}$
		
	\begin{enumerate}[label=(\alph*)]
	
		\item Write out the differential equation that this system satisfies. Identify the time scales in the problem. (You should be able to construct two quantities $\tau_1$ and $\tau_2$ of dimension time from the parameters of the problem.) \hfill \textbf{[2]}
		
		\textbf{Answer}: The differential equation for this system is
		
		\begin{gather*}
			m\frac{d^2 x}{dt^2} = - k x - m \gamma \frac{dx}{dt}
		\end{gather*}
		
		Dividing through by mass, we get
		
		\begin{gather*}
			\ddot{x} = -\frac{k}m x - \gamma \dot{x}
		\end{gather*}
		
		The two time scales in this problem are then $\tau_1 = \frac{k}m$ with dimensions of $\left[ T^{-2} \right]$ and $\tau_2 = \gamma$ with dimensions of $\left[ T^{-1} \right]$
		
		\item Write a code to solve this system using the leapfrog method, setting $\tau_1 = 1$ and $\tau_2 = 1$ with initial condition $x(0) = 1$, $v(0) = 0$. Plot the solution to this problem (i.e. plot $x(t)$) for a sufficiently large range of time. \hfill \textbf{[5]}
		
		\pagebreak
		
		\textbf{Answer}: The code for the problem is
		
		\begin{lstlisting}[language=Python]
import numpy as np
import matplotlib.pyplot as plt

# Configuration Parameters
k = 1
g = 1
tf = 90
dt = 0.01

def a(k, g, x, v):
    return -k*x -g*v


def spring_plot(k, g, tf, dt):
    x, v = np.array([]), np.array([])
    t = np.r_[[0],np.arange(dt/2, tf, dt)]

    x = np.append(x, 1)
    v = np.append(v, 1)

    for _t in t[:-1]:
        v = np.append( v, v[-1] + a(k, g, x[-1], v[-1]) * dt )
        x = np.append( x, x[-1] + v[-1] * dt )
    return x, v, t

x, v, t = spring_plot(k, g, tf, dt)
simple_fig, simple_ax = plt.subplots()
simple_ax.plot(t, x)
simple_fig.savefig('simple_spring.png')
\end{lstlisting}

		\begin{figure}[h]
			
			\includegraphics[scale=0.75,width=1.1\textwidth,center]{simple_spring}
			\label{fig:1}	
			\caption{Plot of $x(t)$ with $\gamma = 1$ and $k/m = 1$}
			
		\end{figure}
		
		\pagebreak
		
		\item Now choose a value of $\tau_1 \gg \tau_2$, and plot the solution to this problem for the same initial conditions. Now choose a value of $\tau_1 \ll \tau_2$, and do the same thing. \hfill \textbf{[2]}
		
		\textbf{Answer}: The respective plot is
		
		\begin{figure}[h!]
		\includegraphics[scale=1,width=1.1\textwidth,center]{comparitive_plot}
		\caption{Plots of $x(t)$ where the value for $\gamma$ varies between $\frac{1}2 k$ to $100 k$ for a damped harmonic oscillator with equation $\ddot x = -\frac{k}m x - \gamma \dot x$}
		\label{fig:2}
		\end{figure}
		
		\item Comment on the any features in the above graphs that you find interesting. \hfill \textbf{[1]}
		
		\textbf{Answer}: It becomes clear from the plots that as the value of $\gamma$ increases with respect to $k$, the potential energy of the spring stored $t=0$ is not translated into kinetic energy of the spring, meaning that the term $F_d$ is a \textit{dampening term}. It is analogous to the \textit{viscosity} of the surrounding fluid, thus $F_d$ is an external force on the spring
		
	
	\end{enumerate}
	
	
	

\end{document}