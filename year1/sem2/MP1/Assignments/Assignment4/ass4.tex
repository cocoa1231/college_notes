\documentclass[12pt]{article}

\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{physics}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{fancyhdr}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\usepackage{fontspec}
\usepackage{enumitem}


\graphicspath{ {./figures/} }

\pagestyle{fancy}
\fancyhf{}
\rhead{Spring 2020}
\lhead{PHY1110 - Mathematical and Computational Toolkit}

\begin{document}
	
	\begin{center}
		{\LARGE Assignment 4: \\ Linear Transformations and Matrices}

		\vspace{1em}
				
		{\large Cocoa}
		
		\vspace {1em}
		
		{\large February 27, 2020}
		
		\vspace{-1em}
		
	\end{center}
	
		\noindent \hrulefill

	\section{Linear Transformations}

	
		\begin{enumerate}[label=(\alph*)]
		
			\item \begin{enumerate}[label=(\roman*)]
			
				\item The function $\mathscr{F} : \mathbb{R}^3 \mapsto \mathbb{R}^2$ can be checked for linearity if under the transformation, the group structure of vector addition and scalar multiplication is preserved. We can check that by ensuring the following two properties
			
			\begin{align*}
				\mathscr{F}(\vb{v} + \vb w) &= \mathscr{F}(\vb v) + \mathscr{F}(\vb w) \\
				\mathscr{F}(\alpha \vb v) &= \alpha \mathscr{F}(\vb v)
			\end{align*}
			
			For some vectors $\vb v,\vb w \in \mathbb{R}^3$ and $\alpha \in R$. Let $\vb v = \smqty[a \\ b \\ c]$ and $\vb w = \smqty[d \\ e \\ f]$. Their sum is $\vb v + \vb w = \smqty[a+d \\ b+e \\ c+f]$. Applying $\mathscr{F}$ to it
			
			\begin{align*}
				\mathscr{F}\left(\mqty[x_1 \\ x_2 \\ x_3]\right) &= \mqty[2x_1 + x_3 \\ -4x_2] \\
				\mathscr{F}\left(\mqty[a+d \\ b+e \\ c+f]\right) &= \mqty[2a + 2d + c+f \\ -4b -4e]
			\end{align*}
			
			If we transform $\vb v$ and $\vb w$ and then add them, we get
			
			\begin{align*}
				\mathscr{F}\left(\mqty[a\\b\\c]\right) = \mqty[2a + c\\ -4b] &\mbox{ and } \mathscr{F}\left(\mqty[d \\ e \\ f]\right) = \mqty[2d +f \\ -4e] \\
				\implies \mathscr{F}(\vb v) + \mathscr{F}(\vb w) = \mqty[2a + c\\ -4b] &+ \mqty[2d +f \\ -4e] = \mqty[2a + 2d + c+f \\ -4b -4e]
			\end{align*}
			
			And if we check for scalar multiplication, 
			
			\begin{align*}
				\mathscr{F}(\alpha \vb v) = \mathscr{F}\left(\mqty[\alpha a\\ \alpha b\\ \alpha c]\right) = \mqty[2\alpha a + \alpha c \\ -4\alpha b] = \mqty[\alpha(2a + c \\ -4\alpha b] = \alpha \mqty[2a + c \\ -4b] = \alpha \mathscr{F}(\vb v)
			\end{align*}
			
			And thus the transformation is linear
			
			\item We can perform the above a bit less verbosely with $\mathscr{G}$ by checking the following
			
			\begin{align*}
				\mathscr{G}(\alpha \vb v  + \beta \vb w) = \alpha \mathscr{G}(\vb v) + \beta \mathscr{G}(\vb w)
			\end{align*}
			
			Doing the above a bit less verbosely, we get
			
			\begin{align*}
				\mathscr{G}\left(\mqty[\alpha a + \beta d \\ \alpha b + \beta e  \\ \alpha c + \beta f]\right) = \mqty[4\alpha a + 4\beta d + 2\alpha b + 2\beta e \\ 0 \\ \alpha a + \beta d + 3\alpha c + 3\beta f - 2]
			\end{align*}
			
			And now first transforming the vectors and then adding them
				
			\begin{align*}
				\mathscr G \left(\mqty[a \\ b \\ c]\right) &= \mqty[4a + 2b \\ 0 \\ a + 3c -2] \\
				\mathscr G \left(\mqty[d \\ e \\ f]\right) &= \mqty[4d + 2e \\ 0 \\ d + 3f - 2]
			\end{align*}
			
			Scaling and adding by $\alpha$ and $\beta$ respectively
			
			\begin{align*}
				\alpha \cdot \mathscr G (\vb v) &= \mqty[\alpha 4a + \alpha 2b \\ 0 \\ \alpha a + 3 \alpha c - \alpha 2] \\
				\beta \cdot \mathscr G (\vb w) &= \mqty[\beta 4a + \beta 2b \\ 0 \\ \beta a + 3 \beta c - \beta 2]
			\end{align*}
			
			As we can see, there is no $-2$ term in the third coordinate, and there is an extra $-2 \alpha -2 \beta$ in the sum $\alpha \mathscr G(\vb v) + \beta \mathscr G(\vb w)$, thus the group structure of vector addition is not preserved. Therefor, the transformation is \emph{not} linear.
			
			\end{enumerate}
			
			\item The rotation matrix for two dimensions is as follows
				
				\begin{gather*}
					R(\theta) = \mqty[ \cos(\theta) & -\sin(\theta) \\ \sin(\theta) & \cos(\theta)]
				\end{gather*}
				
				If we perform a rotation by $\theta_1$ first, and then a rotation by $\theta_2$, that amounts to the same as performing a rotation by $\theta_1 + theta_2$. Similarly, if we perform a rotation by $\theta$ first and then a rotation by $-\theta$, that amounts to doing nothing at all to the plane. Thus the following two properties should hold.
			
			\begin{enumerate}[label=(\roman*)]
			
			\item $R(\theta_1) R(\theta_2) = R(\theta_1 + \theta_2)$. To verify this, we can perform the calculation
			
			\begin{gather*}
				\mqty[ \cos(\theta_1) & -\sin(\theta_1) \\ \sin(\theta_1) & \cos(\theta_1)] \mqty[ \cos(\theta_2) & -\sin(\theta_2) \\ \sin(\theta_2) & \cos(\theta_2)] \\
				= \mqty[ \cos(\theta_1)\cos(\theta_2) - \sin(\theta_1)\sin(\theta_2) & -\cos(\theta_1)\sin(\theta_2) - \sin(\theta_1)\cos(\theta_2) \\ \sin(\theta_1)\cos(\theta_2) + \cos(\theta_1)\sin(\theta_2) & -\sin(\theta_1)\sin(\theta_2) + \cos(\theta_1)\cos(theta_2)]
			\end{gather*}
			
			But using the angle sum identities, this changes to
			
			\begin{gather*}
				\mqty[cos(\theta_1 + \theta_2) & -\sin(\theta_1 + \theta_2) \\ \sin(\theta_1 + \theta_2) & \cos(\theta_1 + \theta_2)]
			\end{gather*}
			
			Which is $R(\theta_1 + \theta_2)$. Thus the property $R(\theta_1) R(\theta_2) = R(\theta_1 + \theta_2)$ holds. \emph{Note}: one could take this property to be true without verifying it through calculation, and thus derive the angle sum formulas through these.
			
			\item $R(\theta)R(-\theta) = \mathbb{I}$. We can verify this by calculating as well
			
			\begin{gather*}
				\mqty[ \cos(\theta) & -\sin(\theta) \\ \sin(\theta) & \cos(\theta)] \mqty[ \cos(-\theta) & -\sin(-\theta) \\ \sin(-\theta) & \cos(-\theta)]
			\end{gather*}
			
			Since cosine is even and sine is odd,
			
			\begin{gather*}
				\mqty[ \cos(\theta) & -\sin(\theta) \\ \sin(\theta) & \cos(\theta)] \mqty[ \cos(\theta) & \sin(\theta) \\ -\sin(\theta) & \cos(\theta)] \\
				= \mqty[ \cos^2(\theta) + \sin^2(\theta) & \cos(\theta)\sin(\theta) - \cos(\theta)\sin(\theta) \\ \cos(\theta)\sin(\theta) - \cos(\theta)\sin(\theta) & \cos^2(\theta) + \sin^2(\theta)] = \mqty[ 1 & 0 \\ 0 & 1]
			\end{gather*}
			
			Thus the property is verified
			
			\end{enumerate}
			
		\end{enumerate}
		
	\section{The Metric and the Determinant}
	
		\begin{enumerate}[label=(\alph*)]
		
		\item The metric is a special function from $V \times V$ to $\mathbb F$ for some vector field $V$ over $\mathbb F$. The properties of the metric that were discussed in the lecture are
		
		\begin{enumerate}[label=(\arabic*)]
			\item \emph{Biliniarity} -- For any two vectors $\vb v, \vb w \in V$, the metric must be linear in both $\vb v$ and $\vb w$. Mathematically, if our metric is $g(\vb v, \vb w)$, then for the scaling property, the following must be true
			
			\begin{gather*}
				g(\lambda \vb v, \vb w) = \lambda g(\vb v, \vb w) \\
				g(\vb v, \lambda \vb w) = \lambda g(\vb v, \vb w)
			\end{gather*}
			
			And for the additive property, the following must be true
			
			\begin{gather*}
				g(\vb v + \vb u, \vb w) = g(\vb v, \vb w) + g(\vb u, \vb w) \\
				g(\vb v, \vb w + \vb u) = g(\vb v, \vb w) + g(\vb v, \vb u)
			\end{gather*}
			
			\item \emph{Symmetry} -- For any two vectors $\vb v, \vb u \in V$, $g(\vb v, \vb u) = g(\vb u, \vb v)$
			
			\item \emph{Positive-definite} -- For a vector $\vb v \in V$, $g(\vb v, \vb v) > 0$ except for the zero vector, where $g(\vb 0, \vb 0) = 0$
			
		\end{enumerate}
		
		\begin{enumerate}[label=(\roman*)]
		
			\item $g(\vb v, \vb w) = \sqrt{-(v_1 - w_1)^2 + (v_2 - w_2)^2}$. Checking this for biliniarity, we need to check for additivity and scaling. First, checking for scaling. Let $\lambda \in \mathbb F$,
			
			\begin{align*}
				g(\lambda \vb v, \vb w) &= \sqrt{-(\lambda v_1 - w_1)^2 + (\lambda v_2 - w_2)^2}
			\end{align*}
			
			Since there is no method to factor out a $\lambda$, the map $g$ is not bilinear, and thus cannot be a metric
			
			\item $g(\vb v, \vb w) = \sqrt{(v_1 - w_1)^3 + (v_2 - w_2)^3}$. The same argument as above would hold. If we multiply the components of $\vb v$ by some $\lambda$, we cannot factor it out giving us $\lambda g(\vb v, \vb w)$. Thus this also cannot be a metric
			
			\item $g(\vb v, \vb w) = \sqrt{(v_1 - w_1)^4 + (v_2 - w_2)^4}$. Same as the above two, this map is also not bilinear since it fails satisfy scalability. Thus it is not a metric
		
		\end{enumerate}
		
		\item \begin{enumerate}[label=(\roman*)] 
		
			\item The transformation $S_1$ takes $\hat e_1$ to $\smqty[1/2 \\ -1/2]$ and $\hat e_2$ to $\smqty[1/2 \\ 3/2]$. Visually it does the following
		
		\begin{figure}[h!]
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.14]{normal}
			\caption{}
		\end{subfigure}
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.14]{transformed}
			\caption{}
		\end{subfigure}
		
		\label{fig:transformation}
		\caption{(a) Shows the plane before transformation and (b) shows the plane after $S_1$ acts on each point. The highlighted square is the unit square spanned by the basis vectors $\hat e_1$ and $\hat e_2$}
		\end{figure}
		
		To calculate the area, we can see that the triangle inside the parallelogram has a height of $1/2$, as the first component of both basis vectors gets scaled by $1/2$, and the base of the parallelogram (i.e, it's diagonal) has a length of $3/2 - (-1/2) = 2$. Thus the area of the triangle is $1/2 \cdot 1/2 \cdot  2 = 1/2$, and since the parallelogram's area is twice that, the area of the parallelogram is $1$.
		
		\item The matrix representing this transaction is the following
		
		\begin{align*}
			S_1 &:= \mqty[ 1/2 & 1/2 \\ -1/2 & 3/2 ]
		\end{align*}
		
		The determinant is $\det(S_1) = 1/2 \cdot 3/2 - 1/2 \cdot (-1/2) = 4/4 = 1$, which is the same as what we get visually.
		
		\end{enumerate}
		
	\section{Programming}
	
		The following is the complete code for this section
		
		\lstinputlisting[language=Python]{code.py}
		
		The code before the if statement on line 23 defines the functions for parts (b) and (c), and the code in lines 27 to 29 gets the vector  as input from the user for part (a). The rotate\_vector function uses NumPy's dot function to perform matrix-vector multiplication and return the rotated vector. The rotation matrix is constructed as a 2x2 numpy array using NumPy's trigonometric functions. The user is asked for the angle to rotate by in degrees and it is converted to radians using NumPy's radians function, and then passed to our rotate\_vector function. Finally the code checks if the rotated vector and the original vector have the same length using the vector\_length, returns the appropriate result. A sample output from the above is as follows
		
		\begin{lstlisting}
[cocoa@arch-os Assignment4]$ ./code.py
Enter first coordinate:  2
Enter second coordinate: 5
Vector entered is [2. 5.]
Enter amount to rotate in degrees: 30
Rotated vector is: [-0.76794919  5.33012702]
The vector's length is preserved during the transformation\end{lstlisting}
		

		\end{enumerate}
			

\end{document}