#!/usr/bin/env python

import numpy as np

def rotate_vector(vec, theta):
    """
    %param vec - 2D array representing a vector in R2
    %param theta - angle by which vec should be rotated in radians

    Rotate a vector and return the new coordinates
    """

    R = np.array([ [np.cos(theta), -np.sin(theta)],
                   [np.sin(theta),  np.cos(theta)] ])
    
    # Use the np.ndarray.dot function to do matrix-vector multiplication
    return R.dot(np.array(vec))

def vector_length(vec):
    # Round the magnetude to 3 decimal places to avoid floating point errors
    return np.round(np.sqrt( vec[0]**2 + vec[1]**2 ),3)

if __name__ == "__main__":

    v = np.zeros(2,float)

    # Ask the user for both the coordinates
    v[0] = float(input("Enter first coordinate:  "))
    v[1] = float(input("Enter second coordinate: "))

    print("Vector entered is",v)
    
    # Get the angle and convert to radians
    angle = float(input("Enter amount to rotate in degrees: "))
    theta = np.radians(angle)
    
    v_prime = rotate_vector(v, theta)
    print("Rotated vector is:", v_prime)

    if vector_length(v) == vector_length(v_prime):
        print("The vector's length is preserved during the transformation")
    else:
        print("The vector's length is not preserved during the transformation")


