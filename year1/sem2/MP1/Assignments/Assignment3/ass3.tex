\documentclass[12pt]{article}

\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[english]{babel}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{subcaption}

\graphicspath{ {./Figures/} }

\usepackage{fontspec}
\usepackage{enumitem}

\setmainfont{Utopia}

\pagestyle{fancy}
\fancyhf{}
\rhead{Spring 2020}
\lhead{PHY1110 - Mathematical and Computational Toolkit}

\begin{document}
	
	\begin{center}
		{\LARGE Assignment 3: \\ Affine Spaces in Physics}

		\vspace{1em}
				
		{\large Cocoa}
		
		\vspace {1em}
		
		{\large February 20, 2020}
		
		\vspace{-1em}
		
	\end{center}
	
	\noindent \hrulefill
	
	\section{Affine Transformations}
	
	\begin{enumerate}[label=(\alph*)]
		
		\item \begin{enumerate}[label=(\roman*)]
			\item An object at rest at some point $x_2$ has the following world line shown in figure \ref{fig:1ai}
			
			\item World line of a body moving at constant velocity $v$ is shown in figure \ref{fig:1aii}
			
			\item World line of a body moving at constant velocity $-v$ is shown in figure \ref{fig:1aiii}
			
			\begin{figure}[h]
			\begin{subfigure}{0.3\textwidth}
				\centering
				\includegraphics[scale=0.4]{1ai}
				\caption{World line of Object at Rest}
				\label{fig:1ai}
			\end{subfigure}
			\begin{subfigure}{0.3\textwidth}
				\centering
				\includegraphics[scale=0.4]{1aii}
				\caption{World line of object with velocity $v$}
				\label{fig:1aii}
			\end{subfigure}
			\begin{subfigure}{0.3\textwidth}
				\centering
				\includegraphics[scale=0.4]{1aiii}
				\caption{World line of object with velocity $-v$}
				\label{fig:1aiii}
			\end{subfigure}
			\caption{World lines of objects at rest and at constant velocity}
			\end{figure}
		\end{enumerate}
		
		\item \begin{enumerate}[label=(\roman*)]
			
			\item Choosing a different unit for the x-axis is an affine transformation as it leaves parallel lines parallel and straight. It is simply the transformation $x^\prime = \alpha x$
		
			
			\item Similarly, choosing a different unit of time is an affine transformation because it simply scales points by some constant factor $\beta$, and thus leaves parallel lines parallel and straight. It is the transformation $t^\prime = \beta t$
			
			\end{enumerate}
			
		\item \begin{enumerate}[label=(\roman*)]
		
			\item The squirrel's world line from it's perspective is the green world line. From it's frame of reference, it will always be at $x=0$ and stationary, however, in our frame of reference (in black), the squirrel is at $x=d$ (Figure \ref{fig:1ci})
			
			\item Since the train starts moving, the squirrel's world line according to our frame of reference (in black) will have a slope of $-v$ now. The squirrel is still stationary according to it's frame of reference, thus the green line stays vertical at $x=0$ (Figure \ref{fig:1cii})
			
			\item The transformation that takes us from the $(x,t)$ in the train's frame of reference to the $(x^\prime, t^\prime)$ in the squirrel's frame of reference is the following one
			
			\begin{align*}
				x^\prime &= x - vt \\
				t^\prime &= t
			\end{align*}
			
			This is an affine transformation, as it leaves straight lines straight and parallel. It simply shifts the $x$ coordinate by $-vt$ term, and leaves the $t$ coordinate unchanged
			
			\item Since the distance $x$ covered in some time period $t$ is constantly increasing, this should be a curved line as shown in Figure \ref{fig:1civ}
			
			\item We can visually see all the lines stay straight, parallel and evenly spaced, as all points are shifted by a factor of $-vt$ (Figure \ref{fig:1cv})
			
		\end{enumerate}
		
		\begin{figure}[h]
		\begin{subfigure}{0.3\textwidth}
			\centering
			\includegraphics[scale=0.4]{ci}
			\caption{}
			\label{fig:1ci}
		\end{subfigure}
		\begin{subfigure}{0.3\textwidth}
			\centering
			\includegraphics[scale=0.4]{cii}
			\caption{}
			\label{fig:1cii}
		\end{subfigure}
		\begin{subfigure}{0.3\textwidth}
			\centering
			\includegraphics[scale=0.4]{civ}
			\caption{}
			\label{fig:1civ}
		\end{subfigure}
		\begin{subfigure}{0.3\textwidth}
			\centering
			\includegraphics[scale=0.4]{cv}
			\caption{}
			\label{fig:1cv}
		\end{subfigure}
		\caption{World lines of two frames of reference, and an accelerating frame (c)}
		\label{fig:1c}
		\end{figure}
	
				
	\end{enumerate}
	
	\pagebreak
	
	\section{Bases in $\mathbb{R}^2$}
	
	\begin{enumerate}[label=(\alph*)]
		
		\item \begin{enumerate}[label=(\roman*)]
		
			\item In order to show that $\hat e_1^\prime$ and $\hat e_2^\prime$ are linearly independent, we need to show that there is no $\alpha \in \mathbb{R}$ such that $\hat e_1^\prime = \alpha \hat e_2^\prime$. If we unravel the two simultaneous linear equations that $\alpha$ would have to satisfy, we would get $\alpha = 1$ from the first, and $\alpha = -1$ from the second. Since $\alpha$ cannot take on two distinct values at the same time, $\hat e_1^\prime$ is linearly independent of $\hat e_2^\prime$
			
			\begin{align*}
				\hat e_1^\prime &= \alpha \cdot  \hat e_2^\prime \\
				\left[\begin{matrix} 1 \\ 1 \end{matrix}\right] &= \alpha \left[\begin{matrix} 1 \\ -1 \end{matrix}\right] \\
				1 = \alpha &\mbox{ and } 1 = -\alpha
			\end{align*}
		
			\item Given some arbitrary vector $\vec v = \left(\begin{smallmatrix} a \\ b \end{smallmatrix}\right)$ in the standard basis $\{\hat e_1, \hat e_2\}$ in $\mathbb{R}^2$, we can compute what it would be under the new basis $\{\hat e_1^\prime, \hat e_2^\prime\}$ by applying the change of basis matrix $B$ to the vector $\vec v$, giving us
			
			\begin{align*}
			\left[\begin{matrix} 1 & 1 \\ 1 & -1 \end{matrix}\right]\left[\begin{matrix}a \\ b\end{matrix}\right] = \left[\begin{matrix} a^\prime \\ b^\prime \end{matrix}\right]
			\end{align*}
			
			And thus, $a^\prime = a + b$ and $b^\prime = a - b$
			
			\item $\hat e_1^\prime = \hat e_1 + \hat e_2$ and $\hat e_2^\prime = \hat e_1 - \hat e_2$
			
			\item We can repeat the procedure with the following new basis vectors
			
			\begin{align*}
			\hat e_1^\prime = \left[\begin{matrix} 1 \\ 1 \end{matrix}\right] \ \ \hat e_2^\prime = \left[\begin{matrix}
			0 \\ 1
			\end{matrix}\right]
			\end{align*}
			
			If we check for linear independence, we get $\alpha = 0$ and $\alpha = 1$ from the first and second coordinates, thus these vectors are linearly independent. For any arbitrary vector, we can express it in terms of our new basis using the change of basis matrix
			
			\begin{align*}
			\left[\begin{matrix} 1 & 1 \\ 0 & 1 \end{matrix}\right]\left[\begin{matrix}a \\ b\end{matrix}\right] = \left[\begin{matrix} a^\prime \\ b^\prime \end{matrix}\right]
			\end{align*}
			
			From this, we get that $a^\prime = a$ and $b^\prime = a + b$, which allows us to write down that $\hat e_1^\prime = \hat e_1$ and $\hat e_2^\prime = \hat e_1 + \hat e_2$
		\end{enumerate}
	\end{enumerate}
\end{document}
