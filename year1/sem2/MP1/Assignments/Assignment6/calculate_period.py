#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

G = 6.673e-11        # Newton's Constant in MKS units
M = 1.99e30          # The mass of the Sun in kilograms
ME = 5.97e24         # The mass of the Earth in kilograms
AU = 149597870700.0  # Earth-Sun distance in metres
yr = 365*24*60*60.0  # One year in seconds

planets = ('Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune')
planetDistances = np.array([57.9, 108.2, 149.6, 227.9, 778.6, 1433.5, 2872.5, 4495.1]) * 10e9
planetVelocity = np.array([47.4, 35.0, 29.8, 24.1, 13.1, 9.7, 6.8, 5.4])*1000 # m/s

dt = 10e4
def a(r):
    rx, ry = r[0], r[1]

    rMag = np.linalg.norm(r)
    aMag = -G * M / rMag**2

    return np.array([ aMag * rx, aMag * ry ])

if __name__ == "__main__":

    accuracy = 0.1

    for i, planet in enumerate(planets):
        print("Planet =", planet)

        # Starting position and velocity
        r = np.array(np.array([1., 0.]) * planetDistances[i])
        v = np.array(np.array([0., 1.]) * planetVelocity[i])
        step = 1
        print(r[-1], v)
        starting_angle = np.arctan2(r[-1][1], r[-1][0])
        while True:
            print(r[-1])

            r = np.vstack( r, r[-1] + v[-1] * dt )
            v = np.vstack( v, v[-1] + a(r[-1]) * dt)
            step += 1
            print(r[-1])

            pos_y, pos_x = r[-1][1], r[-1][0]
            angle = np.arctan2(pos_y, pos_x)

            if (step > 100) and ( np.abs(angle - starting_angle) <= accuracy ):
                print("Period =", step*dt)
                exit()
