{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Assignment 6 - One Body Problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part A - Normal Gravity\n",
    "\n",
    "We will integrate Newton's law of gravitational attraction using an initial time step of $dt = 0.01$, since that seems like a reasonable amount of precision, and we integrate from $t = 0$ to $t = 10$ (picked arbitrarily). The generalized gravitational force is given as\n",
    "$$\n",
    "    F_{grav} = -G\\frac{Mm}{\\|\\vec r\\|^{\\beta}} \\hat r\n",
    "$$\n",
    "\n",
    "Where $\\beta \\in \\mathbb R$. In order to simulate normal gravity, we set $\\beta = 2.0$. Equating the above to $F=m\\vec a$, we get\n",
    "$$\n",
    "    \\vec a = -G\\frac{M}{\\|\\vec r\\|^{\\beta}} \\hat r\n",
    "$$\n",
    "\n",
    "Now taking $\\beta = 2$, since $\\hat r = \\frac{\\vec r}{\\|\\vec r\\|}$, and if we define $\\vec r = x \\hat i + y \\hat j$, then $\\vec a = \\ddot x \\hat i + \\ddot y \\hat j$, and from that, we get the following two equations that define the dynamics of our system\n",
    "\n",
    "$$\n",
    "    \\boxed{\\ddot x = -G\\frac{Mx}{\\|\\vec r\\|^3}  \\\\\n",
    "    \\ddot y = -G\\frac{My}{\\|\\vec r\\|^3}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The quantities we need to keep track of are position, velocity, kinentic and potential energy as functions of time. The positions and velocities can be tracked while numerically integrating the above. The potential energy can be found by integrating the force from $\\infty$ to $\\vec r$, and the result is simply to subtract 1 from the power of $\\vec r$ in $F_{grav}$ and divide by $(\\beta - 1)$, giving us\n",
    "$$\n",
    "    U_{grav} = -G \\frac{Mm}{(\\beta - 1)\\|\\vec r\\|^{\\beta-1}}\n",
    "$$\n",
    "\n",
    "And for $\\beta = 2$, this gives us\n",
    "$$\n",
    "    U_{grav} = -G \\frac{Mm}{\\|\\vec r\\|}\n",
    "$$\n",
    "\n",
    "The kinetic energy $K$ is simply $\\frac{1}2 m\\|\\vec v\\|^2$, and thus the total energy that we get is\n",
    "$$\n",
    "    E_{Total} = U_{grav} + K = \\frac{1}2 m\\|\\vec v\\|^2 -G \\frac{Mm}{\\|\\vec r\\|}\n",
    "$$\n",
    "\n",
    "We can save some computational time by considering energy per unit mass, $E = \\frac{E_{Total}}m$\n",
    "$$\n",
    "    \\boxed{E = \\frac{1}2 \\|\\vec v\\|^2 -G \\frac{M}{\\|\\vec r\\|}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = 6.673e-11        # Newton's Constant in MKS units\n",
    "M = 1.99e30          # The mass of the Sun in kilograms\n",
    "GM = G * M           # Defined for ease\n",
    "AU = 149597870700.0  # Earth-Sun distance in metres\n",
    "yr = 365*24*60*60.0  # One year in seconds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def potential(m, rVec):\n",
    "    r = np.linalg.norm(rVec)\n",
    "    return -G*M*m/r\n",
    "\n",
    "def kinetic(m, vVec):\n",
    "    v = np.linalg.norm(vVec)\n",
    "    return 1/2 * m * v**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": [
    "def aVec(rVec):\n",
    "    x = rVec[0]                   # Extracting x and y\n",
    "    y = rVec[1]                   # from rVec\n",
    "    \n",
    "    RCube = (x**2 + y**2)**(1.5)  # Computing r**3 to simplify calculations\n",
    "    \n",
    "    ax = -GM*x/RCube              # Computing the acceleration vector's components\n",
    "    ay = -GM*y/RCube              # from Newton's Laws\n",
    "    \n",
    "    return np.array([ax,ay])      # Return a \"vector\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_initial = 0                   # Initial time\n",
    "t_final   = 1*yr                # Final time\n",
    "dt = 1000.0                     # Time-step of 1 hour\n",
    "N = int((t_final-t_initial)/dt) # Number of time-steps \n",
    "\n",
    "r0 = 1*AU                       # Magnitude of distance from the sun\n",
    "v0 = 2*np.pi*r0/yr              # Magnitude of velocity for a circular orbit\n",
    "r0Vec = r0*np.array([1.0,0.0])  # The initial position vector, along x-axis\n",
    "v0Vec = v0*np.array([0.0,1.0])  # The initial velcoity vector, along y-axis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [],
   "source": [
    "rVecs = np.zeros((N+1,2),float)       # An array of 2D vectors of length N+1 for position\n",
    "vVecs = np.zeros((N+1,2),float)       # An array of 2D vectors of length N+1 for velocity\n",
    "t     = np.zeros(N+1,float)           # An array of length N+1 for time\n",
    "\n",
    "rVecs[0] = r0Vec                      # Setting the initial position to r0Vec\n",
    "vVecs[0] = v0Vec + aVec(rVecs[0])*dt/2 # Setting the initial velocity using the leapfrog method\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we will not be tracking the total energy of the system since that is just the sum of the kinetic and potential energy, so for the energy array, we can simply sum the other two arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "potentialArray = np.zeros(N+1,float)\n",
    "kineticArray = np.zeros(N+1, float)\n",
    "\n",
    "potentialArray[0] = potential(ME, r0Vec)\n",
    "kineticArray[0] = kinetic(ME, v0Vec)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(1,N+1):\n",
    "    t[i]     = i*dt\n",
    "    rVecs[i] = rVecs[i-1] + vVecs[i-1]*dt\n",
    "    vVecs[i] = vVecs[i-1] + aVec(rVecs[i])*dt\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYoAAAERCAYAAABl3+CQAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8li6FKAAAgAElEQVR4nO3dd3zV5d3/8dcne+8BWYS9d9guHBWtiosqtnVLb63j7rpvO257633/2lqrbW1d1CrurRUVRQUVZAcZsgkQSAhkkj3POdfvjxy8KQ0hgeRcZ3yej0ce5JzzTc6bA8n7XN9xXWKMQSmllDqRINsBlFJKeTctCqWUUp3SolBKKdUpLQqllFKd0qJQSinVKS0KpZRSnfLbohCRZ0SkTES2dGHbs0TkKxFxiMjVxz32kYhUi8j7vZdWKaW8l98WBbAAmNXFbQ8ANwIvd/DYQ8D3eyaSUkr5Hr8tCmPMMqDq2PtEZKB7hLBeRJaLyDD3toXGmM2Aq4PvswSo80hopZTyQiG2A3jYfODfjDG7RWQK8DhwruVMSinl1QKmKEQkBpgOvCEiR+8Ot5dIKaV8Q8AUBe272aqNMeNsB1FKKV/it8cojmeMqQX2icgcAGk31nIspZTyeuKvs8eKyCvAOUAKUAr8GlgKPAH0BUKBV40xD4jIJOAdIBFoBg4bY0a6v89yYBgQA1QCtxhjFnv2b6OUUvb4bVEopZTqGQGz60kppdSp8cuD2SkpKSY3N9d2DKWU8hnr16+vMMakdvSYXxZFbm4u+fn5tmMopZTPEJH9J3pMdz0ppZTqlBaFUkqpTmlRKKWU6pQWhVJKqU5pUSillOqU1aI42eJCInKOiNSIyEb3x32ezqiUUoHO9umxC4C/As93ss1yY8wlnomjlFLqeFaLwhizTERybWZQqic0tjooq22hpqmN2ua29j+bHDS1OXG6XDhcBofT4HQZQoOF8JBgwkODCA8JIiI0mMSoMJKiw0iMDiMpKozIsGDbfyWlvmF7RNEV00RkE1AC/NQYs7WjjURkHjAPICcnx4PxVCAwxnCoppmCsnoKyurZU15PSXUTh2qaOVTTTE1TW48+X2x4CJmJkWQnRZGVGEl2YhQD02IY1ieWtNhwjllTRale5+1F8RXQzxhTLyIXA/8ABne0oTFmPu0r2JGXl6czHapTZoyhsLKRTUXVbCquZnNxDTsO1dLQ6vxmm7iIEPcv8Sgm5SbRNyGC9NgIEqJCiY8MJS4ylLiIUCJDgwkJFoKDhJCg9j8dLkOLw0VLm5MWh4vGVifVja1UNbRypLGVyoZWympbKD7SyIHKRlYUVNB4zHMnRIUyND2W4X3jGJ+TwIScRLISI7U8VK/x6qJwryFx9PNFIvK4iKQYYyps5lL+xRjD7rJ6VhZUsHJPJWv2VX0zQogIDWJURjxXT8xiUHosg1JjGJQWQ0pM2Cn/Yg4NFkKDg4gJ79qPnzGGqoZWdpfVs/NwHTsO17GrtI7X84tYsLIQgNTYcCbkJDClfzJnDUllYGq0FofqMV5dFCLSByg1xhgRmUz7WVqVlmMpP1Df4mDZrnI+3VbKst0VVNS3AJCTFMWskX0Yn5PA2OwEBqfFEBJs9yxyESE5JpzkmHCmDkj+5n6H08XO0jq+OlDNV/uPkL+/isVbSwHIiI/gzMGpnD00lbOHpBLdxVJSqiNW16M4weJCoQDGmCdF5E7gdsABNAE/NsasPNn3zcvLMzopoDrekYZWPtxymI+3HWZlQSWtTheJUaGcNSSVGQNTmDYwmeykKNsxT0tRVSPLdpezfFcFK/ZUUNfsIDwkiLOGpHLRqD6cNzyd+MhQ2zGVFxKR9caYvA4f88eFi7Qo1FEtDidLt5fx9oaDfL6zjDanIScpigtGpPOtEelM7JdofcTQWxxOF+v3H+HDLYf5aMthDtc2ExosnDM0jTkTs5g5LI1QP/27q+7TolABZ3dpHS+s3s8/NhykttlBWmw4s8dlcPn4TEb0jQu4/fcul2FjcTUfbD7EuxtLqKhvISUmjCvGZ/KdvGwGp8fajqgs06JQAaHN6eKTbaU8v6qQ1XurCAsJ4uJRfbhyQhYzBqUQHBRY5XAibU4XX+ws5/X8IpbuKMPhMswYlMzNM/ozc2gaQfo6BSQtCuXXGlsdvLzmAH//ch+HaprJTIjke1P7cc2kbJKiw2zH82oV9S28tq6IF1bt53BtM7nJUdwwPZdrJmUTFaYHwAOJFoXyS9WNrSxYWciClYVUN7YxpX8St505gJnD0nT00E1tThcfbTnMsyv28dWBapKiw7jtzAF8f1q/Lp/Gq3ybFoXyK3XNbTy9fB9PL99LQ6uT84encfs5g5jYL9F2NL+QX1jFo0sLWLarnISoUG49oz83zuivheHntCiUX2hxOHlx9QEe+6yAqoZWLh7dh7vPG8ywPnG2o/mlDQeO8JelBSzdUUZKTBj3nD+EuZOy/fYssUCnRaF8mjGGRV8f5jeLtnOwuokzBqXwswuHMjY7wXa0gLCxqJrfLNrO2n1VDEyN5t6LhnP+8LSAO3PM32lRKJ+1u7SOXy/cyso9lYzoG8cvLh7OGYNTbMcKOMYYPtlWyu8+2sHe8gbOGJTC/1w+iv4p0bajqR6iRaF8TkOLgz9+sosFKwuJDg/hpxcO5brJOXqQ2rI2p4uXVu/n4Y930eJwcfs5A7n9nIFEhOq06L5Oi0L5lBUFFfznW5s5WN3EtZOy+dmFw/Q0Vy9TVtvM/36wnYWbSshNjuK3V45h2sDkk3+h8lqdFYUelVJeo665jZ+//TXffXoNYcFBvPGDafz2yjFaEl4oLS6CR+eO54VbJgMw92+ruf+9rTS3OU/ylcoX6fluyius2VvJj17byOHaZuadNYAfXzBEd2f4gDMHp7LonjN58MMdPLuikC92lfPwnLGMz9FTlf2JjiiUVQ6niz9+sou5f1tNeGgwb94+nV9cPFxLwodEhYVw/+xRvHTrFJpbnVz1xEr+smQ3Lpf/7dYOVFoUypqS6iau+9sa/rxkN5ePz+S9u85ggr4T9VkzBqXw0Y/O4tKxGTz8yS5ueHYtle51PpRv06JQVqwoqODbjy5na0kNf7xmLI98Z5xe+esH4iJC+dM14/jNFaNZs6+Kbz/6JesKq2zHUqdJi0J5lDGGv3+5j+ufWUtqbDjv330mV4zPsh1L9SAR4bopObxzx3QiQoO4dv5qXlhVaDuWOg1aFMpjmtuc/OSNTfzP+9s4f3gab98xQy/Y8mMjM+J5764zOGdIKv/17lbue3cLDqfLdix1CnSsrzyiqqGVW55bx4YD1fz4giHcOXOQrnsQAGIjQpl/fR4PfrSD+cv2sre8gceum0B8lC7H6kt0RKF63YHKRq56YiXbSmp58nsTuPu8wVoSASQ4SPjFxcN56OoxrNlXyZVPrKCkusl2LNUNWhSqV31dXMOVT6zgSGMrL982hVmj+tqOpCyZk5fNC7dMoay2haueWElBWZ3tSKqLtChUr1m1p5Jr5q8iPCSYN/9tOhP7JdmOpCybOiCZV38wlTanYc6Tq9hYVG07kuoCLQrVK5bvLuemBWvJTIjknTumMygtxnYk5SVGZsTz1u3TiI0I5bq/rWb13krbkdRJaFGoHvfZzjJueS6f3ORoXp03lbS4CNuRlJfplxzNm7dPIyMhkpueXccaLQuvpkWhetRnO8r4wfPrGZwWwyu3TSU5Jtx2JOWl0mIjePm2KWQkRHDTgnWs3acX5nkrq0UhIs+ISJmIbDnB4yIij4pIgYhsFpEJns6oum7N3kr+7cX1DO0Ty8u3TiVRZ31VJ5EWG8Ert02lT3wENz67lvX7tSy8ke0RxQJgViePXwQMdn/MA57wQCZ1CrYcrOHW5/LJSozkuZsn63nyqsvS4iJ49bappMdFcPOCfHaV6tlQ3sZqURhjlgGdvYWYDTxv2q0GEkREz6/0MgVl9Vz/zFriIkN58dYpun6E6ra0uAiev3kyYSFB3PDMWr3OwsvYHlGcTCZQdMztYvd9/0JE5olIvojkl5eXeyScgor6Fm58di1BAi/eOoW+8ZG2IykflZ0UxXM3Taa+2cENz6ylprHNdiTl5u1F0WXGmPnGmDxjTF5qaqrtOAGhuc3JvOfzqahv4e83TNJ5m9RpG5ERx/zr89hf2ci8F/Jp07mhvIK3F8VBIPuY21nu+5Rlxhj+863NfHWgmke+M46x2Qm2Iyk/MW1gMr+/egxr9lVx/3tbbcdReH9RLASud5/9NBWoMcYcsh1KwV+XFvDuxhJ+duFQLh6th41Uz7p8fCb/dvZAXlx9gBdW77cdJ+BZnT1WRF4BzgFSRKQY+DUQCmCMeRJYBFwMFACNwE12kqpjfbGrnEc+3cUV4zO545yBtuMoP/WzC4eyq7SO+xduZVBqDNMGJtuOFLDEGP9b1zYvL8/k5+fbjuGXDlY3ccmjy0mPi+CdO2YQGaZrW6veU9fcxuWPraCmycGH95xJaqxewNlbRGS9MSavo8e8fdeT8iKtDhc/fOkr2pyGx787QUtC9brYiFAe/+5E6lva+NFrG3G6/O+NrS/QolBd9uBHO9hYVM3vrx7DgFSd5E95xtA+sdx/2Ui+LKjgic8LbMcJSFoUqktWFFS0r3U9rZ8evFYe9528bGaPy+CRT3axrlCn+fA0LQp1UjVNbfz0jU0MSI3m5xcNtx1HBSAR4f9dMZqsxCh+8vomGlsdtiMFFC0KdVK/fncLZXUt/PE74/S4hLImJjyEh64ew4GqRh78cIftOAFFi0J16qMth/nHxhLuPnewXlSnrJsyIJmbZuTy3Kr9rNxTYTtOwNCiUCdU19zGfy/cyvC+cdwxU6+XUN7hPy4cRm5yFP/x5mbdBeUhWhTqhB7+eBeldc389srRhAbrfxXlHSLDgnnwqjEUH2nir0v1LChP0J9+1aFNRdU8t6qQ66f2Y5zuclJeZsqAZK6ckMnflu+loKzedhy/p0Wh/oXLZfjVP7aQFhvOTy4cajuOUh36+UXDiQwN5tcLt+CPM0x4Ey0K9S/e2XCQrw/WcO9Fw4iL0JXqlHdKjQ3nZxcOZUVBJR98rXOF9iYtCvVPGlsdPLR4J2Oz4pk9tsM1opTyGtdN6cewPrH8/qOdtDp07YreokWh/snflu3jcG0zv7pkBEFBYjuOUp0KDhLuvWgYB6oaeXmNTkfeW7Qo1DfK61p48os9XDSqD5Nyk2zHUapLzh6SyvSByTy6tIC6Zl0+tTdoUahvzF+2hxaHk5/pAWzlQ0SEn180nKqGVuYv22s7jl/SolBA+2jihdX7uXxcps4Mq3zO6Kx4vj26L8+uKKSmUUcVPU2LQgHto4lWh4s7zx1kO4pSp+SHMwdR3+LguVWFtqP4HS0KRUW9jiaU7xuREcf5w9N4ZsU+Glp0ao+epEWheGHVfprbXNwxU0cTyrf9cOYgqhvbeHG1ngHVk7QoAlxzm5MXV+/n3GFpDErT0YTybeNzEpk+MJkFKwtxOPW6ip6iRRHg3t14kMqGVm49o7/tKEr1iJtm9OdQTTMfbyu1HcVvaFEEMGMMz3xZyLA+sUwbmGw7jlI94txhaWQlRrJgZaHtKH5DiyKArd1Xxc7SOm4+oz8iehW28g/BQcL10/qxdl8V2w/V2o7jF7QoAthr+UXEhIdwyZi+tqMo1aOuycshIjSIF/Sgdo+wWhQiMktEdopIgYjc28HjN4pIuYhsdH/caiOnP6ptbmPR14e4dGwGUWEhtuMo1aPio0KZNbIP728qobnNaTuOz7NWFCISDDwGXASMAOaKyIgONn3NGDPO/fG0R0P6sfc2ldDc5uKaSdm2oyjVK66amEVts4NPt+tB7dNlc0QxGSgwxuw1xrQCrwKzLeYJKG+uL2Zoeixjs+JtR1GqV0wfmEKfuAjeWl9sO4rPs1kUmUDRMbeL3fcd7yoR2Swib4rICd/+isg8EckXkfzy8vKezupXDlY3seFANZeNy9CD2MpvBQcJV0zIZNnuCsrrWmzH8WnefjD7PSDXGDMG+AR47kQbGmPmG2PyjDF5qampHgvoiz50rwb27dF6EFv5t8vHZeJ0GT7edth2FJ9msygOAseOELLc933DGFNpjDn6VuBpYKKHsvm1D74+xMiMOHJTom1HUapXDUmPITc5io+2aFGcDptFsQ4YLCL9RSQMuBZYeOwGInLsW97LgO0ezOeXSty7nS7W0YQKACLChaP6sGpPJTVNOv34qbJWFMYYB3AnsJj2AnjdGLNVRB4Qkcvcm90tIltFZBNwN3CjnbT+Y8mOMgBmjepjOYlSnnHhyD44XIalO/Tsp1Nl9QR6Y8wiYNFx9913zOc/B37u6Vz+7Iud5WQnRTJAdzupADEuK4G02HA+3V7GFeOzbMfxSd5+MFv1oFaHi5V7Kjh7SKqe7aQCRlCQcMbgFFYWVOByGdtxfJIWRQDJ319FY6uTs4ek2Y6ilEedMSiFI41tbNO5n06JFkUAWb67gpAg0ZliVcCZMSgFgBUFFZaT+CYtigCSX1jFqMx4YsJ1bicVWNLjIhicFsOXWhSnRIsiQLQ4nGwqriGvX6LtKEpZMWVAEhsOVOPU4xTdpkURILYcrKXV4SIvV4tCBaZx2YnUtzjYU15vO4rP0aIIEOv3VwEwsV+S5SRK2TE+JwGAjQeqLSfxPVoUAWJTcQ1ZiZGkxobbjqKUFf2To4mLCGFD0RHbUXyOFkWA2HGolhF942zHUMqaoCBhbHYCm4trbEfxOVoUAaC5zcm+igaGaVGoADesTywFZfV6QLubtCgCwK7SOlwGhveJtR1FKasGp8XS4nBxoKrRdhSfokURAHYcqgPQEYUKeIPTY4D2N0+q67QoAkBhZQMhQUJOUpTtKEpZNTi9fVS9W4uiW7QoAsCBqkayEiMJDtKJAFVgiwkPIT0unMJK3fXUHVoUAaCoqpFsHU0oBUBGQiQl1U22Y/gULYoAcKCqUXc7KeWWqUXRbVoUfq6hxcGRxjayErUolALITIykpLpZ16boBi0KP1dZ3wpASkyY5SRKeYfMhEhanS4qGlpsR/EZWhR+7ugPQ0qMTt2hFEBydPvPwpGGNstJfIcWhZ+rco8okqJ1RKEUQEJUKADVja2Wk/gOLQo/V9XQ/sOQrLuelAIgPrK9KI406oiiq0661JmIZAHXAmcCGUATsAX4APjQGOPq1YTqtNQ0tf8wHP3hUCrQJbpH1zVNOqLoqk6LQkSeBTKB94EHgTIgAhgCzAJ+KSL3GmOW9XZQdWqa25wARIQGW06ilHeIi2j/tVfb5LCcxHecbETxsDFmSwf3bwHeFpEwIKfnY6me0uxwEhIkhAbrXkalAMJC2n8WWp26M6SrOv3tcbQkROSe4x8TkXuMMa3GmIJTfXIRmSUiO0WkQETu7eDxcBF5zf34GhHJPdXnClRNrS4dTSh1jNCg9l97bVoUXdbVt5k3dHDfjafzxCISDDwGXASMAOaKyIjjNrsFOGKMGQT8kfbdX6obmh1OIkJ1NKHUUUFBQkiQ0OrQouiqkx2jmAtcB/QXkYXHPBQLVJ3mc08GCowxe93P9SowG9h2zDazgf92f/4m8FcREWOMXlLZRe2vlE4GqNSxQoODdETRDSc7RrESOASkAA8fc38dsPk0nzsTKDrmdjEw5UTbGGMcIlIDJAMVx38zEZkHzAPIydHDJkcFB4FLe1Wpf9LU5mTJjjJ++e3jd2KojpysKA4YY/YD0060gbe8wzfGzAfmA+Tl5VnP4y2CRbQolDpOZGgw5w1Lsx3DZ5xs5/VnInKXiPzTW3QRCRORc0XkOTo+ftEVB4HsY25nue/rcBsRCQHigcpTfL6AJCK6PrBSx3G4XAQH6bG7rjrZKzULcAKviEiJiGwTkX3AbmAu8CdjzIJTfO51wGAR6e8+zfZaYOFx2yzk/4roamCpN4xefElIkOBw6kum1FGtDhdtTkN0mJ4N2FUn2/X0NvBDY8zjIhJK+7GKJmNM9ek+sfuYw53AYiAYeMYYs1VEHgDyjTELgb8DL4hIAe0Hz6893ecNNDERITS1OXG6jK5wpxTQ2Np+oV10+EknplBuJ3ulngUWi8gC4CFjzKGefHJjzCJg0XH33XfM583AnJ58zkATG9E+dUd9s4P4KJ3GQ6mG1vbZCqLDdUTRVZ0WhTHmDRH5EPgvIF9EXgBcxzz+SC/nU6cp9uh0Bc1tWhRK0b6YF+iIoju68kq1Ag1AOO3XT+jJxz7k6Lw29S06r41SALXuiTJjtCi67GQX3M0CHqH9oPIEY0yjR1KpHhMf2T5T5pEGnSlTKYDyuvbFvNJiIywn8R0nq9RfAnOMMVs9EUb1vD7x7T8Mh2ubLSdRyjuUHS2KOF31satOdoziTE8FUb2jT1x7URyq0aJQCqCsrpmQICEpShfz6iq94sTPRYYFExcRQqmOKJQCoKy2hZSYcIL0dPEu06IIAH3jI3VEoZRb0ZFGMhL0+ER3aFEEgOykKPZXNtiOoZRXKKxoJDcl2nYMn6JFEQAGpkZTWNGocz6pgNfU6uRwbTP9k7UoukOLIgAMTI2h1emi+Iie3awCW6F7ZK0jiu7RoggAA1Lbfyj2luvuJxXYjv4M9Nei6BYtigAwMDUGgF2ldZaTKGXXtkM1hAQJg9JibEfxKVoUASAxOozMhEi2lNTajqKUVVsO1jIoLYaIUJ0QsDu0KALE6Mx4Nhef9uzwSvksYwxbS2oYlRlvO4rP0aIIEKOz4tlf2UhNY5vtKEpZUVbXQkV9K6My4mxH8TlaFAFiTFb7u6jNB3VUoQLThgNHABidlWA5ie/RoggQY7ISEIH1+4/YjqKUFWv2VRERGsRo3fXUbVoUASI+MpSRGXGs2lNpO4pSVqzZW8WEnETCQvTXXnfpKxZApg1IZsOBaprbnLajKOVRNU1tbD9cy+T+Sbaj+CQtigAybWAyrU4XX+nuJxVg1u2rwhi0KE6RFkUAmZSbRHCQsGJPhe0oSnnU57vKiAoLZmK/RNtRfJIWRQCJjQglr18iS7aX2Y6ilMcYY1i6vYwzBqUQHqIX2p0KLYoAc8GIdHYcrqOoSicIVIFhZ2kdJTXNnDsszXYUn6VFEWDOG54OwJLtpZaTKOUZS3e0j6BnalGcMitFISJJIvKJiOx2/9nhjkMRcYrIRvfHQk/n9Ef9U6IZlBbDJ1oUKkAs3nKYMVnxpMfpqnanytaI4l5giTFmMLDEfbsjTcaYce6PyzwXz79dODKd1XurqKhvsR1FqV5VWNHApuIaLhnT13YUn2arKGYDz7k/fw643FKOgDR7XCZOl+H9TSW2oyjVq95z/x+/ZEyG5SS+zVZRpBtjDrk/Pwykn2C7CBHJF5HVItJpmYjIPPe2+eXl5T0a1t8MSY9lRN843tmoRaH823ubS5icm0RGQqTtKD6t14pCRD4VkS0dfMw+djtjjAFOtJhzP2NMHnAd8CcRGXii5zPGzDfG5Blj8lJTU3vuL+KnLh+fwaaiavZV6Kp3yj9tLalhV2k9l47V3U6nq9eKwhhzvjFmVAcf7wKlItIXwP1nhyf2G2MOuv/cC3wOjO+tvIHmsrGZiMBb64ttR1GqV7y6toiwkCAuHau7nU6XrV1PC4Eb3J/fALx7/AYikigi4e7PU4AZwDaPJfRzfeIjmDk0jVfXFdHqcNmOo1SPamx18I8NB7lkdF8SosJsx/F5torid8AFIrIbON99GxHJE5Gn3dsMB/JFZBPwGfA7Y4wWRQ/6/tR+VNS38PG2w7ajKNWj3t90iLoWB3On5NiO4hdCbDypMaYSOK+D+/OBW92frwRGezhaQDlrSCrZSZG8uHq/nhWi/MpLaw8wKC2GPJ3bqUfoldkBLDhIuG5yP1bvrWJXaZ3tOEr1iPzCKjYVVfP9qf0QEdtx/IIWRYC7ZlI2EaFBzF+213YUpXrEU8v2khAVypy8LNtR/IYWRYBLig7j2kk5/GPDQUqqm2zHUeq07Cmv59PtpVw/tR9RYVb2rPslLQrFrWf2B+Dp5fssJ1Hq9Dy9fC9hwUFcPz3XdhS/okWhyEqM4rJxGbyy9gBVDa224yh1SoqPNPLm+mKunphFSky47Th+RYtCAXD72QNpdjh56os9tqModUr+urQAQfjhzEG2o/gdLQoFwOD0WK4Yl8mClYUcrmm2HUepbimsaOCN9cVcNyVH53XqBVoU6hs/umAILmP485LdtqMo1S2PLtlNaLBwx8wTTgenToMWhfpGdlIU103O4fX8Ip0sUPmMrSU1vLPxIDdMyyUtVhcn6g1aFOqf3HnuYMJDgvjNou22oyh1UsYY7n9vG4lRYdyhxyZ6jRaF+iepseHcde5gPtlWyhe7dF0P5d0WfX2Ytfuq+Mm3hhAfGWo7jt/SolD/4uYzcumfEs39C7fqzLLKazW3OfnNou0M6xPLtZN08r/epEWh/kV4SDD3XTqCvRUNPLNCL8JT3umxzwo4WN3Ery8dSXCQzunUm7QoVIdmDk3j/OHp/PnT3RyobLQdR6l/sv1QLU98vocrJ2QybWCy7Th+T4tCndADs9vfqf3nW5tpX7FWKfucLsO9b20mPjKU//r2CNtxAoIWhTqhjIRIfnHxcFbtreSVtUW24ygFwIKVhWwqruG+S0eQGK2r13mCFoXq1NzJ2UwfmMxvFm3X2WWVdQVl9Ty0eAczh6Zyma6F7TFaFKpTIsLvrhyDyxh+/PpGnC7dBaXsaHW4uOfVDUSGBvPgVWN0USIP0qJQJ5WTHMUDs0exem8Vj39WYDuOClAPf7KTrSW1PHjVGNLi9ApsT9KiUF1y1YRMZo/L4E9LdpNfWGU7jgowKwsqmL9sL9dNyeFbI/vYjhNwtChUl4gI/3v5KDITIrnn1Y1UN+q6FcozDtc0c/erG+ifEs2vvj3cdpyApEWhuiw2IpS/zB1PeV0Ld72yQY9XqF7X6nBx+0vraWx18tT3JuryppZoUahuGZudwAOzR7J8dwW/X7zDdhzl5/73g21sOFDNQ1ePZXB6rO04AUvrWXXbtZNz+PpgDU99sZdRGfFcqqcpql7wen4Rz6/az21n9ufbY/rajhPQrIwoRGSOiGwVEZkaRH4AAA81SURBVJeI5HWy3SwR2SkiBSJyryczqs79+tKR5PVL5D/e3Mzm4mrbcZSfWVlQwS/e/poZg5L5z1nDbMcJeLZ2PW0BrgSWnWgDEQkGHgMuAkYAc0VEr9f3EmEhQTz+vQkkx4Rx84J1Oh+U6jG7S+v4wYvr6Z8SzePfnUhIsO4ht83Kv4AxZrsxZudJNpsMFBhj9hpjWoFXgdm9n051VVpsBAtumozDZbjh2bVUNeiZUOr0lNe1cNOCdUSEBvPsTZN0jQkv4c1VnQkcO8FQsfu+DonIPBHJF5H88nJdcMdTBqXF8PT1eRysbuKW59bR1Oq0HUn5qJrGNq5/Zi2V9a38/YY8shKjbEdSbr1WFCLyqYhs6eCjV0YFxpj5xpg8Y0xeampqbzyFOoG83CQevXYcm4qque35fJrbtCxU99S3OLhxwVr2lNXz5PcnMiYrwXYkdYxeKwpjzPnGmFEdfLzbxW9xEMg+5naW+z7lhWaN6svvrx7LlwUV3P7ielocWhaqa5rbnNz2XD6bi2t4dO54zh6ib/S8jTfveloHDBaR/iISBlwLLLScSXXi6olZ/OaK0Xy2s5y7Xt5Am1OXUVWda25z8oMX1rN6XyUPzxnLrFE6PYc3snV67BUiUgxMAz4QkcXu+zNEZBGAMcYB3AksBrYDrxtjttrIq7ruuik53H/ZSD7eVsodL32lu6HUCTW0OLjp2XUs213O764czeXjT3gIUlkm/rhyWV5ensnPz7cdI6A9t7KQXy/cyvSBycy/Po+YcL22U/2fmqY2bnp2LZuKa3h4zlgtCS8gIuuNMR1e1+bNu56UD7thei6PfGcsa/ZV8d2n1+gkguob5XUtfPfp1Xx9sIbHrhuvJeEDtChUr7lyQhZPfHcC20tqmfPkKoqq9KK8QFdQVs+VT6ygoKye+d/PY9YonZrDF2hRqF71rZF9WHDzJEprm7ni8RVsOHDEdiRlyZq9lVz1xEqaWp28Om8aM4el2Y6kukiLQvW66QNTePuOGUSFhXDt/NV8sPmQ7UjKw97ZUMz3/76WlJgw3rljBuOy9ToJX6JFoTxiUFoM//jhDEZnxvPDl7/ij5/swqXrWfi9NqeL+9/byo9e28T4nATevn0G2Ul6xbWv0aJQHpMUHcZLt03h6olZ/HnJbm5csE7nh/Jj7Qet1/DsikJuntGfF2+dQnyUzt3ki7QolEeFhwTz0NVj+O2Vo1m9p5JL//IlG4t0mnJ/s3ZfFZf8ZTmbi6v50zXjuO/SEYTqLLA+S//llMeJCHMn5/DW7dMRgTlPruSpL/bo0qp+wOF08cjHO7l2/ioiQoN5+/YZevqrH9CiUNaMzorn/bvO4Nxhafz2wx1c97fVFB/RU2h91YHKRuY8tYpHlxZw5YQsPrj7TEZkxNmOpXqAFoWyKiEqjCe/N5GHrh7D1pJaLvrTct7+qhh/nDHAX7lchhdX7+fiR5dTUFbPX+aO5w9zxurV+H5E/yWVdSLCnLxspg5I5kevbeTHr2/i3Y0l/O/lo/QMGS+3t7yee9/+mrX7qpgxKJkHrxqj60j4IZ3rSXkVp8vw/KpC/rB4J05j+PEFQ7h5Rn9dDtPLtDpcPP3lXv706W4iQoL41SUjmDMxCxGxHU2dos7metKiUF6ppLqJ+97dyqfbSxneN477LhnBtIHJtmMpYOmOUv7n/e3sq2hg1sg+PDB7JGlxEbZjqdOkRaF8kjGGxVsP88B72yipaeZbI9L5xcXDyU2Jth0tIO0pr+d/3t/G5zvLGZASzX9dOoKZQ3UaDn+hRaF8WnObk6eX7+Xxz/fQ5nRx/bRc7jhnIMkx4bajBYRDNU38ZWkBr68rIjI0mHvOH8z103IJC9Hdgf5Ei0L5hbLaZv7w8U7eWF9MZGgwN0zPZd6ZA0iMDrMdzS9V1LfwxOd7eGH1fowxzJ2cw13nDiY1VgvaH2lRKL9SUFbPo0t2897mEqJCg7lxRi43zehPio4wekRJdRPPfLmPl9ceoLnNyVUTsrj7vMF6Bpqf06JQfmlXaR1/XrKbDzYfIiwkiKsmZHLLGf0ZlBZrO5pP2nm4jqeW7WHhxhIMcOmYvtx57mAGpcXYjqY8QItC+bWCsnqeWbGPt9YX0+Jwcc7QVG6YnstZg1MJDtLTNTvT5nTx6bZSXlpzgC8LKogMDebaydncckZ/vR4iwGhRqIBQ1dDKi6v38/yq/VTUt9A3PoI5edl8Jy9Lf+kdp6iqkdfWFfFafhHldS1kxEcwd3IO35vaT4/5BCgtChVQWh0ulmwv5dV1RSzbXQ7AjIEpXDq2LxeO7ENCVGD+Iqyob2HR14dYuLGE/P1HCBKYOTSN66bkcM7QNB19BTgtChWwDlY38fq6Iv6x8SD7KxsJCRJmDErhkjF9OXdYmt+fYnuopomlO8pYvLWUFQUVOF2GoemxXDYug8vHZ5KZEGk7ovISWhQq4Blj2FpSy/ubD/H+5hKKjzQhAmMy4zl7aBrnDE1lbFaCz7+rbnO62Fxcwxc7y/h0exnbDtUCkJ0UyaVjMrhsXAbD+uiMrupfaVEodQxjDFsO1vL5zjI+21nGxqJqXAZiI0KY2C+RSblJTMpNYkxWPBGhwbbjdqqp1cmWkhrW7K1kzb4q8guP0NTmJEhgYr9Ezh2WzvnD0xiUFqPzMKlOeV1RiMgc4L+B4cBkY0yHv9VFpBCoA5yA40R/ieNpUajuONLQyvKCClbtqSS/sIrdZfUAhAUHMTg9hhF94xjeN44RGXEM6xNr5RiHMYby+hb2ljewraSWLSU1bDlYQ0FZPUfXexrWJ5Yp/ZOYOiCZqQOS9aC06hZvLIrhgAt4CvjpSYoizxhT0Z3vr0WhTseRhlby9x8hf38V20pq2X6olor6/1vbOz4ylH7JUeQkRdEvOYq+8ZGkxISREhNOSkw4STFhRIeFdHk3VpvTRUOLg7pmB2V1LZTVNlNa20xpXQsHjzSxr6KBfRUN1Lc4vvmalJhwRmfGMToznlGZ8eTlJpGkxaBOQ2dFYWU9CmPMdkCHwsorJUaHccGIdC4Ykf7NfWV1zWw/VMeuw3Xsr2pgf2UjXx+s4aMth3GcYAnX8JAgosKCiQwNJiwkCAMYAy5jMAZaHE7qmh20OFwdfn1osJAeF0H/lGiumpBJ/5Ro+qfGMKxPLOk6W6vyIG9fuMgAH4uIAZ4yxsw/0YYiMg+YB5CTk+OheCpQpMVGkBYbwdlDUv/pfofTRVVDK+X1LVTUt1JR10JVQysNrQ6aWp00tjppanPS6nAhAkEiCIBARGgwseEhxISHEB0eQkxECKmx4aTHRpAeF05iVBhBPn5wXfmHXisKEfkU6NPBQ780xrzbxW9zhjHmoIikAZ+IyA5jzLKONnSXyHxo3/V0SqGV6qaQ4CDS4iJ0PQbl13qtKIwx5/fA9zjo/rNMRN4BJgMdFoVSSqne4bUTyotItIjEHv0c+BawxW4qpZQKPFaKQkSuEJFiYBrwgYgsdt+fISKL3JulA1+KyCZgLfCBMeYjG3mVUiqQ2Trr6R3gnQ7uLwEudn++Fxjr4WhKKaWO47W7npRSSnkHLQqllFKd0qJQSinVKS0KpZRSnfLL2WNFpBzYbzuHWwrQrbmq/JS+Du30dWinr0M7b3od+hljUjt6wC+LwpuISH5XZ731Z/o6tNPXoZ2+Du185XXQXU9KKaU6pUWhlFKqU1oUve+EM94GGH0d2unr0E5fh3Y+8TroMQqllFKd0hGFUkqpTmlRKKWU6pQWhQeIyEMiskNENovIOyKSYDuTDSIyR0S2iohLRLz+lMCeJiKzRGSniBSIyL2289ggIs+ISJmIBPSSASKSLSKficg298/EPbYzdUaLwjM+AUYZY8YAu4CfW85jyxbgSgJw8SkRCQYeAy4CRgBzRWSE3VRWLABm2Q7hBRzAT4wxI4CpwA+9+f+DFoUHGGM+NsY43DdXA1k289hijNlujNlpO4clk4ECY8xeY0wr8Cow23Imj3MvZVxlO4dtxphDxpiv3J/XAduBTLupTkyLwvNuBj60HUJ5XCZQdMztYrz4F4PyHBHJBcYDa+wmOTErCxf5IxH5FOjTwUO/NMa8697ml7QPOV/yZDZP6srroJRqJyIxwFvAvxtjam3nOREtih5ijDm/s8dF5EbgEuA848cXr5zsdQhgB4HsY25nue9TAUpEQmkviZeMMW/bztMZ3fXkASIyC/gP4DJjTKPtPMqKdcBgEekvImHAtcBCy5mUJSIiwN+B7caYR2znORktCs/4KxALfCIiG0XkSduBbBCRK0SkGJgGfCAii21n8hT3yQx3AotpP3D5ujFmq91UnicirwCrgKEiUiwit9jOZMkM4PvAue7fCRtF5GLboU5Ep/BQSinVKR1RKKWU6pQWhVJKqU5pUSillOqUFoVSSqlOaVEopZQP6M6EiiJyloh8JSIOEbn6uMc+EpFqEXm/q8+tRaFUD3HPCLpPRJLctxPdt3NFpO/RH0wRGXfsqZAicomIPGArt/IZC+j6hIoHgBuBlzt47CHaT83tMi0KpXqIMaYIeAL4nfuu3wHzjTGFwI+Bv7nvHwcce878B8ClIhLloajKB3U0oaKIDHSPENaLyHIRGebettAYsxlwdfB9lgB13XluLQqletYfgaki8u/AGcAf3PdfBXzkvir7AeAa90VW17indPmc9ilelOqO+cBdxpiJwE+Bx3vjSXSuJ6V6kDGmTUR+BnwEfMt9uz9wxBjTAiAi9wF5xpg7j/nSfOBM4HWPh1Y+yT2h4HTgjfYZQQAI743n0qJQquddBBwCRtG+aFVfoPwkX1MGZPRyLuVfgoBqY8w4TzyRUqqHiMg44ALaVy37kYj0BZqAiJN8aYR7O6W6xD0t+T4RmQPtEw2KyNjeeC4tCqV6iHtG0CdoX1vgAO1nl/yB9uVvc4/ZtI72SSKPNYT2pWKV6tAJJlT8LnCLiGwCtuJeNVFEJrkn4JwDPCUiW4/5PsuBN4Dz3N/nwpM+t04KqFTPEJF5tK83co37djDt04v/CLgP+IExpsB9+uxiIBT4rTHmNfepsz83xnxtKb5SJ6RFoZQHiMgVwERjzK86eCwdeNkYc57nkyl1cnowWykPMMa8IyLJJ3g4B/iJJ/Mo1R06olBKKdUpPZitlFKqU1oUSimlOqVFoZRSqlNaFEoppTqlRaGUUqpT/x9OariIXgHgdAAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "xArray = rVecs[:,0]         # Slicing out the x-values from rVec\n",
    "yArray = rVecs[:,1]         # Slicing out the y-values from rVec\n",
    "\n",
    "plt.plot(xArray,yArray)     # Plotting y vs. x\n",
    "\n",
    "plt.axis('equal')           # Matplotlib commands to make the plot axes equal and change the labels\n",
    "plt.xlabel(\"X(t)\")\n",
    "plt.ylabel(\"Y(t)\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1.49597871e+11  0.00000000e+00]\n",
      " [ 1.49597868e+11  2.98056552e+07]\n",
      " [ 1.49597859e+11  5.96113093e+07]\n",
      " ...\n",
      " [ 1.49589633e+11 -1.57049602e+09]\n",
      " [ 1.49589943e+11 -1.54069197e+09]\n",
      " [ 1.49590247e+11 -1.51088787e+09]]\n"
     ]
    }
   ],
   "source": [
    "print(rVecs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
