\documentclass{article}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{physics}
\usepackage[inline]{enumitem}
\usepackage{mathrsfs}
\usepackage{cancel}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}

\graphicspath{ {./Figures/} }


\title{Assignment 7}
\author{Cocoa}
\date{April 2, 2020}

\begin{document}

\maketitle

\section{Epidemics: Markov Processes and the SIR Model}

    A Markov processes is one which assumes that the current state of the system only depends on the previous state of the system. Computationally, a Markov process is a kind of a \textit{finite state system}, and thus we can understand it using a framework of a finite state system. We start by defining the states of our system, an entry condition, and an exit condition. For our problem, the state in which one data element (in our case an individual) can be is either Susceptible ($S$), Infected ($I$), or Recovered ($R$). The transition condition is given by our parameters, namely, the probability that a susceptible person becomes infected as $\beta$, and the probability that an infected person recovers as $\gamma$. Thus, the probability of going from $S \to I$ is $\beta$ and for $I \to R$ is $\gamma$. We can visualize this as done in Figure \ref{fig:markovmodel}
    
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.3]{Figures/Markov.svg.png}
        \caption{Markov process in terms of finite state systems. Each arrow represents the probability you have to multiply to get to that state from some previous state}
        \label{fig:markovmodel}
    \end{figure}
    
    \begin{enumerate}[label=(\alph*)]
        \item This allows us to write down the equations for the change of each of the variables as follows. All possible ways to get to the state $S$ are simply all arrows pointing to $S$. In this case, it's just one, with the weight $1-\beta$ and previous state of $S$. Similarly, all possible ways to get to state $I$ is represented by the arrows pointing to $I$, so a previously susceptible person can get infected, i.e, $\beta S_n$, or an infected person can stay infected, which is just 1 minus the probability of leaving the state $I$. The only way to leave the state $I$ is to go to $R$, which has the probability of $\gamma$, thus staying in $I$ has the probability of $1-\gamma$. Doing the same for the last, we get the following.
    
    \begin{align*}
        S_{n+1} &= (1-\beta) S_n \\
        I_{n+1} &= \beta S_n + (1-\gamma) I_n \\
        R_{n+1} &= \gamma I_n + 1 R_n
    \end{align*}
    
    Writing this in vector form, if we let our state be $\psi(n) = \smqty[ S_n \\ I_n \\ R_n ]$, we get the following vector equation
    
    \begin{align}
        \mqty[ S_{n+1} \\ I_{n+1} \\ R_{n+1} ] = \mqty[ 1-\beta & 0 & 0 \\ \beta & 1-\gamma & 0 \\ 0 & \gamma & 1 ] \mqty[ S_n \\ I_n \\ R_n ]
    \end{align}
    
    And we can see that the sum of each column adds up to 1, as the total probability has to add up to 1. Let's call the transition matrix $M$
    
    \item In order to find the eigenvalues and eigenvectors of our transition matrix, we need to solve the equation $\det(M-\lambda I) = 0$, which would allow us to solve $(M-\lambda I)\vb v = \vb 0$, giving us the eigenvalues $\lambda$ and eigenvectors $\vb v$
    
    \begin{enumerate}[label=(\roman*)]
        \item Writing out our equation, we get
        
        \begin{align*}
            \det\left(\mqty[ 1-\beta - \lambda & 0 & 0 \\ \beta & 1-\gamma - \lambda & 0 \\ 0 & \gamma & 1 - \lambda]\right) = 0
        \end{align*}
        
        Expanding that out along the first row, we get
        
        \begin{gather*}
            (1 - \beta - \lambda) ( (1-\gamma-\lambda - 1)(1-\lambda)) = 0
        \end{gather*}
        
        Which gives us the eigenvalues $\lambda = 1$, $\lambda = 1-\beta$ and $\lambda = 1-\gamma$. Solving for the eigenvectors, we plug these values into $(M-\lambda I)\vb v = 0$ to get 3 values of $\vb v$, which will form our eigenbasis. Solving this for each eigenvalue, we get the following.
        
        For $\lambda = 1$,
        
        \begin{align*}
            \mqty[ -\beta & 0 & 0 \\ \beta & -\gamma & 0 \\ 0 & \gamma & 0 ]\mqty[x \\ y \\ z] = \vb 0
        \end{align*}
        
        This makes it instantly clear that only the value of $z$ is free to take any value, and $x, y = 0$. Thus our first eigenvector is $\vb {v_1} = \smqty[0 \\ 0 \\ 1] $.
        
        For $\lambda = 1 - \beta$,
        
        \begin{align*} 
            \mqty[ 0 & 0 & 0 \\ \beta & \beta -\gamma & 0 \\ 0 & \gamma & \beta ]\mqty[x \\ y \\ z] = \vb 0
            \implies \left. \begin{aligned}
                \beta x + (\beta - \gamma) y = 0 \\
                \gamma y + \beta z = 0
            \end{aligned} \right\}
            \text{Resulting system of equations}
        \end{align*}
        
        This implies that $x = \frac{\beta}{\gamma} - 1$ and $y = -\frac{\beta}{\gamma}$, and that $z$ is free to take on any value without changing the equation, giving us $\vb {v_2} = \smqty[\frac{\beta}{\gamma} - 1 \\ -\frac{\beta}{\gamma} \\ 1]$
        
        Lastly, for $\lambda = 1-\gamma$
        \begin{align*}
            \mqty[ \gamma - \beta & 0 & 0 \\ \beta & 0 & 0 \\ 0 & \gamma & \gamma ]\mqty[x \\ y \\ z ] = \vb
            \implies \left. \begin{aligned}
                (\gamma - \beta)x &= 0 \\
                \beta x &= 0 \\
                (z + y)\gamma &= 0
            \end{aligned}\right\}
            \text{Resulting system of equations}
        \end{align*}
        
        Which means $x = 0$, and from the third equation, we can see that $y = -z$, thus our last eigenvector $\vb {v_3} = \smqty[0 \\ -1 \\ 1]$
        
        \item We can do the same using sympy, as has been done in the attached notebook

    \end{enumerate}
    
    \item The eigenvectors are clearly linear independent, thus the matrix $M$ is diagonalizable. The matrix with our eigenvectors as it's columns forms the change of basis matrix for the transformation $M$. This change of basis matrix takes the natural basis $\mathscr{N}$ to the eigenbasis $\mathscr{E}$, thus $B : \mathscr{N} \mapsto \mathscr{E}$, and thus any arbitrary vector in the natural basis transforms as the inverse of $B$, giving us the following relation.
    
    \begin{align}
        M = BDB^{-1}
    \end{align}
    
    With
    
    \begin{align*}
        B = \mqty[ 0 & \frac{\beta}{\gamma} - 1 & 0 \\ 0 & \frac{-\beta}{\gamma} & -1 \\ 1 & 1 & 1 ] \\
        B^{-1} = \mqty[ 1 & 1 & 1 \\ \frac{\gamma}{\beta - \gamma} & 0 & 0 \\ -\frac{\beta}{\beta-\gamma} & -1 & 1 ]
    \end{align*}
    
    Thus $D = B^{-1}MB$. Performing the calculation, we get
    
    \begin{align*}
        D &= \mqty[ 1 & 1 & 1 \\ \frac{\gamma}{\beta - \gamma} & 0 & 0 \\ -\frac{\beta}{\beta-\gamma} & -1 & 1 ] \mqty[ 1-\beta & 0 & 0 \\ \beta & 1-\gamma & 0 \\ 0 & \gamma & 1 ] \mqty[ 0 & \frac{\beta}{\gamma} - 1 & 0 \\ 0 & \frac{-\beta}{\gamma} & -1 \\ 1 & 1 & 1 ] \\
        D &= \mqty[ 1 & 1 & 1 \\ \frac{\gamma}{\beta - \gamma} & 0 & 0 \\ -\frac{\beta}{\beta-\gamma} & -1 & 1 ] \mqty[ 0 & -\frac{(\beta - 1)(\beta-\gamma)}{\gamma} & 0 \\ 0 & \frac{\beta(\beta-1)}{\gamma} & \gamma -1 \\ 1 & 1 - \beta & 1 - \gamma ] \\
        D &= \mqty[ 1 & 0 & 0 \\ 0 & 1-\beta & 0 \\ 0 & 0 & 1 - \gamma ]
    \end{align*}
    
    \item Given a starting state $\psi(0) = \smqty[99 \\ 1 \\ 0]$, in order to go to $\psi(1)$, we need to apply $M$ to $\psi(0)$. Similarly, in order to go to $\psi(2)$, we need to apply $M$ to $\psi(1)$. Doing this generally, in order to get $\psi(n)$, we need to apply $M$ to $\psi(n-1)$
    
    \begin{align*}
        \psi(n) = M\psi(n-1) = M(M\psi(n-2)) = M(M(M\psi(n-3))) \dots 
    \end{align*}
    
    This means in order to $n$ steps ahead, we need to multiply $\psi(0)$ by $M^n$, giving us
    
    \begin{align}
        \psi(n) = M^n \psi(0)
    \end{align}
    
    \item In order to calculate $\psi(n)$, we need to raise the matrix $M$ to the power of $n$. We can do this much more easily using the diagonalized form $M = BDB^{-1}$, and then convert it back into our natural basis. An easier alternative would be to convert our state vector into our eigenbasis and simply work completely in that, however that would be calculating $D^n$ rather than $M^n$, so we won't do that. Performing the calculation $BD^nB^{-1}$, we get
    
    \begin{align*}
        M^n &= \mqty[ 0 & \frac{\beta}{\gamma} - 1 & 0 \\ 0 & \frac{-\beta}{\gamma} & -1 \\ 1 & 1 & 1 ] \mqty[ 1 & 0 & 0 \\ 0 & (1-\beta)^n & 0 \\ 0 & 0 & (1 - \gamma)^n ] \mqty[ 1 & 1 & 1 \\ \frac{\gamma}{\beta - \gamma} & 0 & 0 \\ -\frac{\beta}{\beta-\gamma} & -1 & 1 ] \\
        &= \mqty[ 0 & \frac{\beta}{\gamma} - 1 & 0 \\ 0 & \frac{-\beta}{\gamma} & -1 \\ 1 & 1 & 1 ] \mqty[ 1 & 1 & 1 \\ \gamma \frac{(1-\beta)^n}{\beta-\gamma} & 0 & 0 \\ -\beta \frac{(1-\gamma)^n}{\beta - \gamma} & -(1-\gamma)^n & 0 ] \\
        &= \mqty[(1-\beta)^n & 0 & 0 \\ \beta\frac{(1-\gamma)^n-(1-\beta)^n}{\beta - \gamma} & (1-\gamma)^n & 0 \\  \frac{\gamma(1-\gamma)^n -\beta(1-\beta)^n}{\beta-\gamma} + 1 & 1 - (1-\gamma)^n & 1]
    \end{align*}
    
    And appliying this on our inital condition vector $\psi(0) = \smqty[ S_0 \\ I_0 \\ R_0 ]$, we get
    
    \begin{align}
        M^n \psi(0) = \mqty[S_0(1-\beta)^n \\ I_0(1-\gamma)^n + S_0 \beta\frac{(1-\gamma)^n-(1-\beta)^n}{\beta - \gamma} \\ S_0 \left(\frac{\gamma(1-\gamma)^n -\beta(1-\beta)^n}{\beta-\gamma} + 1\right) + I_0 (1 - (1-\gamma)^n) + R_0] \label{eq:state_at_n}
    \end{align}
    
    This can also be done in SymPy, as done in the notebook
    
    \item We can compute what happens as time goes on by computing the limit of equation (\ref{eq:state_at_n}) as $n \to \infty$. Since both $\beta$ and $\gamma$ are smaller than $1$, any time we take them to a power of $n$, the whole term goes to 0. Doing so, we can compute the limit
    
    \begin{align*}
        \lim_{n\to\infty} \psi(n) &= \left(\mqty[S_0(1-\beta)^n \\ I_0(1-\gamma)^n + S_0 \beta\frac{(1-\gamma)^n-(1-\beta)^n}{\beta - \gamma} \\ S_0 \left(\frac{\gamma(1-\gamma)^n -\beta(1-\beta)^n}{\beta-\gamma} + 1\right) + I_0 (1 - (1-\gamma)^n) + R_0] \right) \\
        &= \mqty[S_0\cancelto{0}{(1-\beta)^n} \\ I_0\cancelto{0}{(1-\gamma)^n} + S_0 \beta\frac{\cancelto{0}{(1-\gamma)^n-(1-\beta)^n}}{\beta - \gamma} \\ S_0 \left(\frac{\gamma\cancelto{0}{(1-\gamma)^n -\beta(1-\beta)^n}}{\beta-\gamma} + 1\right) + I_0 (1 - \cancelto{0}{(1-\gamma)^n}) + R_0] \\
        &= \mqty[ 0 \\ 0 \\ S_0 + I_0 + R_0]
    \end{align*}
    
    So we end up with everybody in the recovered state, which makes sense because, simply analytically, every state but the state $R$ has some probability of a data member leaving that state, thus all data members would converge to the state $R$. Realistically, this makes sense because over time, everybody would get infected, so $S$ would go to 0, and the infected people would either die or recover and become immune, which means that $I$ becomes 0 and everybody ends up in the state $R$, and our total population is $S_0+I_0+R_0$.
    \end{enumerate}

\end{document}
