#!/usr/bin/env python

"""
GOAL: Plot the difference between dist() and dist_midpoint()
"""

a = -9.8 # m/s**2
vel = lambda t : a * t

def arg(param):
    return argv[argv.index(param) + 1]

def dist(t, dt):
    s = 0
    for i in range(int(t/dt)):
        s += vel(dt * i) * dt
    
    return s

def dist_midpoint(t, dt):
    s = 0
    for i in np.arange(dt/2, int(t/dt), dt):
        s += vel(dt * i) * dt
    
    return s

if __name__ == "__main__":
    pass
