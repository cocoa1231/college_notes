#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from sys import argv

"""
GOAL: Given some prec, find a delta t so that
      for a given velocity function v(t), np.round(dist(t), prec)
      does not change
"""

a = -9.8 # m/s**2
vel = lambda t : a * t

def arg(param):
    return argv[argv.index(param) + 1]

def dist(t, dt):
    s = 0
    for i in range(int(t/dt)):
        s += vel(dt * i) * dt

    return s

def dist_midpoint(t, dt):
    s = 0
    for i in np.arange(dt/2, int(t/dt), dt):
        s += vel(dt * i) * dt

    return s

if __name__ == "__main__":
    dt   = 0.1 # initial value of delta t
    prec = int(arg("-p")) # passed in parameter for precision
    endpoint = float(arg("-T")) # passed in parameter for total time

    last_result = abs(dist(endpoint, dt))
    while True:
        dt *= 0.1
        new_result = abs(dist(endpoint, dt))
        
        if np.round(new_result, prec) == np.round(last_result, prec):
            print("Required delta t is", dt)
            print(f"last_result (dt = {dt*10}):", np.round(last_result, prec))
            print(f"new_result (dt = {dt}): ", np.round(new_result, prec))
            break

        last_result = new_result 



