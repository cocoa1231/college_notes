#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.patches import Circle
from matplotlib.lines import Line2D
from matplotlib.animation import FuncAnimation

# Config
SPRING_CONSTANT = 10
eq_point = 6 # Equilibrium point
data = [8, 0] # [pos, vel]
velocity = data[1]
xlim = ylim = 15
ceil_height = -5

F  = lambda x : -SPRING_CONSTANT * x
dt = 0.001

# Set up screen
fig = plt.gcf()
ax  = fig.gca()


bob = Circle( (0, ceil_height - data[0]), radius=0.5 )
string = Line2D( [0, 0], [ceil_height, ceil_height-data[0]] )

ax.add_line( Line2D([-xlim, xlim], [ceil_height, ceil_height], color = 'red') )
ax.add_line( string )
ax.add_patch( bob )

ax.set_xlim(-xlim, xlim)
ax.set_ylim(-ylim, ylim)

def update_position(i, velocity, bob):
    
    x, y = bob.center
    bob.center = (0, y + velocity * dt)
    
    disp = eq_point - y
    dv = F(disp) * dt
    velocity += F(disp) * dt 

    print(velocity, dv, y, bob.center)

    return bob,

ani = FuncAnimation(fig, update_position, frames=5*30, fargs=(velocity, bob), interval=1000/30)
ani.save('bobbing_bob.mp4')

