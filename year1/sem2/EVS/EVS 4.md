# EVS 4

- Framework of viewing nature as capital - relationship b/w econ and env.
  - "Natural Capital" - Capital resources that come from nature when considering industrial planning
  - Valuing the natural capital - how do we price it?
  - Valuing commons - Things such as the atmosphere. So how do we value these global commons?
  - What kind of market structure can you propose where environment can be preserved? Because in the market, currently the environment does not have a market value
- A better measure of **growth** is per capita income per gross environmental products, see how much you're destroying to get where you are
- "Green Growth" - redefining growth to include respect for resources
  - How do you produce energy in such a manner, you consume less resource (water, etc.) to produce the same amount of power?
  - 