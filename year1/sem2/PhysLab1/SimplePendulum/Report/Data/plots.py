#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

files = ['mass.csv', 'length.csv', 'angle.csv']


for _file in files:
    fig = plt.gcf()
    ax  = fig.gca()
    data = np.loadtxt(_file, delimiter=',')
    xvals, yvals = [], []
    for row in data:
        xvals.append(row[0])
        yvals.append(sum(row[1:]) / (len(row)-1))
    yvals = np.array(yvals)/10 
    ax.scatter(xvals, yvals)
    fig.savefig('../Figures/'+_file[:-4]+'.png')
    fig.clear()

    
