\documentclass[12pt]{article}

\usepackage[left=1in, right=1in, top=0.8in, bottom=1in]{geometry}
\usepackage{chngpage}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{amsmath}

\title{Experiment 0 - Simple Pendulum}
\author{Cocoa Kaushal}
\date{Date Conducted - January 23, 2020}

\graphicspath{ {./Figures/} }

\begin{document}

	\maketitle
	
	\noindent \hrulefill
	

	\noindent Date of Submission --- \textbf{February 13, 2020} \\ 
	\noindent Instructor --- \textbf{Amin Nizami}  \\
	\noindent Teaching Fellows --- \textbf{Manpreet Kaur, Philip Cherian} \\
	\noindent Lab Partner ---  \textbf{Abdelsalam Gena}

	\noindent \hrulefill

	
	\section{Introduction}
	
		The goal of the experiment is to determine which factors the period of a simple pendulum depends upon. Three variables were explored --- mass, length and angle, and thus the experiment was divided into three parts. The first part tests the dependence of the mass upon the time period by holding the length and angle constant. The second part tests the dependence of length upon the time period by varying the length and holding the mass and angle constant. Lastly, the third part tests the angle by holding the mass and length constant and varying the angle. In each part, the time period for ten oscillations ($T_{10}$) is measured.
	
		\vspace{-1em}

	\subsection{Apparatus}
		
	\begin{enumerate}
	
		\itemsep0em
		
		\item Metallic bobs of different materials 
		\item A length of string
		\item A cork with a slit
		\item A retort stand with an attached protractor (Fig. \ref{fig:retort1})
		\item A stopwatch
		\item A scale
		\item A weighing balance
	\end{enumerate}
	
	\begin{figure}[h!]
		\centering
		\includegraphics[scale=0.8]{retort_stand.png}
		\caption{Retort stand with protractor at pivot to measure angle of release}
		\label{fig:retort1}
	\end{figure}
	
	\pagebreak
	
	\section{Procedure}
	
		\subsection{Part A: Varying the mass}
		
		\begin{figure}[h!]
		
		\centering
		
		\begin{minipage}{0.5\linewidth} 
		
			\centering
			\begin{tabular}{| c | c | c |}
				\hline 
				\textbf{$M_i$} & \textbf{$T_{10}$} (sec) & \textbf{$T$} (sec)\\
				\hline 
				13.4 & 14.28 & 1.428 \\
				\hline
				26.6 & 14.19 & 1.419 \\
				\hline
				27.2 & 14.29 & 1.429 \\				
				\hline
				32.8 & 14.53 & 1.453 \\
				\hline
				43.3 & 14.53 & 1.453 \\
				\hline
			\end{tabular}
			\captionsetup{type=table}
			\caption{Mass of bob $M_i$ vs. time period of pendulum}
			\label{table:1}

			\end{minipage} 
			\hfill	
		\begin{minipage}{0.45\linewidth}
			\includegraphics[scale=0.2]{mass}
			\caption{Time period as a function of the mass of the bob}
		\end{minipage}
			
		\end{figure}

		
			
			\noindent In the first part of the experiment, the mass of the bob is varied between five masses, $M_1 = 13.4$g, $M_2 = 26.6$g, $M_3 = 27.2$g, $M_4 = 32.8$g and $M_5 = 43.3$g. The mass of each bob is measured using a digital weighing scale. Since we do not know the dependence of the angle on the time period, we can pick an angle that is comfortable to measure the time period of by eye, thus the angle of release is kept fixed at 40$^\circ$ measured using the protractor (least count = $1^\circ$). In order to reduce the error in the measurement, the time taken for ten oscillations ($T_{10}$ seconds) is measured three times for each mass and the average is taken, and the average is divided by 10 to get the time for one oscillation
			
		\subsection{Part B: Varying the length}
		
			\begin{figure}[h!]
			\centering
			
			\begin{minipage}{0.5\linewidth}
			\centering
			\begin{tabular}{|c|c|c|c|}
				\hline
				$l$ & $T_{10}$ (sec) & $T$ & $T^2$ (sec) \\
				\hline
				10.1 & 6.36 & 0.636 & 0.405 \\
				\hline
				20.0 & 8.72 & 0.872 & 0.786 \\
				\hline
				30.1 & 10.87 & 1.087 & 1.181 \\
				\hline
				40.2 & 12.54 & 1.254 & 1.572 \\
				\hline
				48.55 & 13.82 & 1.382 & 1.910 \\
				\hline
			\end{tabular}
			\captionsetup{type=table}
			\caption{The length of the string $l$ vs. time period}
			\label{table:2}
			\end{minipage}
			\hfill
			\begin{minipage}{0.45\linewidth}
				\includegraphics[scale=0.2]{length}
				\label{figure:2}
				\caption{Time period versus length of pendulum}
			\end{minipage}
			\end{figure}
		
			In the second part of the experiment, the length of the string, $l$ is varied from 10 cm to 50 cm and for each length, the value of $T_{10}$ is measured thrice and the average is taken. The range for the lengths is chosen since the measuring scale available has a range of 0 to 50 cm. The length of the string is measured from the pivot to the center of mass of the bob by taking the average of the lengths from the pivot to the top of the bob and from the pivot to the bottom of the bob. This is done since the center of mass of the bob is not directly measurable, and this reduces the error produced by simply eyeballing it. According to Part A, the mass of the bob shows no obvious correlation with the time period, so the mass of the bob is kept constant at $M_1 = 13.4$g chosen at random. The dependence of angle on the time period is still unknown, thus a comfortable angle of release is taken at random to be 20$^\circ$
			
		
		\subsection{Part C: Varying the angle}
		
			\begin{figure}[h]
			\centering
			\begin{minipage}{0.5\linewidth}
			\centering
			\begin{tabular}{|c|c|c|}
				\hline
				Angle & \textbf{$T_{10}$} (sec) & \textbf{$T$} (sec) \\
				\hline
				10$^\circ$ & 14.10 & 1.410 \\
				\hline
				20$^\circ$ & 14.07 & 1.407 \\
				\hline
				30$^\circ$ & 14.10 & 1.410 \\
				\hline
				40$^\circ$ & 14.29 & 1.429 \\
				\hline
				45$^\circ$ & 14.43 & 1.443 \\
				\hline
				50$^\circ$ & 14.44 & 1.444 \\
				\hline
			\end{tabular}
			\captionsetup{type=table}
			\caption{Angle of release vs. time period}
			\label{table:3}
			\end{minipage}
			\hfill
			\begin{minipage}{0.45\linewidth}
				\includegraphics[scale=0.2]{angle}
				\caption{Time period as a function of angle of release}
			\end{minipage}
			\end{figure}
		
			\noindent In the last part of the experiment, the angle is varied from $10^\circ$ to $50^\circ$. It is once again assumed that the choice of both the mass of the bob and the length of the string should not affect the experiment, thus $M_1$ (13.4g) is taken as the mass and the length is kept at 48.5cm. The value for $T_{10}$ is averaged over three measurements for each of the angles and recorded.
		
		\pagebreak
		
		\section{Discussion}
			
			From the data, it is clear that the time period depends the most on the length of the pendulum. The range of the time period for the angle and mass is 0.037 sec and 0.339 sec, however the range for the time period when the length of the pendulum is varied is 0.750 sec. Furthermore, the plot of $T^2$ versus $l$ shows a linear dependence of $T^2$ on $l$. The data on the time period of the pendulum as a function of mass does show some significant variance, however as the string stretching under the weight of the mass $M_i$ was not accounted for, the length of the string may have changed causing the variance.  
			
			\begin{gather}
				T \propto \sqrt{l}
			\end{gather}
			
			This experiment also does not account for the dependence of time period on the acceleration due to gravity. This can be tested by performing the experiment in a falling frame at some acceleration $k\cdot -g$, thus lowering the effective value of $g$ and seeing the dependence
			
			
			
\end{document}
