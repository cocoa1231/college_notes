\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{physics}

\title{Experiment 3 - The Three Terminal Black Box}
\author{Cocoa Kaushal}
\date{Date Conducted - February 27, 2020}

\graphicspath{ {../Figures/}{../Circuits/} }

\begin{document}

	\maketitle
	
	\noindent \hrulefill
	

	\noindent Date of Submission --- \textbf{March 05, 2020} \\ 
	\noindent Instructor --- \textbf{Amin Nizami}  \\
	\noindent Teaching Fellows --- \textbf{Manpreet Kaur, Philip Cherian} \\
	\noindent Lab Partner ---  \textbf{Kartik Tiwari}

	\noindent \hrulefill
	
	\section{Introduction} \label{sec:introduction}
	
		The aim of this experiment was to investigate a given three terminal black box and identify the components within it. The black box had three terminals, and the internal circuit was a star circuit, as shown in Figure \ref{fig:blackbox}. Each element could either be a resistor, a diode, or a battery.
		
		\begin{figure}[h!]
			\centering
			\includegraphics[scale=4]{BlackBox}
			\caption{Three terminal black box with a star circuit}
			\label{fig:blackbox}
		\end{figure}
		
	\section{Apparatus} \label{sec:apparatus}
	
		\begin{enumerate}
			\item A black box with three connecting terminals marked A, B, and C
			\item A variable DC power supply
			\item Two digital multimeters
			\item A signal generator
			\item A digital storage oscilloscope
			\item A digital stopwatch
			\item A resistor ladder
			\item Connecting wires
		\end{enumerate}
	
	\section{Procedure} \label{sec:procedure}
	
		Out of the possible elements that could be there, batteries are the easiest to start with as they would have the simplest circuit as one could simply test for a potential difference between two terminals. Then a diode was tested for, as it would inhibit the flow of current in one direction for a small voltage. And lastly, a resistor would follow a linear V-I relation
		
		\subsection{Battery} \label{sec:battery}
		
			In order to test for a battery, a voltmeter was connected across all possible combinations of the three terminals of the box (see Figure \ref{fig:circuit_battery})
			
			\begin{figure}[t]
			\begin{subfigure}{0.5\linewidth}
				\centering
				\includegraphics[scale=3]{Battery}
				\caption{}
				\label{fig:circuit_battery}
			\end{subfigure}
			\begin{subfigure}{0.5\linewidth}
				\centering
				\begin{tabular}{| c | c |}
					\hline
					\textbf{Connection} & \textbf{Voltage} \\
					\hline
					A to B & None \\
					\hline
					A to C & None \\
					\hline
					B to C & None \\
					\hline 
				\end{tabular}
				
				\caption{}
				\label{table:battery_result}
			\end{subfigure}
			\caption{Test for potential difference across the terminals}
			\end{figure}
			
		\subsection{Diode} \label{sec:diode}
			
			\begin{figure}[h]
			\begin{subfigure}{0.5\linewidth}			
				\centering
				\includegraphics[scale=4]{DiodeDC}
				\caption{}
				\label{fig:circuit_diodedc}				
			\end{subfigure}
			\begin{subfigure}{0.5\linewidth}
				\centering
				\begin{tabular}{| c | c |}
					\hline
					\textbf{Connection} & \textbf{Current Seen} \\
					\hline
					A to C & No \\
					\hline
					A to B & Yes \\
					\hline
					B to A & Yes \\
					\hline
					B to C & No \\
					\hline
					C to A & Yes \\
					\hline
					C to B & Yes \\
					\hline
				\end{tabular}
				\caption{}
				\label{table:diodedc_result}
			\end{subfigure}
			
			\caption{(a) Circuit to test for diode, (b) Results for diode testing}
			\end{figure}
			
			In order to test for a diode with DC current, all possible permutations of the three terminals were connected to a small voltage (below 0.3 Volts). This was done to make sure that if there was a diode in the box, the voltage applied was safely below the breakdown voltage of the diode, and thus it would stop current flowing in one direction. See Figure \ref{fig:circuit_diodedc} for the circuit diagram.
			
			
			
			The V-I data for the connection B to C was taken from 0 Volts to 3 Volts. The data was linear above 1 Volt, and thus the sampling between 0 to 1 Volts was finer with a step size of 0.05, while the sampling from 1 Volt to 3 Volts had a step size of 0.1 Volt. The same was done for connection A to C till 2.09 Volts. The circuit used for this is shown in Figure \ref{fig:vitesting}
			
			\begin{figure}[h]
			\centering
			\includegraphics[scale=4]{VIChar_Diode}
			\caption{Testing V-I characteristics across two nodes}
			\label{fig:vitesting}
			\end{figure}
			
		\subsection{Resistor} \label{sec:resistor}
			
			No further testing for a resistance was done, as the data collected in section \ref{sec:diode} while testing for diodes was enough to test if there were resistors in the circuit.
	
	\section{Analysis} \label{sec:analysis}
	
		From the data in section \ref{sec:battery}, we can see that there is no battery present in the circuit since there is no potential difference across any of the terminals of our black box. Then, from section \ref{sec:diode}, we can see that current can flow from terminal C to terminals A and B, but current cannot flow A or B to C. This indicates that there is a diode present at terminal C. Moreover, we can see that the current across terminals A and B can flow bidirectionally, thus that indicates that there are resistors at terminals A and B.
		
		\subsection{Diode} \label{sec:analysis:diode}
		
			V-I characteristics of the diode are shown in Figure \ref{fig:plots_diode}. The connections were made from both terminals A and B to terminal C.
			
			\begin{figure}[h!]
			\begin{subfigure}{0.5\linewidth}
				\centering
				\includegraphics[scale=0.3]{CtoA}
				\caption{Connection from A to C}
				\label{fig:results_CtoA}
			\end{subfigure}
			\begin{subfigure}{0.5\linewidth}
				\centering
				\includegraphics[scale=0.3]{CtoB}
				\caption{Connection from B to C}
				\label{fig:results_CtoB}
			\end{subfigure}
			\caption{V-I characteristics of the diode}
			\label{fig:plots_diode}
			\end{figure}
			
			In both cases, the reverse breakdown voltage comes out to be 0.4 Volts.
		
		
		
		\subsection{Resistor}
			
			\begin{figure}[h!]
			\begin{subfigure}{0.5\linewidth}
				\centering
				\includegraphics[scale=0.32]{CtoA_above_breakdown}
				\caption{A to C}
				\label{fig:ctoa_above_breakdown}
			\end{subfigure}
			\begin{subfigure}{0.5\linewidth}
				\centering
				\includegraphics[scale=0.32]{CtoB_above_breakdown}
				\caption{B to C}
				\label{fig:ctob_above_breakdown}
			\end{subfigure}
			\caption{V-I characteristics above the breakdown voltage}
			\label{fig:vi_above_breakdown}	
			\end{figure}
			
			The data in Figures \ref{fig:plots_diode} can be used to get the resistance of the resistors at terminals A and B. If we consider the data above the breakdown voltage and plot a trend line for both, we can get the resistance of each resistor as the slope of the line. The effective resistance of the diode above the breakdown voltage is very small with respect to the other two resistances in the circuit and thus the diode is considered nonexistent above the breakdown voltage. 
			
			
			As we can see in both cases, the resistance comes out to be approximately 2.057 $\Omega$
		
		\subsection{Testing with AC source} \label{analysis:ac_source}	
			
			\begin{figure}[h!]
				\centering
				\includegraphics[scale=4]{DiodeAC}
				\caption{Circuit diagram for using the oscilloscope to measure waveform of current}
				\label{fig:circuit:diodeac}
			\end{figure}			

			Since an oscilloscope and an AC power source was also provided, the contents of the black box can be deduced using them too. In order to use the oscilloscope to measure current, a shunt resistance of 1 $\Omega$ was connected in parallel to it, so that the waveform that the oscilloscope shows is that of $I(t)$ and not $V(t)$. The circuit diagram is shown Figure \ref{fig:circuit:diodeac}. The resulting waveforms observed are shown in Figure \ref{fig:osc:waveforms}
			
			\begin{figure}[h!]
			\begin{subfigure}{0.5\linewidth}
				\centering
				\includegraphics[scale=0.08]{GS-Osc_Resistor}
				\caption{Connection A to B (Resistors)}
				\label{fig:osc:atob}
			\end{subfigure}
			\begin{subfigure}{0.5\linewidth}
				\centering
				\includegraphics[scale=0.07]{GS-Osc_Diode_Up}
				\caption{Connection C to B (Diode and Resistor)}
				\label{fig:osc:ctob}
			\end{subfigure}
			\caption{Waveforms of $I(t)$ for various terminals in the black box}
			\label{fig:osc:waveforms}
			\end{figure}
			
			As expected, the waveform of a diode is an upward rectified sine wave, while the waveform of a resistor is simply a pure sine wave. Finally, the resulting circuit diagram of the black box is shown in Figure \ref{fig:final_blackbox}
			
			\begin{figure}[h!]
				\centering
				\includegraphics[scale=5]{final_circuit}
				\caption{Internal circuit of the black box}
				\label{fig:final_blackbox}
			\end{figure}
			
			
\end{document}