\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{physics}

\title{Experiment 2 - Incandescent Lamp and Inverse Square Law}
\author{Cocoa Kaushal}
\date{Date Conducted - February 20, 2020}

\graphicspath{ {Figures/} }

\begin{document}
	
	\maketitle

		\noindent \hrulefill
	

	\noindent Date of Submission --- \textbf{February 27, 2020} \\ 
	\noindent Instructor --- \textbf{Amin Nizami}  \\
	\noindent Teaching Fellows --- \textbf{Manpreet Kaur, Philip Cherian} \\
	\noindent Lab Partner ---  \textbf{Kartik Tiwari}

	\noindent \hrulefill
	
	\section{Introduction}
	
		Through this experiment, we aim to understand the relation between the voltage applied across an incandescent lamp, and the current it draws, and further arrive at a relation between it's power and it's resistance. To do this, we simply apply a voltage across the lamp (measured using a multimeter used as a volt meter) and measure the current through the circuit (measured using another multimeter as an ammeter). In the third part of this experiment, we study the relation of the intensity of light versus distance. We do so by creating a point source by shining a light through a convergent lens, focusing it onto a pinhole, and using the light coming out of the pinhole as our point source. We measure the intensity of light by measuring the current through a photodiode held at a distance $d$ from the point source.
		
		\vspace{-1em}
		
	\section{Apparatus}
	
		\begin{enumerate}
		
			\itemsep0em
			
			\item A DC variable power supply
			\item Two digital multimeters with probes
			\item A 12 V, 21 W incandescent lamp
			\item A stand to hold the lamp
			\item A Resistor Ladder
			\item Connecting cords
			\item A convex lens
			\item A photodiode
			
		\end{enumerate}
		
	\section{Procedure}
	
		The experiment is divided into three parts, two of which involve taking measurements. The first part measures the $VI$ characteristics of the lamp, treating it as a non ohmic resistor and fitting the data using the following power law rather than $V = RI$. 
		
		\begin{gather}
			V = KI^a
		\end{gather}
		
		The second part similarly determines the power law relation between the power supplied to the lamp and the resistance of the lamp in the following form.
		
		\begin{gather}
			P = CR^n
		\end{gather} 
		
		The last part determines the relation between distance from a point source of light and the intensity of light.
		
		\subsection{Part A -- VI Characteristics}
		
			 The incandescent lamp is connected to a power supply, a voltmeter and an ammeter as shown in Figure \ref{fig:part_a:circuit}. The power supply had a maximum voltage of 15 Volts. The resistance box had a resistance of 10 $\Omega$ as this allowed us to go up to approximately up to 9 Volts across the lamp, comfortably below the limit of the lamp at 12 V. The voltage applied across the lamp was varied starting from 0.9 Volts up to 9 Volts in steps of 0.1 Volts initially, and then gradually the steps were increased to 0.25 Volts as no deviation from a linear V-I relationship was seen. A deviation from this linear relationship was suspected near 0.9 Volts, so readings from 0 Volts to 1 Volt were taken again, with a finer step size of 0.05 Volts\footnote{This is the reason for the two curves in Figure \ref{fig:part_a:vi}. The temperature of the lamp changed by the time the second round of readings from 0 to 1 volts were taken, thus the readings are shifted}. A total of 70 data points were collected.
			 			 
			 \begin{figure}[h!]
			 	\centering
			 	\includegraphics[scale=0.6]{circuit}
			 	\caption{Experimentally used circuit diagram}
			 	\label{fig:part_a:circuit}
			 \end{figure}
			 
		\subsection{Part B -- Power-Resistance Relationship}
			
			The data collected in Part A will be used in the analysis of the data to establish the power-resistance relationship. No experimental data was collected for this part
			
		\subsection{Part C -- Intensity-Distance Relationship}
		
			The experimental setup is shown in Figure \ref{fig:part_c_ray_diagram}. An LED lamp with a variable aperture was used as the light source. A converging lens was used to focus the light onto a circular aperture, which will be treated as our point source of light. From this screen, at a distance $d$, a photodiode was kept connected to an ammeter which was used to measure the current flowing through the photodiode as a result of the light shining on it's area. The relationship between current of the diode and intensity of light on the surface is taken to be linear. 
			
			\begin{figure}[h]
				\centering
				\includegraphics[scale=0.7]{ray_diagram}
				\caption{Experimental setup for measuring intensity of light as current through a photodiode with point source as light coming from a circular aperture (pinhole)}
				\label{fig:part_c_ray_diagram}
			\end{figure}
			
			The distance $d$ started at a minimum of 8 cm, and was varied by approximately 0.3 cm when the photodiode was close to the screen. This was done because the current showed a much larger variation with distance when the diode was close to the screen. As $d$ grew larger, the step size was increased suitably to $0.5$ cm, and then to $1$ cm. At each distance, the current was recorded and a total of 19 data points were taken.
			
	\section{Analysis}
	
		\subsection{Part A - VI Characteristics}
		
			The power relation we are trying to establish is $V = KI^a$. The plot of all the data is shown in Figure \ref{fig:part_a:vi}. From 1 volt to 10 volts, the relation is approximately linear, while the relation from 0 volts to 1 volts seems to be nonlinear.
			
			\begin{figure}[t]
				\centering
				\includegraphics[scale=0.4]{VI}
				\caption{Voltage vs Current characteristics of an incandescent lamp}
				\label{fig:part_a:vi}
			\end{figure}
			
			Since it would be difficult to fit a nonlinear curve, we can turn it into a linear curve by plotting a log-log graph of $\log(V)$ versus $\log(I)$. The equation of the line, then, would be
			
			\begin{gather}
				\log(V) = a\log(I) + \log(K)
			\end{gather}
			
			We create two plots, one for the regime where the lamp behaves (approximately) linearly, i.e, above 1 Volts, and the other where the lamp very clearly does not behave linearly, i.e, below 1 Volt. If we try to fit equation (3) in the first regime, we get Figure \ref{fig:part_a:vi_gr1}. From that, we can see that the power $a = 2.41$ and the constant of proportionality $K = 10^{2.11} = 128$.
			

			\begin{figure}[h!]
				\begin{subfigure}{0.5\textwidth}
					\centering
					\includegraphics[scale=0.25]{logplt_VI}
					\caption{}
					\label{fig:part_a:vi_gr1}
				\end{subfigure}
				\begin{subfigure}{0.5\textwidth}
					\centering
					\includegraphics[scale=0.25]{low_logplt_VI}
					\caption{}
					\label{fig:part_a:vi_le1}
				\end{subfigure}
				
				\caption{(a) Shows the plot of voltage vs. current for $V > 1$ Volts, and (b) shows plot of voltage vs. current for $V < 1$ Volts}
			\end{figure}
			
			Unfortunately, the scatter from the data below 0.32 Volts is too much to fit a linear model, and after removing the data, the remaining 12 readings do not produce  a satisfactory linear model, as shown in Figure \ref{fig:part_a:vi_le1}. Thus with the data available, we cannot determine a functional relationship between the voltage and current below 1 Volts. 
			
		\subsection{Part B -- Power - Resistance Relationship}
		
			Knowing the VI characteristics allows us to calculate the power applied to the lamp, and the instantaneous resistance of the lamp. The resistance at a given voltage can be calculated as the ratio of the applied voltage and current drawn, and the power being the product of the two. The power law can be written in log-log form as the following, allowing us to perform a linear regression.
			
			\begin{gather}
				\log(P) = \log(C) + n\log(R)
			\end{gather}
			
			The same is done in Figure \ref{fig:part_b:log_pr} and the value of $n$ comes out to be 2.41, and the value of $C = 10^{-3.00} = 0.001$
			
			\begin{figure}[h!]
				\centering
				\includegraphics[scale=0.4]{log_PR}
				\caption{Logarithmic plot of power applied to the lamp vs. resistance of the lamp}
				\label{fig:part_b:log_pr}
			\end{figure}
			
		\subsection{Part C - Intensity-Distance Relationship}
			
			As with the previous sections, the relationship we are aiming to establish is one in the form of $y = cx^n$. Here, our independent variable is the distance $d$, the one which we can vary, and our observed variable is the current through the photodiode $I_d$. Thus the log-log plot of the same will look like so
			
			\begin{gather}
				\log(I_d) = n\cdot \log(d) + \log(c)
			\end{gather}
			
			If we plot the same, we get Figure \ref{fig:part_c:log_di_raw}. The slope comes out to be $-2.87$, which is interestingly closer to an inverse cube law than an inverse square. The constant of proportionality comes out to be $c = 10^{2.86} = 741.10$.
			
			\begin{figure}
			
			\begin{subfigure}{0.4\textwidth}
				\centering
				\includegraphics[scale=0.65]{table_dist_intensity}
				\caption{}
				\label{table:part_c:data}
			\end{subfigure}
			\begin{subfigure}{0.6\textwidth}
				\centering
				\includegraphics[scale=0.34]{log_di_plot_raw}
				\caption{}
				\label{fig:part_c:log_di_raw}
			\end{subfigure}
			
			\caption{(a) Show the raw data collected by varying distance and measuring the current through the diode, and (b) shows the log-log plot of current vs. distance. \emph{Note} - The last data point, with current at 0 $\mu$A, was excluded from the log-log plot, as $\log_{10}(0)$ is undefined and would throw an error}
			\end{figure}
	
	\section{Scope for Improvement}
		
		If we look at the original plot of voltage vs. current in Figure \ref{fig:part_a:vi}, we can now plot the function $f(x) = 128 x^{2.41}$ and see how well it fits with our data overall. If we do so, we get the relation shown in Figure \ref{fig:discussion:vi_withcurve}.
		
		\begin{figure}[t]
		
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.3]{VI_withcurve}
			\label{fig:discussion:vi_withcurve}
			\caption{}
		\end{subfigure}
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.3]{log_di_plot_manual}
			\caption{}
			\label{fig:discussion:di_manual}
		\end{subfigure}
		
		\caption{(a) Plot of V vs. I with the modeled function $y = 128 x^{2.41}$, and (b) Plot of current vs. distance with some values manually adjusted below the least count of the ammeter}
		\end{figure}
		
		As we can see, the curve does not always fit our data. Some of that is expected, as all the data was not taken in one sitting and the temperature of the lamp between the two measurement sessions was not the same. However even if we look at the curve with voltage greater than 1 volt, we can still see that the curve is deviating from quite a lot of data points, which may be suggestive of the fact that this may not be a power law relation, and that more types of functions may need to be tried.
		
		Regarding the last part, one can see some large spreads when the distance between the source and the diode becomes large. This may be due to limitations of the instrument. The ammeter's least count is 0.1 $\mu$A, and in the data table, we can see some values for current being repeated, such as 0.2 $\mu$A, which are the points where the spread increases. It's possible that the change in current is smaller than 0.1 $\mu$A, and thus the points should not look flat. If we manually make up some values for the points that are repeating (for instance, replace the three instances of 0.2 with 0.29, 0.25 and 0.22), and then plot the log-log graph and perform the regression, we get Figure \ref{fig:discussion:di_manual}. The scatter is under the least count of our instrument, and thus should be insignificant.
		
	
\end{document}