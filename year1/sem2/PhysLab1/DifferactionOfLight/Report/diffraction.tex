\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{graphicx}

\title{Experiment 1 - Diffraction of Light}
\author{Cocoa Kaushal}
\date{Date Conducted - February 13, 2020}

\graphicspath{ {Figures/} }

\begin{document}

	\maketitle
	
	\noindent \hrulefill
	

	\noindent Date of Submission --- \textbf{February 13, 2020} \\ 
	\noindent Instructor --- \textbf{Amin Nizami}  \\
	\noindent Teaching Fellows --- \textbf{Manpreet Kaur, Philip Cherian} \\
	\noindent Lab Partner ---  \textbf{Kartik Tiwari}

	\noindent \hrulefill
	
	\section{Introduction}
	
		The goal of this experiment is to study the diffraction patterns of various objects and gain an understanding of how light diffracts, and how information about the geometry of the object can be inferred from the diffraction pattern. Throughout this experiment, three objects have been taken to study, a diffraction grating, a single slit, and a circular aperture. A laser beam is passed through the object and the light diffracts around it, and from the pattern produced, we can deduce the parameters of the object. Since the diffraction grating has known parameters, namely, the width of each slit that the light is diffracting through, we can use this to determine the wavelength of the light we are using.
		
		
	\section{Procedure}
	
			
		\subsection{Diffraction Grating}
		
			A diffraction grating is an object which has a large number of very fine single slits stacked horizontally next to each other. A diffraction grating produces a pattern with sharp maximas with complete destructive interference in between, and the equation for the maximas is given by
			
			\begin{gather}
				d \sin(\theta_m) = m\lambda
			\end{gather}
			
			Where $d$ is the spacing between two slits on the diffraction grating, $m$ is the order of the maxima, $\lambda$ is the wavelength of the light, and $\theta_m$ is the corresponding angle to the maxima, as shown in Figure 1. 	Since we know $d$ for a given grating, we can calculate the wavelength of the light $\lambda$.
			
			In order to do this, we shine a green laser\footnote{We had red and green laser lights at our disposal, and we chose the green laser to study the objects, as a shorter wavelength should, in principle, should provide greater resolution} through our diffraction grating and project it on the wall (The laser is kept close to the diffraction grating because that produced the sharpest image). The distance between the grating and the screen (the wall) is measured using a measuring tape and kept fixed at $D = 253$ cm $\pm 1$ cm. This value of $D$ is chosen because it is sufficiently large to make the relative error in the measurement of $D$ insignificant ($\frac{1}{253} \times 100 = 0.3$ \%). $\sin(\theta_m)$ is calculated by taking the ratio $\frac{y}{\sqrt{D^2 + y^2}}$ as $\theta_m$ cannot be directly measured. The resulting data is tabulated in Table \ref{table:diffgrating}, and the wavelength of light is approximately $\lambda = 489$ nm, which falls under our expectation of green light's wavelength.
		
			\begin{figure}[t]
			\centering
			\includegraphics[scale=0.4]{diffraction_grating}
			\caption{Diffraction Pattern Produced by Diffraction Grating}
			\label{fig:diffgrating}
			\end{figure}
			
			\begin{table}[h]
			\centering
			\includegraphics[scale=.9]{300lpmm_table}
			\caption{Wavelength of light $\lambda$ calculated with varying maximas}
			\label{table:diffgrating}
			\end{table}
			
		\pagebreak
		
		\subsection{Single Slit}
			
			The Single Slit is simply one slit of width $d$ through which light is allowed to diffract. The diffraction pattern produced has been shown in Figure \ref{fig:sslitdiff}. Here we can see a central maxima with some spread, followed by fainter and smaller maximas. The aperture of the slit, $a$, is related to the location of the \emph{minimas} by the following relation.
			
			\begin{gather}
				a \sin(\theta_m) = \pm m\lambda
			\end{gather}
			
			\begin{figure}[t]
			\centering
			\includegraphics[scale=0.3]{DiffractionPattern_SingleSlit}
			\caption{Diffraction Pattern of a Single Slit as Observed in the Lab}
			\label{fig:sslitdiff}
			\end{figure}
			
			The schematic diagram remains the same as in Figure \ref{fig:diffgrating}, however rather than a diffraction grating, we simply have a single slit. Thus the calculations remain basically the same, however this time our parameter that we are varying is $m$ and $D$ (the distance between the slit and the screen), and the resultant calculation is the value of $a$ (however, as expected, as we vary $m$ and $D$, $a$ does not change, because the width of the slit does not depend on which fringe we decide to measure). The resulting data has been tabulated below in Table \ref{table:SingleSlit}.
			
			The measurements were taking by clicking photos of the diffraction pattern and then measuring the pixels between fringes using ImageJ. To do so, a meter scale had been placed in the image in the plane of the projection of the diffraction pattern for reference. Since the center of the central maxima (from where we measure the distance to the $m$-th minima) is ambiguous, the distance between $\pm m$ was measured and then divided by 2, and taken as $y$. The approximate width of the slit comes out to be 0.008 cm.
			
			\begin{table}[b]
			\centering
			\includegraphics[scale=0.9]{SingleSlit_table}
			\caption{Width of Slit $a$ measured against multiple distances $D$ and orders $m$}
			\label{table:SingleSlit}
			\end{table}
			
		\subsection{Circular Aperture}
		
			The diffraction pattern of a circular aperture is concentric dark and light fringes, as shown in Figure \ref{fig:circapDiffraction}. This time, the relationship between the geometry of the object and the diffraction pattern is as follows.
			
			
			
			\begin{figure}[h]
			\centering
			\includegraphics[scale=0.3]{CircAperture_cropped}
			\caption{Diffraction Pattern of a Circular Aperture}
			\label{fig:circapDiffraction}
			\end{figure}
			
			\begin{gather}
				d \sin(\theta_m) = \bar m\lambda
			\end{gather}
			
			Where $d$ is the aperture's diameter, and $\theta_m$ is the angle of deviation between the center of the central maxima and the $m$-th minima. In order to measure this, we need to distance $y$ from the center to the $m$-th minima. Since the center cannot be directly determined easily, we measure the diameter of the $m$-th minima and divide that by 2 to get the length $y$ which will be our distance from the center to the $m$-th minima. Once we have $y$, we can calculate $\sin(\theta_m)$ as we had been doing so far, by calculating $\frac{y}{\sqrt{y^2 + D^2}}$. The resulting data has been tabulated below in Table \ref{table:circaperture}. The diameter comes out to be approximately 0.02 cm.
			
			\begin{table}[b]
			\centering
			\includegraphics[scale=0.85]{CircAperture_table}
			\caption{Diameter $d$ of circular aperture by varying $m$ and $D$}
			\label{table:circaperture}
			\end{table}
	
	\pagebreak
			
	\section{Analysis}
	
		By studying the diffraction pattern of an object, we can study it's geometry even when we cannot directly measure the dimensions of the object. In each case, the measurements were around less than a tenth of a millimeter at least, which would not be possible to study directly, and even if it were it would be prone to measurement errors. The diffraction pattern's dimensions are inversely proportional to the dimensions of the object (for instance, a smaller slit produces a wider diffraction pattern), which allows us to study it much more accurately
				
		The three diffraction patterns can be related to each other by understanding the geometry of the object that the light is diffracting around. Around a single slit, one gets a wide central maxima, but as we keep adding more slits, the points of constructive and destructive interference increase, giving us sharper maximas. The circular aperture is also related to the single slit, by simply rotating the single slit by $\pi$, and the new object formed will be a circle with diameter as the height of the slit. This is reflected in the diffraction pattern as well, as we can see that if we rotate the pattern in Fig \ref{fig:sslitdiff} by $\pi$, we would get the diffraction pattern in Fig \ref{fig:circapDiffraction}. The choice of the green laser was because a shorter wavelength is expected to provide a better resolution, however as we can see, the dimensions of the objects measured are much larger than the wavelength of the laser, thus the choice of laser would not have made a difference.
		
	\section{Scope for Improvement}
	
		The measurements for this experiment were taken by clicking an image of the diffraction pattern with a meter scale in the image to allow is to convert pixels to centimeters. This method, while much better than simply eyeballing it, still has a few sources of error that were encountered while actually taking the measurements. When taking pixel measurements of the scale, it was noticed that the camera was not completely parallel to the wall, thus giving slightly different values for each measurement of the scale itself at different points. This was accounted for by measuring just 1 centimeter on the scale, rather than the whole scale. This way the scale was approximately flat along the line of measurement. In the future, this can be better accounted for by fixing the camera on a stand and clicking the image.
		
		
		
\end{document}