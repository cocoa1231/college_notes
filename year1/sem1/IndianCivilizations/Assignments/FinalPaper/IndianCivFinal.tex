\documentclass[12pt]{article}

\usepackage{fontspec} 
\setmainfont{Times New Roman}
\usepackage{setspace}
\usepackage[backend=biber,sorting=none]{biblatex}
\usepackage[left=1in, right=1in, top=0.8in, bottom=1in]{geometry}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=black,
}
\usepackage{graphicx}
\graphicspath{ {./images/} }

\doublespacing

\addbibresource{IndianCivFinal.bib}

\title{Images of the nation: Colonial Influences on Indian Art}
\author{Cocoa}
\date{Indian Civilizations FC-0003-1}

\begin{document}

	\maketitle
	
%	Indian art has had it's own unique forms of painting such as the miniature and mural, however throughout colonial rule, and specially as India moved closer to independence, the influence of western styles of art became more dominant. A fusion of the western styles of art and Indian styles began taking place and a new subset of Indian-Western art emerged which was simultaneously political, as all art is, and reflective of contemporary lives of the people the artists chose to portray. Throughout this paper, I would briefly like to trace out a history of this transformation of Indian art, focusing on popular groups such as the Bengal School of Art and the Progressive Artists' Group (PAG). One notable early influence on both of these was the Hungarian-Indian artist Amrita Sher-Gil whose body of work preceded both of the aforementioned groups. We shall begin our discussion with her.
	
	The transformation of Indian art during the 20th century has been one which has assimilated both western modernist post-renaissance styles with local Indian and Asian styles of art. This effect is caused by the fact that the identity of the nation was still forming during this period, and as we will see, this struggle for identity was often seen in the contemporary artists trying to use while simultaneously reject the Western ideas of modernism. The nation's identity yearned to be a modern one, a concept that is inherently an ideological import, yet not abandoning it's Indian and south Asian tradition. The art through this century is one that brings into question the idea of what tradition is, what an authentic Indian style of art is, and if there should be one.
	
	This paper presents a brief overview of the history of contemporary Indian art as discussed by Shiva Kumar~\cite{kumar1999indianart}, along with looking into the specific case of Amrita Sher-Gil, a Hungarian-Indian artist, who was trained in Western art styles, and who discovered India through it's impressions in Hungary. She forms an interesting case study, as she was an upper class bisexual woman, born out of India, and rediscovering India as an Indian. Her art, therefor, allows us to see how she was able to use her relative position of privilege to produce a body of artwork that depicts India from the eye of a queer woman, while not letting go of the colonial narratives
	
	This paper is divided into two eras. The first part looks at the Tagore family, Sher-Gil, and a few contemporary artists, all pre-1940, and the second part looks at the schools of art that emerged that attempted to reject the amalgamation of the Oriental and Occidental art forms, and attempted to show a more humanist and international approach to art. 
	
	\section{Redefining Tradition: Pre 1940 Artists}
	
		We shall start with the Bengal School of Art which was pioneered by Abanindranath Tagore (1871-1951). His vision of developing an art style that reflected the culture of the nation was one which returns to the miniature. His justification for doing so was that India needed to form an identity that is independent of the colonialist, which fell in line with the swadeshi movement, and he believed that the miniature was a distinctly Indian style of painting, one that could integrate the spirituality he ascribed to the nation, and which he viewed as antipodal to the ``materialistic" West~\cite{atteqa2004tagore}. At this point in the history we can see that the conception of the nation was still one which created the dichotomy of the spiritualistic ease and the materialistic west, as discussed by by Milton Singer~\cite{Singer}. His attempt at constructing a cultural national identity also included using mythological figures, and constructing the concept of \emph{Bharat Mata} in his painting
		
		Abanindranath Tagore had, what one would consider at that time, a British upbringing. His view of India, thus, naturally fell into this dichotomy, as it was created by the West. His decision to pioneer a modern style of the miniature was received with heavy criticism from the students of the Bengal school as well as the contemporary artists of the time. One such artist was Amrita Sher-Gil (1913-1941), saying
		
		\begin{quote}
		According to her, this school had come to be known as such ``more on grounds of priority than of merit, for in spite of its illustrious antecedents in Ajanta and the equally admirable later schools of Indian miniature painting, which the Bengal movement strives to emulate, it cannot claim to have captured the spirit of Indian art of bygone days". She went on to add that ``far from fulfilling its vast ambition, this school is responsible for the stagnation that characterizes Indian painting today. The tenets of the Bengal School seem to have a cramping and crippling effect on the creative spirit"~\cite{iqbal1975shergil}
		\end{quote}
		
		Sher-Gil's response to the Bengal school was to approach portraying the lives of Indian people in a much more intimate setting. She would often portray the pain of the lives of her subjects, employing the artistic methods of post-impressionism while attempting to assimilate local traditional forms of painting. Siva Kumar says that this process led to Sher-Gil developing a style that ``transformed into a removed realist idiom infused with a personal romantic"~\cite{kumar1999indianart}. Most importantly, however, is that her art departed from the spiritualistic aspect of Indian art that had come to define it, and established a more materialistic, or arguably, an anthropological one. Her works redefined the conception of ``traditional" art.
		
		Sher-Gil was an interesting figure in the history of modern Indian art. She came from a privileged background, and discovered her Indian heritage as an outsider to the country. She was born in Hungary and was brought up on romanticized Oriental images of India, thus it comes at no surprise that when she arrived in India, her view was still a heavily colonial one, looking at her subjects from an Orientalist gaze.
		 
		\begin{quote}
		  As soon as I put my foot on Indian soil, my painting underwent a change not only in subject and spirit but in technical expression, becoming more Indian. I realised my mission then: to interpret the life of Indians, and particularly of the poor Indians, pictorially, to paint the silent images of infinite submission and patience, to depict the angular brown bodies, strangely beautiful in their ugliness, to reproduce on canvas the impression their sad eyes created on me~\cite{tillotson1997shergil}
		\end{quote}
		 
		One can also argue that Sher-Gil found in her Oriental subjects a place to project her desires, which is evident in her works such as \emph{Siesta} (1937). She says ``As a matter of fact I think all art has come into being because of sensuality; a sensuality so great that it overflows the boundaries of the mere physical."~\cite{kumar1999indianart}. When we look at Sher-Gil's influence on the turning point of Indian art history, we must keep this in mind, as this becomes an important critique posed by the post-1940 artists.
		 
		 What the artists during this period attempted to do was to create a cultural identity for the nation as one was difficult to construct from the disjoint lives of the people of the subcontinent~\cite{mukherjee2014nationalism}. The artists of the post-1940 era would depart from this ideal, rather attempting to create an international and humanist style of art while reflecting their Indian identity in it.
		 
	\section{Progressive Modern Art: Post 1940 Art}
	
		At the turn of 1940, a new set of artists began to emerge known as the progressives. These artists began to depart from the issue of addressing the cultural national identity of the people of the subcontinent, and started portraying the people of the subcontinent. A number of groups emerged, the most prominent of which was the Bengal Progressive Artists' Group (PAG), formed in 1947, just after the partition of India. It consisted of F. N. Souza, S. H. Raza, M. F. Husain, K. H. Ara, H. A. Gade, and S. K. Bakre. Their vision was the reject the revivalist methods of the Bengal school and create an Indian art which is ``international and interdependent"~\cite{kumar1999indianart}
		
		The aim of the progressive artists group was to use to tools of Western post-modernist art and generalize them, to use them to create new modernist art depicting India. An instance of this is F. N. Souza's \emph{Untitled (High Street, Goan Village)}, which employs post-modernist figurative methods of painting to depict a street in the artist's home village. It employs the use of vivid contrasting colors, reminiscent of some of the works of French artists such as Henri Matisse, but depicts the ordinary microscopic view of a village street in India. The artist's view was that this method employs Western styles, yet generalizes them to depict, what has now come to be, the traditions of India. Similarly, artists such as S. H. Raza and M. F. Husain used styles such as figurative and cubist art to portray the culture of India. An instance of this is Raza's \emph{Untitled Landscape by S.H. Raza, 1948}, where he uses a style that can be seen in more popular western painters such as van Gogh's \emph{Wheatfield with Crows, 1890}, with the use of a color palette that blends the boundaries of objects in the image, and the use of similar quick wide brush strokes. Here too, however, we see them using these styles of painting to portray a microscopic view of Indian life.
		
		Through both of these examples, one might argue that even though these paintings depict India as seen through Indians, they still fail to portray India through Indian art forms. This is what J. Swaminathan (1928-1996) argued in the 1960s, after the decline of the Progressive Artists' Group.
		
		\pagebreak		
		
		\begin{quote}
		
			Distancing himself from revivalisms old and new, he argued that contact with Western art has been inhibiting Indian artists from finding themselves. He called into question the concepts of progress and modernism and countered it with the concept of the con- temporary in an effort to overcome ethnocentric readings of culture. He demonstrated his position in the collection he put together at Bharat Bhavan in Bhopal, where folk, tribal, and urban (modern) art were brought into juxtaposition within a single museum. Even those who did not fully agree with his thesis were convinced of the creative vitality of the tribal and folk art on display, and this has since led to similar presentations in a number of subsequent exhibitions of contemporary Indian art~\cite{kumar1999indianart}
		
		\end{quote}
		
		The progressive artists group had failed to recognize the pluralism of the culture and styles available to them, according to Swaminathan. K. G. Subramanyam (b. 1924) thought that the only way to create art that is representative of the culture of India is to have a ``enlightened eclecticism"~\cite{kumar1999indianart}, and the be open to the variety of works that existed around them. Rather than rejecting Western modernism, Subramanyam wanted to integrate it as just a part of a richer whole of art styles available to the contemporary modernist Indian artist
		
		\begin{quote}
			
			By connecting his work to it, his work stands to gain an added reso- nance comparable to what traditional artists got from functioning as part of a larger cultural whole. Given the role cross-cultural contacts play in the formation of modernisms, Indian artists cannot and should not shut out what comes to them from other parts of the world
			
			[...]
			
			As contact with the Other and re-reading of the past are both integral to the process of modernization, he does not, like many of his contempo- raries, give an essentialist reading of either, but under- lines the need for seeing modernism as a continuous process of rethinking, adding that the focus should be on the issues and not on the form. What anchors the mod- ern artist in this plural and changing world is the artist's sensibility and his or her environmental contact. An enlightened eclecticism, Subramanyan believes, is the appropriate mode for creativity in a plural~\cite{kumar1999indianart}
		\end{quote}
		
	\section{Conclusion}
	
		The art that came about throughout the colonial era of India has had to struggle with the task of nation building on a cultural level. While political leaders such as Gandhi, Nehru and Jinnah were struggling to creating a political and social theory of the Indian nation, the contemporary artists of the time were struggling with the questions of a national Indian identity and culture. The art of a culture is an important representative cultural export, and as Milton Singer has established, the images of India that arose in the West did influence how India perceived itself, and the interplay of the image and self-image is constantly changing and shifting. Thus the art that the contemporary artists create very much painted these images of India.
		
		The issue that these artists faced was a similar one that the political leaders did, namely, how does one go about defining what an Indian nation is, in any context (social, political and cultural), when India itself is such a diverse, fragmented and often disjoint collection of people and experiences? The artists had the option to address this question in various ways, and their approach was often reflective of the political climate of the time as well as the sociopolitical standing of the artist. 
		
		Abanindranath Tagore's works, such as \emph{Bharat Mata}, often use Hindu mythology while claiming to be nationalist works of art, which is representative of what the dominant social narrative at the time was, namely, the conflation of a Hindu identity and an Indian identity as often done by Gandhi. Amrita Sher-Gil represented the point of view of a colonial outsider with some connection to India, who was able to express her view of India as a queer woman and a colonial outsider, simultaneously. The Progressive Artists' Group, which formed after the nation was partitioned, saw itself as the avant-garde for a new modernist conception of the Indian nation which borrowed from the Western culture, much like Nehru's vision for India. They believed that the ideas developed outside of India (in the Progressive Artists' Group's case, the post renaissance forms of Western art, and in Nehru's case, rationality and scientism as foundations for the nation-state) were more universal and could be used in India without sacrificing an Indian identity.
		
		
	
	\printbibliography

\end{document}