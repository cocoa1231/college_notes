# Introduction to class, syllabus discussion, attendance and grading, resources

## Class information

### Teaching Fellows

1. Aastha Gandhi (aastha.ofc@gmail.com)
2. Nivedita Bose (niveditabose@hotmail.com)

### Syllabus

[See attached document](../Syllabus.pdf)
*Note: The mid-term exam will be a "take home exam", i.e, an assignment given a week before when it's due*

### Qualities of a good assignment

- Represent the author's argument
- Shows depth of analysis, addresses the assignment question thoughtfully
- Has to have a clear argument with evidence
- Organized logically
- **Don't plagiarize**

Things to keep in mind:

1. What are the concepts 
2. What are the methods they are using, how do they produce the evidence for their claims
3. What is the argument?

## Orientation for next week

* <u>What is Civilization?</u>

  * the <u>stage</u> of human social and cultural development and organization that is considered most advanced.

    "the Victorians equated the railways with progress and civilization"

  * the <u>process</u> by which a society or place reaches an advanced stage of social and cultural development and organization.

  * the society, <u>culture</u>, and way of life of a particular area.

## Readings

- [4500-year-old DNA from Rakhigarhi reveals evidence that will unsettle Hindutva nationalists](https://www.indiatoday.in/magazine/cover-story/story/20180910-rakhigarhi-dna-study-findings-indus-valley-civilisation-1327247-2018-08-31)

