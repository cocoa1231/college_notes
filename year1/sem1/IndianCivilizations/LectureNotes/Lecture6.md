# Lecture six - The Vedas

- "Rig Veda" is a collection of 1028 poems (or "mantras") grouped in 10 circles ("mandalas")
- After this we have 4 other Vedas
  - Yajurveda - Knowledge of sacrifice and rituals
  - Samveda - Book of songs
  - Atharthveda - Practical mundane matters, more everyday than rituals
  
    Bramanas - Explanatory and interpretive texts of rituals in the Vedas
  - Upanishads - Reflections of the Vedas and the rituals in it
- Historical data that can be gleaned
  - Strong sense of color discrimination
  - Author of the "Rig Veda" were generally pastoralists
  - Social structure was patriarchal and headed by Chiefs
  - Importance of rituals
  - We can also draw inferences from what is *not* mentioned in the text, such as the lack of flora and fauna, the focus on lions rather than tigers (the writers came from north), the Ganga is mentioned very late in the Vedas (meaning they took a while before they reached that geographic area)
- Women
  - The role of women was romanticized in the Vedas
  - Birth of a son was celebrated
  - We see atypical social structures such as polygamy and polyamory
- They worshiped intoxicating substance called *soma*
- Sacrificing was a method of communication between the local community and the cosmos
- The idea of "daan" was seen as a method for forming relationships and kinships. It was perceived as a gift that created obligation and must be acknowledged 

