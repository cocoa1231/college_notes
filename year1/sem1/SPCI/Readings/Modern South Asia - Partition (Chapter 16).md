# Modern South Asia - Partition (Chapter 16)

* Identities formed when the British enumerated the people of India into categories such as Hindu and Muslim

  * This had a more successful effect in creating political groups, rather than actually creative a cohesive category of "Hindu" or "Muslim"
  * The identities of people as "Hindus" or "Muslims" were further solidified when formed against the colonial power (i.e, the religious identities formed an in-group and the colonial powers formed the out-group) 

* > The innumerable divisions of Islam in South Asia even today suggest that the construction of an Indian Muslim identity, much less a coherent one, in the late nineteenth century occurred more in the mind of latter-day scholars than in the actual unfolding of societal rules and relations [p 137]

* After the rebellion of 1857, the British officials began looking at the "Indian Muslims" as a separate political identity

  * They viewed the Muslim lower classes as prone to *religious revivalism* (def. **Religious revivalism** is term applied to mass movements which are based upon intense **religious** upheaval. Periodic **religious** revivals which seek to restore commitment and attachment to the group are a regular sociological feature of **religious** traditions)
  * The Muslim lower classes required a counter to the narrative put out by the assertive Hindu educated classes(?)
  * This made the Muslim landed elite seem like a natural ally
  * **Saiyid Ahmad Khan**  used the framework of the 'two nation theory' to argue for political reservations for Muslims
  * The British, subsequently, granted separate electorates to Muslims in local governments under **Ripon's reforms, 1882/3**

* > If pitting Muslim communitarianism against Indian nationalism had the potential to misfire, playing the region against the centre could secure British imperial interests [p 139]

  What I understood from this was that if the British couldn't divide along the lines of religion, i.e, a divide between the Muslim religious identity and the Indian national identity, then dividing political power between the province under the control of the local government and the unitary center could secure British imperial interests

  * I think this is like gerrymandering? Making Muslims a minority in most constituencies means forcing them to ally with other political powers in their constituency, and thus not having to put forth a common religious identity? Maybeee?

* > During the next round of constitutional negotiations in the early 1930s there was no single all-India Muslim political party which could put forth a plausible claim to speak for all Indian Muslims [p 140]

* The **Communal Award of 1932** granted Muslims majority representation in Punjab and Bengal, but this meant the elimination of British officials from provincial councils which was seen as a safeguard for minority interests, thus failed to enthuse Muslims in minority provinces. **This is when Muslims from minority politics turned to Jinnah**

* Basically, the Muslim league wasn't doing so well, winning a total of 4.4% in the total Muslim votes cast. The Muslim League still survived by being elected in provinces which were completely Muslim majority

* After the massive 1937 victory of Congress, it saw no reason to seek help from outside parties. However, the Congress still didn't have support of the Muslim majority provinces, namely Punjab and Bengal, which is where Jinnah saw his entry

* With the federated structure that was proposed, Muslim minorities feared that their voices would not be heard at a national level. Thus, the majority Muslim provinces threw their support behind the Muslim League

  *  Even with separate electorates and weighted elections, the Muslims were still in a minority. They could not expect anything but a marginal role in settling how power was to be shared in an independent India

* This is where **Saiyid Ahmad Khan's** 'two nation' narrative can help put forth an argument for equal voices of the Hindu nation and the Muslim nation

* In December 1930, **Muhammad Iqbal** proposed the idea of the creation of a separate Muslim state in NW-India. In 1933, **Chaudhri Rahmat Ali** coined the term Pakistan

  *  This idea never picked up any traction because of the massive transfer of Muslim population from other parts of India it would require
  *  A number of alternative schemes were proposed, however most of them used the 'two nation' theory, proposing for a Muslim nation and a Hindu nation

* With the outbreak of WW2 and Congress demanding immediate independence, the viceroy needed some way of diluting or delegitimizing the voice of the Congress

  *  One way of doing so was by showing it doesn't speak for all Indians
  *  This was done by giving power to the Muslim League, and showing that the Congress does not speak for Indian Muslims
  *  Jinnah was now in the position to formulate a demand out of the contradictory requirements of Muslims in majority and minority provinces

* Thus, in **March 1940**, the All-India Muslim League at it's annual session in Lahore formally demanded a independent Muslim states in the north-west and north-east on the grounds of the 'two nation' theory

*  