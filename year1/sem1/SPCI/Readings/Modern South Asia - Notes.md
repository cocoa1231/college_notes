# Chapter 3: Pre-Modern Accommodations of Difference: The making of indo-islamic cultures

## Notes


* Opposing views on the extent to which  the coming of Islam to the subcontinent fomented new processes of cultural accommodation and assimilation
  * One says there's a clear line between the Islamic civilization and the "pre existing corpus of 'Hindu Tradition'"
    * (-) There were a lot of lower class Hindus converting to Islam (inshallah kashmir)
    * (-) The contiguity of people belonging to different faiths meant that Islam developed local Indian roots
    
  * The other argument emphasizes 'syncretism' (def. The combining of different beliefs, while blending practices of various schools of thought). 
  
    * This helps erase the problem of difference, allowing you to paint a picture of a shared unified heritage
  
    * For instance,
  
      > Richard Eaton’s portrayal of Bengali peasants as a ‘single undifferentiated mass’ with a uniform ‘folk culture’ neatly erases the problem of difference. With the major historiographical challenge conveniently out of the way, a fanciful cultural argument can then be erected on quicksand-like material evidence from Bengal’s agrarian frontier.
  
* Early conversions to Islam (pre 14C) took place more gradually. Capitulation and submission was the norm, followed by laying down of terms of loyalty and dependence


  * This is why conquest did not lead to too much political change

  * > The Indians sometimes go to war for conquest, but the occasions are rare. . . . When a king subdues a neighbouring state, he places over it a man belonging to the family of the fallen prince, who carries on the government in the name of the conqueror. The inhabitants would not suffer it otherwise.

  * *Chachnama* elaborates a royal code which demands sensitivity to the fluidity and shifting nature of the real world of politics. This is contrasted with Chanakya's text *Arthashastra*, which advises princes on ways to avoid the dilution of absolute and centralized power

## Whatever shoddy timeline I can assemble to keep things straight in my head

- First wave of Arab political expansion reached the subcontinent @ Makran coast when NW India was invaded in **644**
- Makran was subjugated under the first Ummayid caliph, Muawiya
- Islam reached the east when Md. bin Qasim conquered Sind in **712**
- The relative prosperity of India and decline in west Asia led to the Ghaznavid invasions beginning in **997**
- Md. Ghuri (Turk) invaded India in **1192** and defeated Prithviraj Chauhan. This helped establish the first Muslim sultanate in Delhi by Qutubuddin Aibak from **1206 - 1526**

## Questions

1. >The great Central Asian scholar Al-Beruni, who visited India in 1030, wrote: ‘The Hindus believe with regard to God that He is One, Eternal . . . this is what educated people believe about God . . . if we now pass from the ideas of the educated people to those of the common people, we must say that they present a great variety. Some of them are simply abominable, but similar errors also occur in other religions.’ 

   What does that last line mean exactly? "Some of them are simply abominable, but similar errors also occur in other religions". Is Al-Beruni calling the views of the common people on God "abominable"?

   >In making this comment Al-Beruni was not simply giving a Muslim view but echoing the Hindu elite’s position on monotheism and polytheism

   What exactly is the view on monotheism and polytheism that is echoed here? As far as I can make out, it's claiming that the common people do not think that God is singular and eternal and then calling this view "abominable", and then Al-Beruni tries to justify the existence of this view in the Hindu community by saying that such views exist in every community

2. Simple one: What exactly is "Indian Ocean world economy"?

3. Who were the Rajpuits? But more importantly, how do I get the Historical context for reading this?