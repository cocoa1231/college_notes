# Partha Chaterjee - Moment of Maneuverer

He repeatedly uses the phrases "problematic" and "thematic". For instance
> What appears as a critique of Western civilization is, therefore, a total moral critique of the fundamental aspects of civil society. It is not, at this level, a critique of Western culture or religion, nor is it an attempt to establish a spiritual claims of Hindu religion. [...] At this level of thought, Gandhi is not operating at all with the problematic of nationalism. His solution too is meant to be universal applicable as much to the countries of the West as to nations such as India (Page 93)

This is because what Gandhi is saying is that the issues he points out is not unique to India, but is universal. It's something that applies to mankind at large. Thus, he is not operating with the problematic of nationalism, because the charge isn't against a given nation, but against the idea of modern civilization. 

The "thematic" then, as the professor explained, is the modality of the solution

> Not only that; what is even more striking but equally clear, is that Gandhi does not even think within the thematic of nationalism. He seldom writes or speaks in terms of the conceptual frameworks or modes of reasoning and inference adopted by the nationalists of his day, and quite empathetically rejects rationalism, scientism and historicism (Page 93)

Nowhere did Gandhi talked about the culture or history of India. His mode of argument was different from that of the nationalists, i.e, "why we are a self sufficient nation" and "why we deserve to be a sovereign nation [L2.Definitions]". Gandhi didn't say anything unique to India, and he didn't believe in Sovereignty. Thus his solution to the problems he posited is also a universal solution, not using the modalities (or arguments) based on nationalist sentiment. 