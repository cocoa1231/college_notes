# The Drama Queen - Why we need to take him seriously

1. Where did Gandhian thought become popular. Why did it strike the chord?
2. Why was it needed?

## Gandhism and the preexisting Nationalist movement

* What was considered as "The Nationalist Movement" was primarily an elite affair
  * It argued that we need Indian accommodation in the administration of India. Their project was a hegemonic [1] project
  * This was far removed from the actual world of the villagers. *The demands of the bourgeoisie were far removed from the interests of the common man*
  * This meant that they peasant community had no motivation to participate in administration
  * What Gandhism did was bring the idea that to gain the support of the peasant community was vital
* In order to mobilize the masses at the beginning of the nationalist movement, the elites had to accommodate Gandhi's demands which lined up (or rather, which came from) the demands and interests of the common man. The villager.
  * The elite needed a populist revolution
  * The only way they could do that was through Gandhi, because he found resonance among them
  * **But**, once they had this support, Gandhi is sidelined (but this comes later in the nationalist movement)
* The kind of strength Gandhi lends to symbols
  * **Khadi**: He linked the image of the common man with wearing khadi cloth, and even now, when we think of the picture of a politician, we think of them in a white kurta pajama/dhoti
  * **Salt**: Salt was never considered as something, well, important. When Gandhi took up the idea of salt as a symbol for an essential resource for the common person that was monopolized or, sort of, now owned(?) by the government (and not only that, it was earned by a *non-Indian Government*), it wasn't initially seen by the British government as something to care about. But that link between salt and government monopolization of essential resource struck a chord with the villagers, which helped in mobilizing them against the Government
  * He was, essentially, an **economical strategician** and a born politician

## Views of Gandhi

* He rejected capitalism because he said that the idea of "private property" is false. He argues that one comes into existence and flourish only through interaction and in relation to others. If I acquire something, it is because of others make me

* While he saw untouchability as a sin, his views on Caste left a lot to be desired
  
  * He saw caste as simply a division of labor, whereas the caste system is a _division of laborers_: It puts them in a social hierarchy based on their labor
* He argued against mechanization because it *robs people of the dignity of labor*. 
* His view that linked the masses was a complete rejection of modern civilization. 
  
  <u>The problem</u>: We need a common theme to catch the imagination of the people, a solution for the common man
  
  <u>The solution</u>: Gandhi said that the solution was a complete rejection of modern civilization (which was coincidentally also British civilization at the particular time these ideas were coming up). He said that the solutions posited by modernity for the problems it created came from within modernity. It said that these problems that were created by the "advancements"  we made with our our intellect can be solved by using our intellect, reason and the scientific method. What Gandhi asked was how far can you take this idea. He said that we should reject modernity and in doing so, he created a common cause that resonated with the common man, and thus gave them something to rally behind
  *The common cause* is adopting a new sense of values: A complete rejection of civil society [2]
  
  * He rejects institutions like the parliament. One of the fundamental ideas of civil society is sovereignty (defined in Lec2). But the moment you say that and the moment you elect somebody to represent your interests, they will not represent your interests, but rather the collective interests of the representative's voters.  
  * He says what makes us a subject people is not the British but rather the idea of modern civilization, and the parliament is just one part of it

## "The Problematic" and "The Thematic" - Nationalism

* The ideological framework with which we see and approach a problem is called *the problematic*. In case of nationalism. 
  * The problematic is defining India as a nation
  * What is the alternative? Where we are now - the colonial period - is not acceptable. So what is the alternative? On what basis do we decide that alternative?
* The way we go about achieving what we want to achieve is *the thematic*

## Definitions

1. *Hegemony* - Rule with consent. That whoever is ruling has the consent of the people. This is in contrast with *Domination*
2. *Civil Society* - An organization that acts between the people and the state, which keeps a check on the power of the state and represents the interests of the people. Here, however, how Partha Chatterjee uses the term is to refer to civility, or rather just, the elite