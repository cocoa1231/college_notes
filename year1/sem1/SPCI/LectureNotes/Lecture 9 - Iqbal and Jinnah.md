# Iqbal and Jinnah

> These notes are a bit rambly and not well structured

## Why did the question of minorities suddenly come up

- Any modern state needs to have certain technologies to work
- One of that is the statistics of the population
  - The moment enumeration happens, you have a majority and a minority
  - When people are asked to pick a category such as "Hindu" or "Muslim", there is a *forced homogenization*
- In the late 1920s, the Muslim community becomes *self conscious* because of said enumeration, because now one is aware of their in-group
  - This self consciousness leads to the formation of the Muslim League
  - The issue they were bringing up was that given the numbers, the Hindu population can always overwhelm the Muslim population
- Globally, after WWI, the Ottoman Empire (the largest Muslim empire) had just broken up
  - In some ways, the world was looking at India to give guidance to the Muslim community(?)
  - This started off the Khilafat movement

## The Three Speeches

- To develop not only the British, but also the Congress (as a representation of Hindus) as the others
- Jinnah and Iqbal insisted that we don't think of Hindus and Muslims and majority and minority, but rather as two different nations
  - Here the concept of the nation is different. This isn't a territorial nation, but rather a cultural one
  - Islamic thought stresses on Nature and History as the source of knowledge(?)
  - This is how they argued that Muslims were a nation in themselves. It had everything that one would expect from a nation, a history, culture, literature, territory, etc.
  - **This is where the two nation theory comes from**

---

## Suggested Readings

- The second message of Islam
- The sole spokesman - A biography of Jinnah