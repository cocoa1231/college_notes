# How did we arrive where we are? 

## Course Information

### Teaching Assistants

1. Raka Roy
2. Annie - 9999346645, ainee.farooqui_tf@ashoka.edu.in?

### Time frame

- From abolition of Sati to constitutional parliament [1820 - 1950]

### Sections

The course will be divided into the following sections

1. Nation
2. Religion and modernity
3. Caste
4. Gender

Each section will have a primary text and a secondary text that is a reflection on the primary text

### Requirements

* For each module, we have to write one short paper (~ 2000 words)
  * We will be given two topics to choose from
  * You can request to write your paper on a topic of your choice
  * On average, each module will take 3 weeks. Could take longer
  * Each paper carries 10%, so total 40%
* Final research paper - 30%
* Class participation, attendance (in DS and LS) - 30%