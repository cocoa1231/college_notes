# Imagined Communities - Benedict Anderson

> Whenever a nation is imagined, it is imagined as a glorious past heading towards a limitless future

1. What happened a few hundred years ago that this idea of a nation came up?
2. How did it become so ubiquitous?
3. If it's such a novel idea, how does it invoke such passion?

Why is this lecture just repeating everything in the last lecture?

