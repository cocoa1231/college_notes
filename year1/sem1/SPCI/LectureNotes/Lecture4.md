# Imagined Communities - Continued

1. Did India imagine itself as a nation like the British did?

## How was India imagined? - Hind Swaraj

* For Gandhi, civilization is a moral enterprise. "How do we lead a moral life" is the primary goal of civilization
* His problem is primarily with these three things
  * Colonialist Imperialism
  * Rationalist materialism
  * Industrialist Capitalism

