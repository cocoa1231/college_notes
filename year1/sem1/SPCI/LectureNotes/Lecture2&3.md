# Introduction to Nations

* What does 'political' mean?
  * Etymologically: Comes from polis, i.e, city (X-check)
  * The (administrative?) activities that go on in a city/nation/state
* What does 'social' mean?
  * A system which creates a set of rules w...
    Write a better definition after class

## Nations

* What's a 'nation'? 
  * A group of people subscribing to a larger common ideal
  * The citizens of a nation don't pledge loyalty to a tangible authority, rather something more abstract built with a shared historical context
  * It is geographically a set of closed mass/masses of land
* The idea emerged in early 18C
* It is an *imagined* community: Even for the smallest nation, not all of them can know each other, yet it makes disconnected people part of a greater whole
* A nation always produces *closures*. It is an in-group, and the creation of an in-group must create an out-group, thus it is *limited*
* It is *sovereign* [1]. The nation answers to nobody but it's citizens, even in case of monarchies. Every monarch has to, at the very least, say that the actions of the monarch are for the betterment of the citizens
* (Professor's Claim) It is a *horizontal community*, i.e, every member of the community is equal. Once a membership is given, the idea is that the membership in any nation is horizontal. The idea is that "citizenship" is a binary quality. That the nation doesn't treat certain members as citizens while others as subjects. 

### Cultural roots of the nation

* The idea comes from 18C Europe
* The idea of "private property" did not exist before this
* Before 18C, dominating cultural power centers were
  * The Monarch
  * The Church

#### Challenge to traditional authority

* With the discovery of the new world, the new class of people were a challenge to traditional authority. This led to the erosion of traditional authority
* The dynastic wealth was being eroded
* With the spread of science, the authority of the church was also being eroded
  * The authority to explain the world no longer rested with the Church, but with reason
  * The authority to rule was given to the king by the word of God, and this authority was essentially given by the Church
* The kings had "A divine right to rule" and a "sacred language" (in Europe, Latin, which was considered superior to all others)
  * Texts were super rare because they had to be hand-written, but with the Gutenburg revolution, vernacular languages started flourishing
  * As people became more connected, the need for the Church to be a facilitator between the person and God diminished

#### Origins of national consciousness

* This started with the origin of print. Once printing came about, "the word" became available to everybody

  > In the beginning there was word, and the word was with God and the word was God
  > (Literally the first line of the Bible)

  This idea was eroded

* With knowledge available to everybody, a more horizontal and secular social structure started to emerge

* With the advent of capitalism, printing rapidly flourished

  * Anderson calls this ["Print Capitalism"](https://www2.bc.edu/marian-simion/th406/readings/0420anderson.pdf)
  
* Now the past was "fixed", it was static. The glorious past is now accessible to the public and it's now a static object, because it was standardized through print

* The country was now imagined as a singular whole

* Now people could be asked to subscribe to and work for this singular glorious past, their shared nation

* Anderson argues that this idea of a singular whole and a glorious past which originated in Europe was spread to it's colonies. The idea of the nation was transplanted from Europe to India 

### Paradoxes of the nation

The three paradoxes can be found in [this](http://faculty.georgetown.edu/irvinem/CCT510/Sources/Anderson-extract.html) article, elaborating on Benedict Anderson's idea of imagined communities. The professor didn't really finish this thought

1. Modernity of a nation: The objective modernity of nations to the historian's eye vs. their subjective antiquity in the eyes of nationalists
   1. Once the idea of a nation is created, it becomes an eternal idea. That it has existed since forever and will exist forever
   2. Nations however strive to create a modern, progressive and innovative image

### Questions

* Just because the members of a community don't know everybody, does that mean it's not a community?

## Definitions

1. *Sovereignty*
   1. The idea came during the enlightenment. That "I am a thinking and rational individual, and that I do not need somebody else to tell me what I should do with my autonomy"
   2. This idea is then mapped onto an entire nation. The individual is then substituted for the entire nation 