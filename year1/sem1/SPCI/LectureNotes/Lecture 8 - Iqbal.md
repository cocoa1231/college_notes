# Iqbal

## The Nehru reports in early 1920

* Complete universal adult suffrage - The right to vote for women
  * The people who demanded it were also people who came from societies where this wasn't present. You had to have some qualifications to vote - Education, Property ownership, Paying taxes. The Nehru report, on the other hand, provided a vote to everybody above 21
*  No representation for minorities
  * Either separate electorate for minorities or separate reservations were demanded by minorities. The report flat out rejected it
  * It said that "what your religion, caste, gender, etc. is, when you participate in the public sphere, you represent the welfare of the citizens and an Indian. Thus there is no need for special representation for minorities, because whatever their issues were can be addressed in the general sphere"
  * Iqbal's response was: No, minorities have issues of their own. There should be a special representation of their own
  * This brings into question the idea of the Nation. The conception of a nation can be either 
    1. Territorial, defined by a geographical boundary, and everybody within the boundary is an Indian, or
    2. Cultural. To have a cultural understanding of what the nation is, not simply territorial. Then the minority argument is that the territory of India is *made up of several nation, since many culture live and coexisted here*. According to this then, we have to ensure the rights and privileges of these minorities are ensured
  * **The congress' arguments were contingent on the territorial framework**
  * **Iqbal uses the cultural framework, and thus he envisioned a *"A Muslim India within India"***

## Debates on Social Reform post 1820

* The debates after the Crown took over the EIC were centered around Gender
  * Sati pratha, Widow remarriage
* Around the 1880, the Hunter Commission was set up to investigate what reforms were needed for educational reforms
  * It found that Muslims were far behind Hindus in terms of modern education
  * This was a wake up call for the Muslim community and the Muslim leaders
  * The debate of the compatibility of Islamic traditions and modernity started to arise both in Europe and India
    1.  Is Islam compatible with modernity?
    2. How do Muslims preserve their past and at the same time interpret or adapt into the new present?
    3. How do we introduce these ch-ch-ch-changes smoothly, i.e, without disrupting existing social realities?
  * Sayed Ahmed Khan felt that Muslims were left behind, and so he started Alighar Muslim College (formerly Alighar *Anglo* Muslim College)
* Key figures: **Al Afghani, Mohd. Aubudh, Sayed Ahmed Khan** and of course **Iqbal**

## Question of Majority/Minority

> Disclaimer: As the professor explained, the ideas of majority and minority communities is not one that can be used without acknowledging the intersectionality and heterogeneity of the community you are addressing. Thus, I'm putting the terms majority and minority in double quotes to remember that we are using these terms while acknowledging this nuance, and using them in the context of the questions we are asking. The validity of these questions should be looked at by answering the question: "What is the merit of this demand/objection"?

* Is the Indian National Congress a Hindu "majority" organization which cannot address the questions of "minority" communities?
    1. Are there problems specific to "minority communities" which the INC cannot address?
    2. Are those problems things that should be addressed in any kind of future that we envisioned? Are they serious enough to demand special attention?
    3. If the answer is yes, then how do we do it? If they aren't being addressed, then how should we go about addressing them?