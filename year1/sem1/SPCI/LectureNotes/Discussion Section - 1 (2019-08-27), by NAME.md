# Discussion Section - 1 (2019-08-27), by Annie/Aditi?

*Assignment: Write down my idea of  modernity and tradition and see if it changes after the readings and classes*

<u>Tradition</u>: Tradition refers to the (sometimes ritualistic) practices that exist in any given community

<u>Modernity</u>: Refers to any ideas or practices that did not traditionally exist in a given community. These ideas don't necessarily have to be opposed to whatever the traditions of the community may be

