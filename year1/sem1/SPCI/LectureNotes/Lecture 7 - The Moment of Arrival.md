# The Moment of Arrival
## Mature phase of Indian Nationalism

* Nehru's ideas are congruent with the growing educated urban class
* He was the quintessential representation of the contemporary youth. He says he discovered India as a westerner, but the spirit of India was always very strong in him
* Based on his understanding of India, his vision for India says 
  1. Unless you become an economic powerhouse, you can't solve the problems of today
  2. You cannot avoid becoming industrialized and modernized, because if you want to remain relevant in the world, you need to become modernized
  3. To do the above, we have to first capture power from the colonialists, because their interests don't lie in the modernization or development [1] of India
* Thus, the primary need is to establish a sovereign nation-state and to do that we have to replace the colonial government

## The "thematic" and "problematic" in context of Nehru / Ideological differences b/w Gandhi and Nehru

* The *thematic* is of establishing a modern nation-state through scientific and rational means
* There is nothing essential in Europe that makes it predisposed to becoming modernized and scientific, just as there is nothing essential to the East that makes it "backwards" (Chattarjee, p 136-7 [PDF 146-7])
* Here is an ideological difference between Gandhi and Nehru
  * Gandhi says that this PoV is essentially European and if we start thinking like this we are headed for failure. He considers the ideas of development and the nation-state extremely problematic
  * Nehru says that this European spirit is exactly what we need and exactly what we should borrow. He says that we can take these concepts of scientific development and rationality out of their original cultural context (i.e, that of the West), they don't belong to any culture. Then it becomes common to all humanity
  * Nehru argues that there should be a strong, democratic and representative state that is in charge of the development and planning with the backing of the nation, and
  * To form said nation-state, we need to
    1. Remove colonial power
    2. Uhh.. something about everybody having equal representation regardless of socioeconomic background (everybody's vote is equal)

## Why did Nehru champion Gandhi?

> Peasants were 'ignorant' and subject to 'passions'. They were 'dull certainly, uninteresting individually, but in the mass they produced a feeling of overwhelming pity and a sense of ever-impending tragedy'. They needed to be led properly, controlled, not by force or fear, but by 'gaining their trust', by teaching them their true interests.
> \- Chatterjee (p 148 [PDF 157])

* Thus, Nehru considered Gandhi as the leader of a vanguard to mobilize the masses. A charismatic leader who had an ingrown understanding of how the peasant works and thinks, who (Gandhi) acts not from reason or rationality, but from instinct
* *However*, Nehru said that a nation cannot be built on just passion or instinct. He considered Gandhi's ideas obsolete, reactionary and idiosyncratic. Still, this reactionary (Gandhi) has captured India like no leader had

## Definitions

1. *Development* - Associated with technological and scientific innovation

## DS Notes

1. Subaltern styles of historiography - Look it up!