\documentclass[12pt]{article}

\usepackage{fontspec} 
\setmainfont{Times New Roman}
\usepackage{setspace}
\usepackage[backend=biber,sorting=none]{biblatex}
\usepackage[left=1in, right=1in, top=0.8in, bottom=1in]{geometry}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\addbibresource{SPCI2.bib}

\onehalfspacing

\title{Conceptions of Nationalism}
\author{Cocoa}
\date{Social and Political in Contemporary India - FC-0402-1}

\begin{document}

	\maketitle
	
	\section{Introduction}
	
		The conception of India as a nation through the 1990s has manifested in various forms by different political leaders. Each conception of the nation doesn't exist independently of the others, and as the different ideas of India interact, they change in form. They change their ideological basis for nationalism as well as the realistic plans of action they posit. In this paper, I will be talking about the conception of nationalism by Gandhi, Nehru, Jinnah and Iqbal, looking at them through the analytical framework of \emph{the problematic and thematic} as explained by Partha Chatterjee. I would like to talk about how these conceptions of nationalism engage in a conversation with each other, and how their own problematic and thematic changes in conversation. Through the paper, I will talk about India from without and within, namely, the conception of India as a nation in a global context, and the conception of the nation 	
	
	\section{The Problematic and Thematic}
	
		The analytical framework used in this paper to understand each ideology is one proposed by Partha Chatterjee where each ideology is understood at two levels. The first is \emph{the problematic}, which is the practical action an ideology proposes, and the other is \emph{it's thematic}, which is the justificatory structures it uses to provide reason for the practical realization of it's problematic. According to Partha Chatterjee
		
		\begin{quote}
			
			We wish to separate the claims of an ideology, i.e. its identification of historical possibilities and the practical or programmatic forms of its realization, from its justificatory structures, i.e. the nature of the evidence it presents in support of those claims, the rules of inference it relies on to logically relate a statement of the evidence to a structure of arguments, the set of epistemological principles it uses to demonstrate the existence of its claims as historical possibilities, and finally, the set of ethical principles it appeals to in order to assert that those claims are morally justified. The former part of a social ideology we will call its problematic and the latter part its thematic~\cite[38]{chatterjee1986nationalist}			
			
		\end{quote}
	
		So, for each of the conceptions of nationalism we will discuss, we will consider it's problematic and thematic. That is to say, the purely theoretical ideals that the ideology posits (the thematic), the possibilities it identifies for the nation through it's own theoretical framework of the nation (the problematic), and how it justifies it's problematic through it's thematic. 
		
		The idea of the problematic and thematic has a number of parallels from linguistics and philosophy, as Chatterjee has discussed~\cite[39]{chatterjee1986nationalist}, however, since mathematics and philosophy often overlap, I believe a parallel between mathematical frameworks and Chatterjee's sociological framework is also possible. At the very foundation of modern mathematics, there exists a similar dynamic in the rules and structures that mathematics is built upon. There exist abstract elements, and rules that allow us to relate one element to the other, and these rules or axioms allow us to describe and control the behavior of these elements. An example of this would be how the natural numbers are constructed, where the numbers are abstract elements related to each other through a rigorous axiomatic system which describes the behavior of numbers as well as the rules for their existence. Here, the abstract elements (for instance, natural numbers) serve the function of the problematic, and the axiomatic framework from which they arise serve the function of the thematic. Thus, when thinking of an ideology, I find it's useful to think of the thematic as \emph{the axioms the ideology proposes}, and the problematic as \emph{the direct consequence of the framework set up}.
		
	\section{The Nation and The World: Gandhi and Nehru}

		To begin our discussion, we will look at how India perceived itself in a global context. This meant defining the Indian identity in opposition to a colonial identity, and the two key figures who attempted to do this were M. K. Gandhi and Jawaharlal Nehru. Gandhi thought of India along the lines of a moral, ethical and religious entity, one which puts the peasant as the central stakeholder when talking about India. On the other hand, Nehru conceived of India in macroeconomic terms, a democratic nation state with similar ideas of progress and industry as Western countries. We will start our discussion with Gandhi
		
		\subsection{Gandhi}			
			
			Partha Chatterjee showed how Gandhi rarely operated within the thematic or problematic of nationalism. Rather than providing a critique of the colonial occupation of India, Gandhi provided a moral critique of the institutions and practices that constituted modern civilization
		
			\begin{quote}
			What appears as a critique of Western civilization is, therefore, a total moral critique of the fundamental aspects of civil society. It is not, at this level, a critique of Western culture or religion, nor is it an attempt to establish a spiritual claims of Hindu religion. [...] At this level of thought, Gandhi is not operating at all with the problematic of nationalism. His solution too is meant to be universal applicable as much to the countries of the West as to nations such as India
			
			Not only that; what is even more striking, but equally clear, is that Gandhi does not even think within the thematic of nationalism. He seldom writes or speaks in terms of the conceptual frameworks or the modes of reasoning and inference adopted by the nationalists of his day, and quite emphatically rejects their rationalism, scientism and historicism.~\cite[93]{chatterjee1986nationalist}
			\end{quote}
			
			For instance, Gandhi's issue with modern medicine is that it promotes self indulgence and gluttony. His issue with Railways is that it gave the British immense control over India, that it spread plague and that it shrinks the size of the world by allowing us to cover distances faster. He critiques lawyers by saying that the profession furthers disputes rather than resolve it. In all his critiques, his criticism is not of the British, but rather that of the institutions that compromise modern society, however, the British were the power that brought these institutions into India in their modern form. Thus, he indirectly positions the colonialists as the carrier of the disease that is modern society. He uses the thematic of truth and morality to provide this critique of civil society. 
			
			The solution, according to Gandhi, was a complete rejection of civil society. He envisioned a utopian society (a Ramrajya) where ``the ruler, by his moral quality and
habitual adherence to truth, always expresses the collective will" and where society is organized by the varna system with no competition and differences in status between different kinds of labour~\cite[92]{chatterjee1986nationalist}. In order to achieve this, the peasant, who was before inactive in the nationalist discourse, would have act against the symbol of modern society which was the British power in India

			Moreover, Gandhi's idea of the nation advocated for self reliance. He argued that one should do physical labour to feed the body, and intellectual labour to feed the mind, and one should be self sufficient, in that they should produce what they require. The modality through which this idea is realized is encapsulated in the problematic of Gandhism, namely the ideas of \emph{satyagraha} and \emph{ahimsa}. Through these two ideas, Gandhi's ideology transforms from a simple critique of civil society to a framework for a nationalist movement. Gandhi created a 'science of non-violence' as a response to the problematic of nationalism~\cite[107]{chatterjee1986nationalist}
			
		\subsection{Nehru}
			
			Nehru as a political figure is somewhat more mainstream and easier to understand than Gandhi. Nehru's conception of the nation was one grounded in rational thought and economic and scientific progress. He viewed India as a Westerner would, while simultaneously being an Indian. Nehru operated within the thematic of establishing a modern democratic nation-state, however at the same time, he championed Gandhi, a figure whose entire philosophy was built around a critique of modernity. Nehru's problematic can be looked into in two parts. The first dealing with how he viewed Gandhi as a tool to mobilize the peasant class, and the other being his actions towards establishing a modern democratic nation-state, namely, the National Planning Committee
			
			\subsubsection{Gandhi as a figure to Mobilize the Peasantry}
			
				So far the nationalist movement had been an elite affair, however if they wanted their ideas to be realized, then they needed a populist revolution. They were unable to mobilize the peasantry, since the larger economic goals of the nation often didn't overlap with the individual needs of the peasant. This is where Nehru saw the potential that Gandhi had.  He had struck a chord with the peasant classes across the nation, and thus Nehru wanted to use Gandhi to mobilize the masses. He viewed Gandhi as the leader of the Vanguard. Thus, in order to achieve this, Nehru had to accommodate Gandhi within his problematic.
				
				\begin{quote}
				
				Peasants were `ignorant' and subject to `passions'. They were `dull certainly, uninteresting individually, but in the mass they produced a feeling of overwhelming pity and a sense of ever-impending tragedy'. They needed to be led properly, controlled, not by force or fear, but by `gaining their trust', by teaching them their true interests~\cite[148]{chatterjee1986nationalist}
				
				\end{quote}	
			
			\subsubsection{The National Planning Committee}
			
				The other aspect of Nehru's problematic deals with the modality of achieving his idea of the democratic nation-state. His ideas aligned with that of the growing educated urban middle class, and his vision for India was one where India stands as a fully modernized and industrialized nation with a voice in the world, and in order to do this, power had to be taken back from the colonialists. In his attempts to do so, he established the National Planning Committee with the goal to gather information about the current economic state of India and plan ahead for the future		  
			
	\section{Religion Inside The Nation: Iqbal and Jinnah}
	
		Now that we have looked at the two conceptions of India in a global context, we turn to how India was conceived by the people living in India. Namely, we turn to the question of religious minorities. Iqbal's idea of a nation was not a territorial one, but rather a cultural one. He argued that a nation comprises of a culture and it's history, and thus he envisioned a \emph{Muslim India within India}. At this juncture in the discussion is where our conceptions of nationalism start to influence each other, since this idea of a Muslim Indian with India was posited to secure the rights of the Muslim minorities within India. The Nehru Reports in the early 1920 rejected the demands for reservations and accommodations along religious lines, stating ``what your religion, caste, gender, etc. is, when you participate in the public sphere, you represent the welfare of the citizens and an Indian. Thus there is no need for special representation for minorities, because whatever their issues were can be addressed in the general sphere". Nehru's idea of a modern democratic nation-state had come into conflict with Iqbal's cultural conception of India
		
		Here, the thematic that has been developed is that of the \emph{two nation theory} of India. The problematic of minority rights and accommodations is justified using the two nation theory. Jinnah elaborates on the need for the two nation theory in his presidential address to the All-India Muslim League
		
		\begin{quote}
		
		The British and particularly the Congress proceed on the basis, ``Well, you are a minority after all, what do you want? What else do minorities want?" Just as Baba Rajendra Prasad said. But surely the Musalmans are not a minority. We find that even according to the British map of India, we occupy large parts of this country where the Musalmans are in a majority~\cite[208]{zaidi1978evolution}
		
		\end{quote}
		
		\noindent Thus, when considering the question of minorities in India, Jinnah functions as the problematic which is justified by the thematic of the two nation theory provided by Iqbal. Jinnah and Iqbal represent the two levels in which one can think of the minority question in India		
		
	\section{Conclusion}
	
		We see three conceptions of the nation arise through the various conceptual frameworks proposed by Gandhi, Nehru, Iqbal and Jinnah. Namely, the moral and ethical conception of India by Gandhi, the modern democratic nation-state by Nehru, and the two nation conception of India by Iqbal and Jinnah. These three conceptions are often never isolated from each other, and they interact and evolve over time. Gandhi's rejection of modernity and Nehru's strive towards it seem contradictory, however as we saw, Nehru accommodates Gandhi's ideas in order to use him to mobilize the masses. Their two conceptions of the nation, which conceptually are antipodal to each other, in practice are often enmeshed. Similarly, the cultural and territorial conception of India is where Nehru and Iqbal's conceptions of India interact. The question of minority representation, which isn't addressed in Nehru's thematic is where Iqbal's thematic comes in. By arguing for a Muslim India inside India, Jinnah is able to justify his demands for accommodations for Muslims in legislation
	
	\printbibliography

\end{document}