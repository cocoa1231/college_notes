\documentclass[12pt]{article}

\usepackage{fontspec}
\usepackage[pdfpagelabels=true]{hyperref}
\usepackage{pdfpages}
\usepackage{bookmark}
\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry}
\usepackage[backend=biber,sorting=none]{biblatex}
\usepackage{setspace}

\setmainfont{Times New Roman}
\onehalfspacing
\addbibresource{SPFBibliography.bib}

\title{Social and Political Formations: Mid Term Assignment}
\author{Cocoa}

\begin{document}

	\maketitle
	
	I've chosen to present my arguments against Gandhi's views on Doctors, Lawyers, Parliament, Railways, Education and Machinery. Since Gandhi has expressed his views in separate sections in his book \emph{Hind Swaraj: Indian Home Rule}~\cite{HindSwaraj}, I will also discuss each issue as it appears in the book, since I believe it provides a critique of the narrative Gandhi sets up as well. When Gandhi talks about Doctors and Lawyers, he talks by having an archetype of a person representing that institution and the structural faults that are inherent to the institution. For instance, he has the following critique of Doctors
	
	\begin{quote}
		Their business is really to rid the body of diseases that may afflict it. How do these diseases arise? Surely by our negligence or indulgence. I overeat, I have indigestion, I go to a doctor, he gives me medicine, I am cured. I overeat again, I take his pills again. Had I not taken the pills in the first instance, I would have suffered the punishment deserved by me and I would not have overeaten again~\cite[53]{HindSwaraj}
	\end{quote}
	
	Here, Gandhi has used the phrase ``Doctor" to talk about the effect of modern medicine in general. His issue lies with the fact that modern medicine enables us to indulge in, what he perceives as, sin. Thus, throughout this paper, I will not use the word ``Doctor", rather I will directly refer to the institution of modern medicine or modern healthcare. Similarly, I will not say ``Lawyer", I will use phrases such as the judicial system. I believe this to be more helpful and direct. Let's start our discussion where Gandhi starts, with Railways
	
	\section{Railways}
	
		Gandhi's objection to railways come from two separate but connected angles. The first is that he believes that railways are one of the ways the British have established and held power in India. He does not elaborate on this in Hind Swaraj, however I don't think this is a controversial statement. His other critique of railways was, as usual, a moral one. The following quote, I believe, is the crux of his position
		
		\begin{quote}
			The railways, too, have spread the Bubonic plague. Without them, the masses could not move from place to place. They are the carriers of plague germs. Formerly we had natural segregation. Railways have also increased the frequency of famines because, owing to facility of means of locomotion, people sell out their grain and it is sent to the dearest markets. People become careless and so the pressure of famine increases. Railways accentuate the evil nature of man: Bad men fulfill their evil designs with greater rapidity. The holy places of India have become unholy. Formerly, people went to these places with very great difficulty. Generally, therefore, only the real devotees visited such places. Nowadays rogues visit them in order to practice their roguery~\cite[41,42]{HindSwaraj}
		\end{quote}
		
		The central issue with this line of reasoning is that it says that railways inherently cause these issues, whereas I believe that railways are simply a tool, and under the colonial government, it wasn't used to serve the interests of the Indian population. For instance, while it is true that more interconnected populations do make it more likely that a plague will spread, this doesn't address the actual various plagues. Often plagues are created through animal to human transmission of pathogens (such as the bubonic plague~\cite{CDCPlague}), or through contaminated water food (e.g, cholera). These conditions were present in abundance in Europe, and thus Europe had massive plague epidemics~\cite{Americapox}. Similarly, his argument for why railways increased the frequency of famines was because people become careless and sell their grains in bulk to the market, however he fails to take a more holistic view of the situation, such as considering other environmental factors~\cite{Mishra}. He assumes causation due to perceived correlation. His argument is contingent on assuming that people inherently have a good or evil nature. He elaborates on this idea in the next chapter
		
		\begin{quote}
			I should, however, like to add that man is so made by nature as to require him to restrict his movements as far as his hands and feet will take him. If we did not rush about from place to place by means of railways and such other maddening conveniences, much of the confusion that arises would be obviated~\cite[44]{HindSwaraj}
		\end{quote}
		
		His argument is always contingent on assuming that a person has a defined role in life, a set duty to fulfill, and that god has given them all the tools required to fulfill said duty. It is the person who creates disharmony by seeking more than what has been given to them. He ignores the fact that railways have, in fact, helped bring food from areas where there is a surplus to areas where there is a shortage. I think a better critique would be to attack the policies surrounding railways, rather than attacking railways as a whole
		
	\section{The Judicial and Legislative System: Lawyers and the Parliament}
	
		Gandhi argued that a representative government is inherently a dysfunctional one. He says, specifically about the English parliament, that it has not done a single useful thing yet. He says that it always acts under public pressure, and the fact that this happens is a symptom of the dysfunction. If a parliament is supposed to have representatives who are educated and well informed, and elected by the people, then it should function smoothly. Yet the fact it does not is, for him, a sign that the parliamentary structure is flawed. Further, he says that the form of dispute resolution offered by this system is one that doesn't resolve disputes, it perpetuates them and profits off of exacerbating them
		
		\begin{quote}
			The lawyers, therefore, will, as a rule, advance quarrels instead of repressing them. Moreover, men take up that profession, not in order to help others out of their miseries, but to enrich themselves. It is one of the avenues of becoming wealthy and their interest exists in multiplying disputes. It is within my knowledge that they are glad when men have disputes. Petty pleaders actually manufacture them. Their touts, like so many leeches, suck the blood of the poor people. Lawyers are men who have little to do. Lazy people, in order to indulge in luxuries, take up such professions. This is a true statement. Any other argument is a mere pretension~\cite[51]{HindSwaraj}
		\end{quote}
		
		While Gandhi does bring up issues with the parliamentary system which are valid and need to be addressed, I believe he does not provide a better solution. For instance, he says \emph{``If the money and the time wasted by Parliament were entrusted to a few good men, the English nation would be occupying today a much higher platform"}, however this entirely depends on what one calls a ``good man". His solution to solving the systemic issues of the parliamentary system of legislation is to default to the judgment of whatever is supposed to be a ``good man". This would, in the end, mean giving the power to decide who is good and who isn't to one person or a few people. Further, when he talks about lawyers, he only talks about conflict resolution between individuals. He does not provide a framework for conflict resolution between individuals and larger political institutions
		
	\section{Modern Medicine: Doctors}
	
		Gandhi's primary issue with ``Doctors" or simply modern medicine is that it lets one indulge in sinful activities. Gluttony, lethargy, etc. He says that if one just cures themselves of diseases caused by these sinful activities, they will never learn their lesson. This may also be a reflection of Gandhi's views on consumption under capitalism, however I will talk about that in the section of Machinery. For now, what Gandhi is saying about modern medicine is an interesting approach to the idea of the \emph{Principle of Personal Responsibility}, which is a ideological framework for determining who is and isn't ``deserving" of healthcare. The principle of personal responsibility says that those who are ``responsible for their own condition" should not be prioritized over those who require medical attention through no fault of their own. This principle doesn't debate the question of whether or not healthcare is something that should be provided, rather it's a commentary on the autonomy of the individual
		
		What Gandhi does is that he takes away the individual's autonomy in this dynamic. He says that it is not the individual's fault if they seek out healthcare when they are ``sick because of their actions" (a concept that isn't unproblematic, as discussed by Oliver Thorne in his video essay \emph{Healthcare, Ethics \& Postmodernism}~\cite{PTHealthcare}), rather it is the fault of the institution of modern medicine for providing avenues of these cures. This can be refuted  just by acknowledging that the individual does in fact have autonomy, and limiting that autonomy should not be a method to ``teach them their lesson", whatever that may be according to the speaker's morality
		
	\section{Education and Machinery/Automation}

		I've chosen to talk about Education and Automation together because I believe they are heavily interconnected. Gandhi's views on both of these topics touch upon the idea of \emph{Alienation} in Marxist ideologies. Alienation on it's own is defined as ``a social or psychological ill involving the problematic separation of a subject and object that properly belong together"~\cite{Stanford}. In the context of Marx, he talks about alienation between the labourer (the subject) and four different objects
		
		\begin{enumerate}
			\item The products they make
			\item The act of labouring
			\item Other labourers
			\item Their ``species-essence"~\cite{PTAlienation}
		\end{enumerate}
	
		I will not talk in detail about these concepts here, as it's already been done by Oliver Thorne, cited below. Rather, I would like to talk about how Gandhi expresses similar ideas. 
		
		When talking about Education, Gandhi says
		
		\begin{quote}
			A peasant earns his bread honestly. He has ordinary knowledge of the world. He knows fairly well how he should behave towards his parents, his wife, his children and his fellow villagers. He understands and observes the rules of morality But he cannot write his own name. What do you propose to do by giving him a knowledge of letters ? Will you add an inch to his happiness? Do you wish to make him discontented with his cottage or his lot?~\cite[82]{HindSwaraj}
		\end{quote}
		
		Here, what he essentially says is that Education to a peasant, who is satisfied with how they are living, would bring nothing but discontentment with their current life. Ironically though, he also argues that peasants are not capable of deciding what the concepts of \emph{Satyagraha} should mean, and thus they should default to the teachings of a few incorruptible people who he calls \emph{Satyagrahai Leaders}. Here, this discontentment that Gandhi warns us about is what Marx would call ``Alienation of the labourer from the act of labouring". Similarly, his fears for automation stem from two different places. First being that he thinks automation puts people out of jobs, a fear that isn't unfounded. And the second being Alienation of the labourer from the products they make
		
		He argues that people should be provided with what is needed to sustain themselves, and the only use of their labour should be to fulfill their bodily requirements. When a labourer works for a wage for somebody else, the products they will make will not be used to fulfill their needs
		
		Even though to some extent I agree with the idea of alienation, I still object to Gandhi's solution to the problem of alienation, and here I would agree with Nehru. His ideas don't allow for the nation to have an educated mass, capable of independent thought. And while automation does reduce employment, it also enables better standards of living for most people. Automation and education are a necessary tool that the nation has to wield if it wants to have a voice
		
	\section{Conclusion}
	
		The pattern seen above is that Gandhi critiques modern society in similar a fashion to other socialist philosophers such as Marx. However, Gandhi's solution to organizing a nation as vast and diverse as India is to default to his conception of morality. It requires moral absolutism to function, and thus inherently fails to acknowledge diversity. As Partha Chatterjee demonstrates, Gandhi's views were never meant to be one that should be implemented in the real world, and they do contain contradictions within themselves. However, Gandhism was supposed to be an ideal that the peasantry can aspire to in order to mobilize, and an ideal that was completely against the oppressors of the time. Gandhi provides a critique of modern society, he calls it a disease, and he names the British as the people who have infected us. The cure he provides is an alluring one, however it is not a practical one
		 
	\printbibliography
	
\end{document}
