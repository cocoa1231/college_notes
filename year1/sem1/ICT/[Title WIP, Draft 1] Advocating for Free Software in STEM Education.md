# [Title WIP, Draft 1] Advocating for Free Software in STEM Education

> TODO: Add more accessible content. Namely, add more ideas from other fields, including non-STEM fields

If you've ever sat in a high school mathematics class, you know the boredom and dread that often goes along with it. It's a shame really that a subject as artistically appealing as mathematics is often diametrically opposite to art in people's minds, and the reason for it is simple. Mathematicians need a better PR rep.

In this article, I want to talk to teachers of mathematics, science and related fields, about changing one key aspect of teaching, which I feel has multiple benefits simultaneously. Namely, I want to make the case for teaching these subjects through programming and using Free Software while doing so

## Part 1: What is "Free Software"?

When I say "Free Software", I don't want you to think that it doesn't cost anything (although that is the case most of the times). Rather, I would like to explain it using a quote from one of the founders of this movement, Richard M. Stallman

> Free software does not mean that you don't have to pay. Think of "free" as in freedom of speech, not free beer

And which is why the term "libre" (from the word liberty) is often used rather than free

Libre software is a different school of thought about *how* software should be distributed. It's key features are that it gives users the right to,

1. Look at the source code of the software
2. Modify the code
3. Distribute modified or unmodified copies of  the software

These three freedoms are considered essential for a software to be considered libre. Along with these, there are more principles that go along with this, such as respecting the user's right to privacy and security.

## Part 2: Why "Free Software"

Although free software doesn't have to be focused around non monetary goals, the reality is that it often is. Consider a popular software used in Science education, Matlab by Matworks. When a university purchases a license for this for their students, the economic overhead is obviously something that the university has to bear. Moreover, once the students do start using this software, if they would ever like to learn how Matlab does what it does, they are not allowed to, because Matlab's primary interest, as a corporate, lies with making sure they have a monopoly in their market and thus the actual source code is a trade secret

On the other hand, let's take a look at a popular free alternative called Matplotlib. It's a library written in the Python programming language and it's freely available for everybody to use. Since it is free and open source, thousands of people from around the world have worked on it to make sure that it's up to industry standards, and that it's fast, accessible and secure. If somebody would like to learn how the internals of this library work, all the code is well documented and available to read online, and it has a vibrant community around it.

In case of "Matplotlib", the primary goal is not monetary gain, it's to create a good library for mathematical graphics. 