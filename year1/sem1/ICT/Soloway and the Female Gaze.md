# Soloway and the Female Gaze

- All writing is propaganda for themselves.    *Protagonism is propaganda that protects and perpetuates privilege.*

- <u>The Female Gaze</u>

  1. *A way of feeling-seeing* - A way to get inside the protagonist. It uses the frame to share and evoke a feeling of being in-feeling rather than looking at. *Reclaiming the body, using it as a tool to communicate a feeling seeing*
  2. *The gazed gaze* - The camera does the impossible task of showing what it feels like to be the subject of the gaze. Your effect on the world when you are seen. It's a story structure similar to the heroine's journey
  3. *Returning the gaze* - "I see you seeing me, and I don't want to be the object any longer. I want to be the subject and with that subjectivity I want to name you as the object". This is a *socio-political justice demanding way of art making*

- The female gaze is a empathy generator. It's a conscious effort to create empathy for women

- *The divided feminine* - how men divide women into the the Madonna and the whore

  - If you're the Madonna, you're what men want and by being that you share their privilege. You get safety and privacy
  - If you're the whore, well. "You get what's coming to you" (Soloway)

  