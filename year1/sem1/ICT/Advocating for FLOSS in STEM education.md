# Advocating for FLOSS in STEM education

*  **Proposition**: The use of Free/Libre and Open Source Software, or, FLOSS, should be made an essential part of STEM education

## Audience

The target audience for this op-ed would be my peers, whom I've found to be not very informed about the philosophy and reality of the Free Software movement. Thus while I don't expect too hard a push back against this idea, since this idea would be presented to most as a new one, I would expect some arguments that come from the quality of education provided. The common fallacy that Free and/or Libre software is inherently less professional and less suited for academic or enterprise use. This is, of course, easily disproven by simply giving the statistics of usage of FLOSS in nearly everything related to computation

### Reasoning

The reasoning would be based on two main arguments, accessibility and pedagogy

#### Accessibility

The argument here is not very complex. Free software is often, literally, free of cost. Although that is not a necessary condition for software to be free (or a better term would be libre), it is often the case that it is. This already helps in terms of economic accessibility, in that this can be implemented with little to no financial overhead, and often reduce the costs that the educational institution often bears such as licensing fees.

This, however, would only be helpful if the quality of education is not lowered. This brings me to my second point

#### Pedagogy

Why I think the philosophy of the free software movement lines up particularly well with well tested methods of teaching is that both of them encourage *active and interactive learning*. [Insert some sources about why active learning is more effective rather than passive learning]

When teaching with a proprietary software, the method of implementation, the actual logic behind the computation, is hidden behind the monetary interests of the parties selling the software. With libre software, the student's ability to learn from the software can be put before anything else. Not only is it possible for the student to always learn from the literature of code that they use, it's encouraged. It's encouraged that the user explore the code and that they make it their own, and share it. A sense of learning, exploration and play is given the foremost importance

I would like to draw the classic analogy between code and baking here. Imagine going to a baking class, but not being able to see the recipes of your ingredients. Imagine being given a cake, where the student's job is to use the cake in their own recipe, but they never get to learn to make the cake, or even see it's recipe. Moreover, each student is taught to value this philosophy, with the argument that sharing your unique recipe would put you at a disadvantage compared to your peers. That the students must hide their creations in order to succeed in their course. You cannot see how the cake is made, and cannot share how you make your  dish.

Learning how to bake in such an environment would not be very stimulating. Similarly, learning how to use code without learning about the code is utterly pointless. This isn't to say that everybody should understand how the code they are using works, but that they should have the ability to do so. Especially in an educational and academic setting

#### Artistic Appeal

Talk about the artistic appeal of a playful environment, and how often open source software encourages artistic expression of science

## Goal

My goal with this article would be to convince the reader that the use of FLOSS not only encourages a more stimulating, communal and playful learning environment, but that is also at par with, if not better, than the preexisting proprietary software used in most STEM fields.  I would also like to show the reader that this option isn't just a Utopian dream, rather that it can actually implemented economically