# ICT: Anatomy of an Academic Essay, Part 1 (2019-10-22)

## Structure

- The development of the essay should be *linear*
- Guess that's the only point that was emphasized in the class

### Methods of Organizing content

- Outlining
- Essay Mapping 
  - **What** - What's the argument you're trying to make?
  - **Why** - Why are you making this argument? How does it add to the narrative you're constructing with your essay?
  - **How** - How are you going to support your argument? What sources are you gonna cite? What is the background information required, such as the context, jargon, statistics, etc.?
  - Give each topic a word limit, how much do you want to elaborate on each?

## Introduction

- Start by answering "**What** is my essay about?"
- **Why** is what you're talking about something that should be talked about?
- And lastly, walk through the line of reasoning your essay will follow

