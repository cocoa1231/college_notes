# ICT: Anatomy of an Academic Essay, Part 2 (2019-10-29)

## The Body - The "How"

- Crap I zoned out. I'll go back to the slides

### Paragraphing

- One para deals with one topic
- All paragraphs should be related to the central claim of the essay
- Each sentence in a paragraph should follow a linear reasoning structure
- Every idea discussed in the para should be well explained and sourced

#### How to build a paragraph

1. Decide on a topic sentence/claim
2. Explain your topic sentence
3. Provide Evidence
4. Explain evidence - Why have you used this piece of evidence? How does it support your major claims?
5. Complete the paragraph's idea or transition into the next paragraph

## The Conclusion - The "Why"

- Why does your interpretation matter? Why have you written this and why have I spent time reading this
- This is the last chance you have to persuade your readers, and this is what they will be leaving with
- It should explain your essay within a larger context

### How to write a conclusion

- Return to the claims made in the intro maybe?
- Ask "so what"? Tell the reader why should anybody care about this
- Put your writing in the context of a larger whole
- Now that you have set up your narrative, the reader has all the background needed. Now explain to the reader how the arguments flow from one to the other, how they support your main argument, and how does everything relate together
- Redirect your reader's thought process to a new place. Make them think about the issue in a way they maybe didn't think about in the intro of the essay