\documentclass[12pt,titlepage]{article}

\usepackage{fontspec} 
\setmainfont{Times New Roman}
\usepackage{setspace}
\usepackage[backend=biber,sorting=none]{biblatex}
\usepackage[left=1in, right=1in, top=0.8in, bottom=1in]{geometry}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\onehalfspacing

\title{The Neurotypical Gaze: Portrayals of Atypical Minds in Visual Media}
\author{Cocoa}

\addbibresource{NTGaze.bib}

\begin{document}
	
	\maketitle
	
	\vspace*{\fill}
	
	\begin{center}
	\textit{The celebration of Autism transcends the shadows of ableism and the silence of tolerance. Celebration pulls first-person autistic experience and joy into the limelight. The celebration of Autism is the most powerful tool we have to hold back the dark of indignity and to spread our message. In a world dark with pity, fear, and hatred for people with disabilities, celebration gets people's attention. Celebration is radical.}
	\end{center}
	
	\begin{flushright}
	\~{} Myth Schaber\\
	Celebration of Autism \\
	RAIC Richmond Autism Resource Fair, 2015
	\end{flushright}
	
	\vspace*{\fill}

	\pagebreak	
	
	
	\section{Introduction}
	
		The idea of, what we will come to understand as the neurotypical gaze, is similar to the idea of the Orientalist Gaze as proposed by Edward Said. It is an attempt at describing the experiences and worlds of somebody else, in his case, the ``orientals", from the point of view of somebody else who has power over them. As autistic writer Katherine May puts it
		
		\begin{quote}
		
			I am reminded of Edward Said’s 1978 description of the orientalist gaze, in which the exoticised subjects endure a kind of fascinated scrutiny, and are then rendered ‘without depth, in swollen detail’. Never allowed to speak for themselves, their behaviours are itemised, but not actually understood~\cite{may2018asdfrominside}
			
		\end{quote}
		
		When it comes to identifying the social construct of the neurotypical gaze, we have to first understand disability from a sociopolitical point of view. In this essay, I would like to illuminate what I understand as the neurotypical gaze by going through three portrayals of autism. The stereotypical folk understanding of ASD is that of a rude, clever, and socially inept man, and one of the works that has both represented and reinforced this is the 1988 Dustin Hoffman movie `Rain Man'. I will attempt to summarize what this stereotype primarily consists of. Then, to see how this portrayal has developed over the years, I would like to look at the 2017 Netflix series `Atypical'. Lastly, as an example of a portrayal of a well rounded autistic individual in media, I will discuss Abed Nadir from NBC's 2009 series `Community' by Dan Harmon
		
		While discussing these images of autism in media is helpful, it runs the risk of feeling pointless if we do not discuss how simple depictions of autism in media can have real consequences in the lives of autistic individuals. It is here where I would like to start discussing the neurodiversity movement, it's history and what it aims to do. We will see how people internalize ideas of autism from media portrayal, and further nuance this by discussing how this internalization and it's effect is felt differently depending upon the person's gender. I would like to discuss about some of the drawbacks of the social model of disability, and finally I would like to elaborate on my personal experience with social life and medical professionals in India as an autistic person. However, in order to understand all of this, I will first cover the background you would require to understand the paper
		
	\section{Background}
	
		The background required for this discussion is immense, however I will attempt to introduce the basic concepts of disability, neurodiversity, the social and medical models of disability and the current discourse surrounding ASD. Throughout the essay, I will also be talking about some ideas, colloquialisms and discourses that exist inside the autistic community, which one gets to learn by being a part of the community. I will try to cite forum threads as much as I can, however it will be hard to provide academic citations for a number of these. Due to these, I would like to ask the reader to take this into consideration and consider what I say as my personal experience, to some extent. With that said, let us start by understanding what neurodiversity means
		
		\subsection{Neurodiversity}
		
			The idea of neurodiversity was popularized by Sociologist Judy Singer in her 1998 thesis ``NeuroDiversity: The Birth of an Idea"~\cite{judysingerthesis}, and since then, she has reworked the idea into the contemporary neurodiversity ``movement" that exists right now. She has outlined her view of neurodiversity in a blog post titled ``What does NeuroDiversity mean?"~\cite{judysingernd2}. The term ``neurodiversity" is a subset of the idea of biodiversity. According to Singer, the idea of neurodiversity follows directly from the idea of biodiversity
		
		\begin{quote}
		
			The most stable environments are those that are most diverse and that every species is of intrinsic value and has a necessary part to play in the whole~\cite{judysingernd2}
		
		\end{quote}
		
		Singer imagined the neurodiversity movement as a part of the disability rights movement. It provides a framework for any neurominority to shed off the stigma assigned to them through the \emph{medical model of disability}, and naturally, it provides a counter to it by talking about \emph{the social model of disability}~\cite[214 - 221]{davis2013disability}
		
		\subsubsection{Models of disability}
		
			The idea of neurodiversity is heavily dependent on the social model of disability. The social model of disability emerged in the disability rights movement in Britain in the 1980s. It is created as a response to the preexisting ideas of disability, retrospectively labeled the medical model of disability. The medical model of disability conflates the idea of disability and impairment. It assigns the burden of the disability on the individual, and calls any deviation from the norm wrong, and perpetuates a culture of cure. It is, in essence, what philosopher Michel Foucault called ``the medical gaze". It reduces the person into a bag of symptoms, or in this case, a set of atypicalities, which the medical practitioner aims to cure
			
			\begin{quote}
				
				A `gaze' is an act of selecting what we consider to be the relevant elements of the total data stream available to our senses. Doctors tend to select out the biomedical bits of the patients' problems and ignore the rest because it suits us best that way.~\cite{Misselbrook2013}
			
			\end{quote}
			
			The social model of disability is the cure to the medical model of disability. It provides a framework which puts the disabled person's experiences and identity first. The social model of disability starts by creating the distinction between a disability and an impairment\footnote{As an autistic individual, rather than call it impairments, I prefer to call my atypicality `a difference'. There reason for doing so is that even though my brain processes input atypically, I am still only disabled because the input is designed by and for people with typical sensory processing. This, however, doesn't stay true for all invisible disabilities, such as ADHD, which can come with severe executive dysfunction and does require mediation to manage}. An impairment exists at an individual level, but a disability arises at a social level. It is when society fails to accommodate these impairments is when the impairment becomes a disability. In other words, preexisting social structures that solely cater to able bodied people are the cause of the disability, not the individual's own impairments. In this model, disabled people are an oppressed group, and the oppressors are non-disabled people~\cite{davis2013disability} 
			
		\subsection{Autism}
			
			% I am an individual who has been formally diagnosed with `High-functioning Autism Spectrum Disorder', as my diagnosis read. When I received my diagnosis at the age of 17, I had no prior knowledge of what autism was, apart from a visceral disgust for the atypical, specifically autistic (which was a catch-all term for ``weird", ``annoying", or ``bratty") cultivated through social interactions throughout my childhood. In retrospect, I hated all that made me autistic, because it was never accepted around me. I had faced verbal and physical abuse from my peers and elders for acting, thinking and moving in ways that felt natural and healthy to me, and thus, I developed a hatred for myself. I begin this section on understanding what Autism is with my experience because any attempts at defining Autism can be distressful to the reader, specially if they are autistic themselves. The community of autistic people is mostly created by non autistic psychologists, and thus, entry into this community requires an allistic person to deem you to be autistic. I would like to reassure the reader
			
			Autism Spectrum Disorder, or ASD, is a developmental disorder that shows in early childhood. According to the Diagnostic and Statistical Manual of Mental Health Disorders, fifth edition (DSM-5), in order for a person to be diagnosed with ASD, the individual has to have ``Persistent deficits in social communication and social interaction across multiple contexts" and ``Restricted, repetitive patterns of behavior, interests, or activities" from early childhood, and these symptoms must cause ``clinically significant impairment in social, occupational, or other important areas of current functioning". The diagnostic criteria of Autism go further into detail about the severity of each of these symptoms and thus categorize the individual as ``high functioning" or ``low functioning"
			
			It is important to remember that these diagnostic criteria were developed by observing autistic people by non autistic people, and thus, these represent the autistic experience as seen from the outside. Because of this, these diagnostic criteria inherently do not reflect the actual experience and inner world of the autistic individual. To understand what Autism is, we should look at it through the lens of neurodiversity
			
			\subsubsection{Looking at Autism Through Neurodiversity}
			
				Autism is a spectrum, and there are as many different kinds of autistic people as there are people. It is important to understand that the Autism spectrum isn't a spectrum from ``not very autistic" to ``extremely autistic". Rather, it is a set of spectra, each describing an individual's cognitive ability. Autistic people often have impairments in some of these, such as executive functioning. Autistic people will have the following traits
				
				\begin{enumerate}
					\item All autistic people will have some form of atypical sensory processing, which means that they will have certain aversion or affinity towards certain sensory inputs, such as touch, sound, or smell. Some autistic individuals love the textures of things, while others cannot tolerate it
					
					\item Autistic people will have atypical ways of processing information. Their cognition patterns differ from that of allistic people
					
					\item Autistic people have different capacities for language. We often don't think in terms of language of words, and we often start speaking at a later age. Some autistic people will, of course, not speak with their mouth parts at all
					
					\item Autistic people often also have atypical motor functioning, often manifesting itself as motor dyspraxia, which is a disconnect between the brain's intentions and execution of motor functions, causing issues with fine or gross motor control
					
				\end{enumerate}
			
				Autism is a developmental condition, which means that manifests from a very young age. Thus it is important to understand that \emph{autism is not something to be cured}. It is a different way in which the brain has developed, and it is a natural variation in the human brain.
				
				
					
	\section{Portrayals of Autism in Media}
	
		\begin{quote}
		
		I found myself at a lunch table recently with a journalist, a sociologist and a business psychologist, which sounds like the set-up for a bad joke. We were discussing Silicon Valley, and its notorious working culture of long hours and company-dictated leisure time. `The problem, of course,' said the sociologist, `is that these businesses are institutionally autistic.'

		`I'm autistic,' I said. `Can you explain what you mean by that term?'

		[...]
		
		`I mean,' he said, a little more carefully, `that these companies are run by men who probably have Asperger syndrome.' A pause. I raise my eyebrows. `And so there's a lack of … emotional understanding.'~\cite{may2018asdfrominside}
		
		\end{quote}
		
		The above quote is from Katharine May's post titled \textit{Autism from the Inside}, and it describes what the current folk perception of autism is. 
	
	\printbibliography
		
\end{document}