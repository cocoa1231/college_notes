\documentclass[12pt,titlepage]{article}

\usepackage{fontspec} 
\setmainfont{Times New Roman}
\usepackage{setspace}
\usepackage[backend=biber,sorting=none]{biblatex}
\usepackage[left=1in, right=1in, top=0.8in, bottom=1in]{geometry}
\usepackage{hyperref}
\usepackage{enumerate}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=black,
}

\onehalfspacing

\title{The Neurotypical Gaze: Portrayals of Atypical Minds in Visual Media}
\author{Cocoa}

\addbibresource{NTGaze.bib}

\begin{document}
	
	\maketitle
	
	\vspace*{\fill}
	
	\begin{center}
	\textit{The celebration of Autism transcends the shadows of ableism and the silence of tolerance. Celebration pulls first-person autistic experience and joy into the limelight. The celebration of Autism is the most powerful tool we have to hold back the dark of indignity and to spread our message. In a world dark with pity, fear, and hatred for people with disabilities, celebration gets people's attention. Celebration is radical.}
	\end{center}
	
	\begin{flushright}
	\~{} Myth Schaber\\
	Celebration of Autism \\
	RAIC Richmond Autism Resource Fair, 2015
	\end{flushright}
	
	\vspace*{\fill}

	\pagebreak	
	
	
	\noindent\hrulefill
	
			\begin{center}
			\textbf{Abstract}
			\end{center}
			
			Neurotypical portrayals of people on the spectrum often render the individuals as flat caricatures of actual people, distilled down to their disability, serving only the function of moving along a plotline. Through this paper, I have attempted to dissect this phenomena through the framework of The Gaze, borrowing from Edward Said's Oriental Gaze and Foucault's Medical Gaze. Further, this paper attempts to demonstrate the impacts of such representations in terms of internalization of ableist sentiments and provide a response to, what we will conceive of, as ``The Neurotypical Gaze", attempting to oppose it through systemic economic redistribution and representative recognition
	
	\noindent\textit{\textbf{Keywords-}} Autism, Gaze, Judy Singer, Social Recognition
	
	
	\noindent\hrulefill		
	
	\section{Introduction}
	
		The idea of, what we will come to understand as the neurotypical gaze, is similar to the idea of the Orientalist Gaze as proposed by Edward Said~\cite{said1995orientalism}. It is an attempt at describing the experiences and worlds of somebody else, in his case, the ``orientals", from the point of view of somebody else who has power over them. As autistic writer Katherine May puts it
		
		\begin{quote}
		
			I am reminded of Edward Said’s 1978 description of the orientalist gaze, in which the exoticised subjects endure a kind of fascinated scrutiny, and are then rendered ‘without depth, in swollen detail’. Never allowed to speak for themselves, their behaviours are itemised, but not actually understood~\cite{may2018asdfrominside}
			
		\end{quote}
		
		When it comes to identifying the social construct of the neurotypical gaze, we have to first understand disability from a sociopolitical point of view. In this essay, I would like to illuminate what the neurotypical gaze is by going through portrayals of autism in visual media. The stereotypical folk understanding of ASD is that of a rude, clever, and socially inept man, and one of the works that has both represented and reinforced this is the 1988 Dustin Hoffman movie `Rain Man' and we will look at how this perception has changed over the years
		
		While discussing these images of autism in media is helpful, it runs the risk of feeling pointless if one does not discuss how simple depictions of autism in media can have real consequences in the lives of autistic individuals. Seeing oneself being portrayed by media results in the internalization of the stigmatization of the individual, and fails to let the individual nuance their world for themselves. The autistic viewer is fixed by the gaze of the allistic writer. The neurodiversity movement is the social movement which constructs the framework which allows one to break free of this gaze. 
		
		
	\section{Background}
	
		The background required for this discussion is immense, however here I have attempted to introduce the basic concepts of disability, neurodiversity, the social and medical models of disability and the current discourse surrounding autism.
		
		\subsection{Neurodiversity}
		
			The idea of neurodiversity was popularized by Sociologist Judy Singer in her 1998 thesis ``NeuroDiversity: The Birth of an Idea"~\cite{judysingerthesis}, and since then, she has reworked the idea into the contemporary neurodiversity ``movement" that exists right now. She has outlined her view of neurodiversity in a blog post titled ``What does NeuroDiversity mean?"~\cite{judysingernd2}. The term ``neurodiversity" is a subset of the idea of biodiversity. According to Singer, the idea of neurodiversity follows directly from the idea of biodiversity
		
		\begin{quote}
		
			The most stable environments are those that are most diverse and that every species is of intrinsic value and has a necessary part to play in the whole~\cite{judysingernd2}
		
		\end{quote}
		
		Singer imagined the neurodiversity movement as a part of the disability rights movement. It provides a framework for any neurominority to shed off the stigma assigned to them through the \emph{medical model of disability}, and naturally, it provides a counter to it by talking about \emph{the social model of disability}~\cite[214 - 221]{davis2013disability}
		
		\subsubsection{Models of disability}
		
			The idea of neurodiversity is heavily dependent on the social model of disability. The social model of disability emerged in the disability rights movement in Britain in the 1980s. It is created as a response to the preexisting ideas of disability, retrospectively labeled the medical model of disability. The medical model of disability conflates the idea of disability and impairment. It assigns the burden of the disability on the individual, and calls any deviation from the norm wrong, thus pathologizing the individual and perpetuating \emph{a culture of cure}. It is, in essence, what philosopher Michel Foucault called ``the medical gaze". It reduces the person into a bag of symptoms, or in this case, a set of atypicalities, which the medical practitioner aims to cure
			
			\begin{quote}
				
				A `gaze' is an act of selecting what we consider to be the relevant elements of the total data stream available to our senses. Doctors tend to select out the biomedical bits of the patients' problems and ignore the rest because it suits us best that way.~\cite{Misselbrook2013}
			
			\end{quote}
			
			The social model of disability is the cure to the medical model of disability. It provides a framework which puts the disabled person's experiences and identity first. The social model of disability starts by creating the distinction between a disability and an impairment\footnote{As an autistic individual, rather than call it impairments, I prefer to call my atypicality `a difference'. The reason for doing so is that even though my brain processes input atypically, I am still only disabled because the input is designed by and for people with typical sensory processing. This, however, doesn't stay true for all invisible disabilities, such as ADHD, which can come with severe executive dysfunction and does require mediation to manage}. An impairment exists at an individual level, but a disability arises at a social level. It is when society fails to accommodate these impairments is when the impairment becomes a disability. In other words, preexisting social structures that solely cater to able bodied people are the cause of the disability, not the individual's own impairments. In this model, disabled people are an oppressed group, and the oppressors are non-disabled people~\cite{davis2013disability} 
			
		\subsection{Autism}
			
			Autism Spectrum Disorder, or ASD, is a developmental disorder that shows in early childhood. According to the Diagnostic and Statistical Manual of Mental Health Disorders, fifth edition (DSM-5), in order for a person to be diagnosed with ASD, the individual has to have ``Persistent deficits in social communication and social interaction across multiple contexts" and ``Restricted, repetitive patterns of behavior, interests, or activities" from early childhood, and these symptoms must cause ``clinically significant impairment in social, occupational, or other important areas of current functioning". The diagnostic criteria of Autism go further into detail about the severity of each of these symptoms and thus categorize the individual as ``high functioning" or ``low functioning"
			
			It is important to remember that these diagnostic criteria were developed by observing autistic people by non autistic people, and thus, these represent the autistic experience as seen from the outside. Because of this, these diagnostic criteria inherently do not reflect the actual experience and inner world of the autistic individual. 
				
				
					
	\section{The Neurotypical Gaze}

		 What we will construct as ``The Neurotypical Gaze" will borrow ideas from Edward Said's Oriental Gaze and Michel Foucault's Medical Gaze. The former operates on a macroscopic social and cultural level, of interactions between the orient and the occident, while the latter operates on a microscopic level of the skewed power dynamics of the doctor-patient relationship. As we will see, the neurotypical gaze creates a power dynamic that acts on both a socio-cultural level, and on an interpersonal level. 
		 
		 One key element of the neurotypical gaze are that it creates an image of the atypical individual in a similar fashion as the oriental gaze, that is, \emph{it reads the object of the gaze from a framework of neurotypical speech, movement, and cognition}, and it modifies the subject's story, their identity and their history, discarding large chunks of it and renders the subject as a flat caricature of a person. Another key element of the neurotypical gaze that separates it from similar ideas such as the male gaze is that the neurotypical gaze can only manifest once the person is identified as atypical. That is to say, atypical people possess \emph{invisible disabilities}, thus any manifestations of it can only happen \textit{after} the individual is outed as atypical (either through manifestations of that atypicality\footnote{Such as when autistic people stim. Autism is an invisible disability, however when an autistic person stims in public, that invisible disability becomes visible} or by active choice). A result of this invisibility is that when people with atypical minds interact with the world around them, the process of internalization of society's messaging about them\footnote{Here, what I refer to ``society" is simply the culture one finds themselves in. This paper focuses mostly on American and ``Western" depictions of atypicality, and in some cultures, some traits of one's atypicality might be valued positively} happens more subtly than, say, in the case of orientalist depictions of the East
		 
		 What is unique to the neurotypical gaze is that it is the medical gaze magnified to a larger social and cultural level, which is to say, the social and cultural images of ``the autist" is constructed from the medical description of autism from Leo Kanner and Hans Asperger, the people who coined autism and Asperger's respectively, and the disorder's development throughout the years. Such an examination of the proliferation of the stereotypes relating to autism is already done by Draaisma Douwe~\cite{douwe2009asd} and thus we will not go into a similar analysis, but simply touch upon it
		 
		\subsection{Medical conceptions of Autism}
		 
			A more complete description of the medical and social history of Autism can be found in Davis' ``Disability Studies Reader" in the article ``Autism as Culture" by Joseph N. Strauss~\cite[460]{davis2013disability} and the article by Draaisma Douwe~\cite{douwe2009asd}, however for our purposes, what matters is how the category of Autism was constructed. The descriptions of Autism were created by \emph{observing} the subject, in order to develop the ``Gestalt of the child - his voice, face, body language, intonation, gestures, gaze, expression and diction."~\cite{douwe2009asd}. 
			
			Asperger's description of the first case study, Fritz V., ``presents a lengthy description of his looks, his aggressive behaviour on the ward, his resistance against being tested, his thin, high-pitched voice, his adult-like choice of words, his clumsiness, his irritated reactions to any show of affection, his vacant gaze, his nonsense answers to questions, his precociousness in arithmetic and his abrupt mood swings"~\cite{douwe2009asd}. His subsequent descriptions become shorter, as the reader is now expected to have a mental image of the autistic child. ``Once this profile has been pointed out to you, Asperger claimed, you will recognize it at first sight, as soon as the boy enters and as soon as he starts talking."~\cite{douwe2009asd}
			
			What becomes evident from Asperger's account of the children is that rather than questioning the child, the child is observed. He is not asked why he is resistant to being tested, he isn't asked why he is resistant to show affection, or why his gaze is vacant. The descriptions Asperger provides is one that intends to document rather than discover the individual. In the whole process, it is assumed that the atypical child shows affection in the same way neurotypical standards of affection are set, that a person's gaze isn't supposed to be ``vacant", a concept that is rooted in neurotypical patterns of looking. 
		 
		 	This practice has continued to the present day, reflected in the Diagnostic and Statistical Manual's (DSM) description of Autism Spectrum Disorder. It defines the diagnostic criteria for Autism by observing, from a neurotypical framework of movement, expression and reciprocity, autistic children, describing them using phrases such as ``repetitive patterns of behavior, interests, or activities", ``apparent indifference to pain", ``ritualized patterns of verbal or nonverbal behavior", ``repetitive motor movements", etc.~\cite{american2013diagnostic} As Katherine May has already critiqued this, she writes
		 	
		 	\begin{quote}
		 	
				To autistic communities, the DSM’s descriptors can feel less like a neutral diagnostic matrix, and more like a colonial narrative. They fail to grasp the challenging aspects of autistic experience, and pathologise the positive ones. The DSM goes on to note ‘apparent indifference to pain’, which might well be true if you judge pain and its responses only in neurotypical terms~\cite{may2018asdfrominside}		 	
		 	
		 	\end{quote}
		 	
		 	Similarly, the DSM lists sensory sensitivities as a subset of repetitive behaviors, however, ``for many autistics, is routinely described as the experience from which many of the observable autistic behaviours derive: the repetitive movements (known as self-stimulatory behaviour or ‘stimming’), withdrawal from social contact, ‘extreme distress at small changes’ and ‘inflexible adherence to routines’ are often responses to being sensorily overwhelmed, and are aimed at managing chaotic environments and bringing about pleasant feelings to counteract unpleasant ones"~\cite{may2018asdfrominside}. To get a better understanding, refer to ``Autism as Culture", subsection ``Autism Culture" from Davis~\cite[466]{davis2013disability}. These narratives of Autism from the outside develop the seed for what becomes the folk perception of autism
		 	
		\subsection{Social and Cultural conception of Autism}
		
			The stereotypes that Asperger and Kanner created have since taken on a life in media. Movies such as `Rain Man' echo the image of an autist, one who lacks empathy, is unable to communicate, mathematically inclined, but disconnected from the world. They are portrayed exactly as Kanner described autistic kids, ``Human beings normally live in constant interaction with their environment, and react to it continually. However, “autists” have severely disturbed and considerably limited interaction. The autist is only himself (cf. the Greek word autos) and is not an active member of a greater organism which he is influenced by and which he influences constantly"~\cite[460]{davis2013disability}. The pervasiveness of autistic characters in current media is highlighted in Draaisma's article
			
			\begin{quote}

				We all remember the white-coat scene in Rain Man. There is not such a scene in the movie Snow Cake, released in 2006. Alex, the character played by Alan Rickman, comes to spend a few days with Linda, an autistic woman, played by Sigourney Weaver. He is puzzled by Linda’s bizarre behaviour and at some point halfway through the movie, when they are in the backyard, Linda jumping up and down on a trampoline, her neighbour confides to him, behind the back of her hand: ‘Autistic, but very verbal’. Alex answers with a nod of understanding. That is all. She is autistic and apparently he does not need more information, and neither do we~\cite{douwe2009asd}
			
			\end{quote}
			
			These folk conceptions of autism are essentially magnification of the diagnostic criteria of autism developed through the medical model of disability to the level of mass literature. We can see that autistic characters are often used as plot points, rather than actual characters. TV shows such as Netflix's `Atypical' and ABC's `The Good Doctor' use autism as a plot device. The character is defined by their disabilities and impairments, and other people often have to tolerate and put up with them. Relationships between autistic and allistic characters are not relationships between equals, with mutual respect, admiration and reciprocity for each other. Autistic writer and computer scientist Ada Hoffmann articulates this in her blog post
			
			\begin{quote}
			
				Sheldon Cooper from The Big Bang Theory fascinates me because of his sheer indifference to other people’s expectations, his insistence on being himself.

Of course, the cost of Sheldon’s attitude is that to his friends he is an annoyance, a burden, even a child. Nobody really likes autistic characters, in the end. People put up with us. People selflessly shoulder the burden of our inconvenient selves.~\cite{hoffmann2018fiction}
			
			\end{quote}
			
			One rarely finds autistic characters that are well rounded and have fully fledged relationships, and one good example of this is the relationship between Troy and Abed from Dan Harmon's Community (NBC). As Ada Hoffmann explains
			
			\begin{quote}
			
				 I get obsessed with the friendship between Abed and Troy. The strongest, closest, most joyful relationship on the show is an autistic friendship. Not a friendship between the autistic man and a nice person who took pity on him, but a friendship based on sheer mutual excitement, a friendship which actively refuses to be normal. Troy and Abed spend their time wrapped up in science fictional worlds of their own devising, playing imaginary games the way I did as a child, gleefully indifferent to everyone else’s expectations of reality~\cite{hoffmann2018fiction}
			
			\end{quote}
			
	\section{Recognition}
		
		We have discussed \emph{what} the neurotypical gaze is, and how it fails to represent autistic experiences faithfully in media, however the impact of this power imbalance and responding to it is yet another complex discussion, one which involves dissecting the social, political and psychological impacts that these representations have on the lives of autistic individuals. Through this section, we will briefly discuss these impacts through the framework of social and political recognition, namely, the Hegelian idea of intersubjective recognition. Since the neurotypical gaze acts at an interpersonal level as well, a brief discussion of internalization of ableism will also be conducted
				
			The philosophical concept of `recognition' can be understood as ``the act of acknowledging or respecting another being, such as when we `recognise' someone's status, achievements or rights"~\cite{recogiep} and it is an important means through which we can socially value and respect one another. The process through which two consciousnesses become self-aware, i.e, develop a consciousness of the self as a separate entity from the external other, is by being acknowledged or ``recognized" by that external other, according to Hegel. He posits that ``it is through the intersubjective recognition of our freedom that right is actualised. Rights are not instrumental to freedom; rather they are the concrete expression of it. Without recognition we could not come to realise freedom, which in turn gives rise to right"~\cite{recogiep}, thus when discussing the ramifications of the neurotypical gaze on a social and political level, we must understand how atypical people are left unrecognized or misrecognized
			
			As we have seen, the neurotypical gaze magnifies and somewhat flattens the power dynamics of the medical gaze to a macroscopic scale. When one sees these one-sided portrayals of atypical minds in the media, they form an image of the specific atypicality, such as forming the image of ``the autist". When these images of autistic people exist on this larger cultural level, it becomes harder to secure rights based on actual needs of autistic people, and rather the rights given to them are often based off of the cultural image that exists of them. It is in this sense that we can say that rights are a concrete expression of freedom, and without them one cannot come to realize freedom. Examples of this can be special education programs in schools that end up harming the child, abusive methods of therapy such as Applied Behavioral Analysis, abusive practices to ``cure" autism such as chelation, and so on.
			
			Charles Taylor called recognition a ``vital human need" and said ``can inflict a grievous wound, saddling its victims with a crippling self-hatred". What Taylor has identified is the psychological aspect of misrecognition, which we will call \emph{internalized ableism}. The harm that the neurotypical gaze inflicts on a cultural and political level is one of social injustice and oppression, however on an individual level, seeing oneself misrepresented can prevent one from developing a nuanced image of themselves. If they are always portrayed as burdens on society, they will see themselves as burdens on burdens of society. It is the psychological process of internalization\footnote{Which is defined as ``the nonconscious mental process by which the characteristics, beliefs, feelings, or attitudes of other individuals or groups are assimilated into the self and adopted as one’s own" by the APA Dictionary of Psychology} that happens when a flat cultural conception of autism is presented as the only recognition one sees, and this has been studied as well. Neurotypical and non-neurotypical adults naturally form negative first impressions of people on the spectrum~\cite{grossman2019asd}. Ada Hoffmann echos this in her closing lines about ``Community" (warning: spoilers)
			
			\begin{quote}
			
			Troy leaves in season five, of course. “I want to be one person,” he says. Even when you have a happier autistic friendship than it is physically possible to have, your friends will still get tired of your shit.~\cite{hoffmann2018fiction}
			
			\end{quote}
			
	\section{Conclusion}
		
		The focus now shifts from the social, political and psychological ramifications of the neurotypical gaze to how to oppose this gaze. How does one go about reclaiming their identity when their identity has been diluted, flattened, and stigmatized to such an extreme point? How does one respond to the neurotypical gaze? I believe the response is twofold. First, is recognition. Not misrecognition, rather representative recognition. Media, art, music, and culture made by autistic people exists, and it speaks boldly ``my brain is beautiful". It allows us to speak our own language, to make our own sounds, and to paint our own stories. In Kristina Chew’s words, ``Autistic language is a fractioned idiom, its vocabulary created from contextual and seemingly arbitrary associations of word and thing, and peculiar to its sole speaker alone . . . Autistic language users think metonymically, connecting and ordering concepts according to seemingly chance and arbitrary occurrences in an ‘autistic idiolect’ "~\cite[469]{davis2013disability}. Only once we have been allowed to recognize ourselves, not as broken neurotypical people, but as whole autistic people, is when we can start to shed off our internalized ableism and self-hatred. The second is redistribution. Recognition alone can only help heal the wound made to the individual, and to the social conceptions of atypicality. It does not address the fact that economic redistribution of resources is also required to compensate for the fact that disabled people, especially people with invisible disabilities, have always had an inherent disadvantage. ~\cite{recogiep}
			
	\printbibliography
		
\end{document}